#include <iostream>

#include "AudioObject.h"
#include "AudioProject.h"
#include "SDLUtils.h"
#include <algorithm>

AudioObject::AudioObject() {
	m_startIndexInWFMFile = 0;
	m_EndIndexInWFMFile = 0;

	m_startTimeInTrack = 0;
	//m_endTimeInTrack = 0;
	m_MediaFile = NULL;

}
AudioObject::~AudioObject() {

}

WFM_Stereo_SampleBlock * AudioObject::GetEnergy(WFM_Stereo_SampleBlock* pEnergyBuf,double &offmsOnTheEnergy, WFM_Stereo_SampleBlock* pEnergyEndBuf,int & left, int & right) {
	
	//left = 0;
	//right = 0;
	//double frameDuration = m_MediaFile->GetDurOfFrame();
	//WFM_Stereo_SampleBlock* pEnergy = pEnergyBuf;
	//double freeMSInCurFrame = offmsOnTheEnergy;
	//double FilledMsInUPP = frameDuration- freeMSInCurFrame;  //�Ѿ�ȡ�õ� ms ��Ŀ;
	//do {
	//	if (freeMSInCurFrame <= AudioProject::UPP) {  //The remaining time is less than AudioProject::UPP
	//		left = std::max(pEnergy->left, left);
	//		right = std::max(pEnergy->right, right);
	//		FilledMsInUPP += freeMSInCurFrame;

	//		pEnergy++;
	//		if (pEnergy > pEnergyEndBuf) {
	//			offmsOnTheEnergy = 0;
	//			pEnergy = NULL;
	//			break;
	//		}

	//		offmsOnTheEnergy = frameDuration - (AudioProject::UPP - freeMSInCurFrame);
	//		freeMSInCurFrame = offmsOnTheEnergy;
	//	}
	//	else {
	//		left = std::max(pEnergy->left, left);
	//		right = std::max(pEnergy->right, right);
	//		offmsOnTheEnergy -= AudioProject::UPP;
	//		//offmsOnTheEnergy = frameDuration - (AudioProject::UPP - FilledMsInUPP);
	//		//offmsOnTheEnergy -=  (AudioProject::UPP - FilledMsInUPP);


	//		break;

	//	}


	//} while (pEnergy != NULL);

	//return pEnergy;
	//Ҫ�õ� UPP MS ������

	 left = 0;
	 right = 0;
	double frameDuration= m_MediaFile->GetDurOfFrame();
	WFM_Stereo_SampleBlock* pEnergy = pEnergyBuf;
	double freeMSInCurFrame = offmsOnTheEnergy;
	double FilledMsInUPP= 0;  //�Ѿ�ȡ�õ� ms ��Ŀ;
	//double FilledMsInUPP= frameDuration - freeMSInCurFrame;  //�Ѿ�ȡ�õ� ms ��Ŀ;
	do {
		if ((FilledMsInUPP + freeMSInCurFrame) <= AudioProject::UPP) {
			left = std::max(pEnergy->left, left);
			right = std::max(pEnergy->right, right);
			FilledMsInUPP += freeMSInCurFrame;

			pEnergy++;
			offmsOnTheEnergy = 0;

			if (pEnergy > pEnergyEndBuf) {
				pEnergy = NULL;
				break;
			}

			freeMSInCurFrame = frameDuration;
		}
		else {
			left = std::max(pEnergy->left, left);
			right = std::max(pEnergy->right, right);

			offmsOnTheEnergy = freeMSInCurFrame - (AudioProject::UPP - FilledMsInUPP);
			//offmsOnTheEnergy = (frameDuration- offmsOnTheEnergy)+( AudioProject::UPP- FilledMsInUPP);
			//offmsOnTheEnergy -=  (AudioProject::UPP - FilledMsInUPP);


			break;

		}


	} while (pEnergy != NULL);
	
	return pEnergy;
}
AudioObject AudioObject::Clone() {
	AudioObject ao;
	ao.m_MediaFile = m_MediaFile;
	//ao.m_endTimeInTrack = m_endTimeInTrack;
	ao.m_startTimeInTrack = m_startTimeInTrack;
	ao.m_startIndexInWFMFile = m_startIndexInWFMFile;
	ao.m_EndIndexInWFMFile = m_EndIndexInWFMFile;
	return ao;
}
int AudioObject::GetEndTimeInTrack() {

	char* pBuf;
	int bufSize;
	bool got = m_MediaFile->GetWaveformBuffer(&pBuf, &bufSize);
	if (!got)
		return 0;
	int samplecount = bufSize / sizeof(WFM_Stereo_SampleBlock);
	WFM_Stereo_SampleBlock* pwfmStartBuf = (WFM_Stereo_SampleBlock*)(pBuf + m_startIndexInWFMFile * sizeof(WFM_Stereo_SampleBlock));
	WFM_Stereo_SampleBlock* pwfmEndBuf = (WFM_Stereo_SampleBlock*)(pBuf + m_EndIndexInWFMFile * sizeof(WFM_Stereo_SampleBlock));


	int objectdur = pwfmEndBuf->mspos - pwfmStartBuf->mspos;
	return (m_startTimeInTrack + objectdur);

}
bool  AudioObject::Draw(SDL_Surface* surface, int startDrawTimeOnTrack,int& nextDrawPixelOnScreen) {

	char* pBuf;
	int bufSize;
//	printf("AudioObject::m_MediaFile->GetWaveformBuffer\n");

	bool got = m_MediaFile->GetWaveformBuffer(&pBuf, &bufSize);
	if (!got)
		return true;


	//printf("AudioObject::got:%d!\n", got);

	int samplecount = bufSize / sizeof(WFM_Stereo_SampleBlock);
	WFM_Stereo_SampleBlock* pwfmStartBuf = (WFM_Stereo_SampleBlock*)(pBuf+ m_startIndexInWFMFile* sizeof(WFM_Stereo_SampleBlock));
	WFM_Stereo_SampleBlock* pwfmEndBuf = (WFM_Stereo_SampleBlock*)(pBuf + m_EndIndexInWFMFile * sizeof(WFM_Stereo_SampleBlock));


	int objectdur = pwfmEndBuf->mspos - pwfmStartBuf->mspos;
	double msperframe = m_MediaFile->GetDurOfFrame(); //һ��energy ������ms ��Ŀ�������Ԫ�������ٽ��б༭

	int endTimeInTrack = m_startTimeInTrack + objectdur;
	

	//int DrawingPixelStartPos = 0;
	//int DrawingPixelEndPos = 0;
	//
	double frameDuration = m_MediaFile->GetDurOfFrame();

	int framesOffset_screenleft = (int)(((double)startDrawTimeOnTrack - (double)m_startTimeInTrack) / msperframe);

	pwfmStartBuf = (WFM_Stereo_SampleBlock*)(pBuf + (m_startIndexInWFMFile + framesOffset_screenleft) * sizeof(WFM_Stereo_SampleBlock));

	if (pwfmStartBuf >= pwfmEndBuf)
		return  true;
	//if(m_startTimeInTrack <= AudioProject::LeftTimeOnScreen && endTimeInTrack>= AudioProject::LeftTimeOnScreen){
	//	DrawingPixelStartPos = 0;
	//	
	//	
	//}
	//else if (m_startTimeInTrack > AudioProject::LeftTimeOnScreen&& m_startTimeInTrack <=( AudioProject::LeftTimeOnScreen+AudioProject::TrackScreenWidth*AudioProject::UPP )) {
	//	//
	//	DrawingPixelStartPos =(int)(((double) m_startTimeInTrack - (double)AudioProject::LeftTimeOnScreen) / AudioProject::UPP);
	//}
	//else
	//	return false;

	//int rightpixel=(endTimeInTrack - AudioProject::LeftTimeOnScreen) / AudioProject::UPP;
	
	

	//һ�����ض���MS����energylist ���� ֵ����������ÿ��energy �м���һ��MS ƫ������
	int DrawingPixel = nextDrawPixelOnScreen;

	double unitstep=(startDrawTimeOnTrack - m_startTimeInTrack) / frameDuration;
	double freeMSInCurFrame = frameDuration - (unitstep - (int)unitstep) * frameDuration;

	//printf("AudioObject:: begin DrawingPixel %d  !\n",DrawingPixel);

	int channelHeight;
	if (m_MediaFile->m_FileInfo.channelNum == 1) {
		channelHeight = AudioProject::TrackScreenHeight;
	}
	else {
		channelHeight = AudioProject::TrackScreenHeight/2;
	}

	while (DrawingPixel < AudioProject::TrackScreenWidth) {

		int left, right;
		//bool res = false;
	//	printf("GetEnergy:: start %d  !\n",DrawingPixel);

		pwfmStartBuf=GetEnergy(pwfmStartBuf, freeMSInCurFrame, pwfmEndBuf,left,right);
	//	printf("GetEnergy:: end %d \n",DrawingPixel);

		if (!pwfmStartBuf)
		{
			//SDLUtils::FillRect(surface, DrawingPixel, 0,
			//	AudioProject::TrackScreenWidth - DrawingPixel, AudioProject::TrackScreenHeight, SDL_MapRGB(surface->format, 128, 128, 128));
			break;
		}
			
		int l = left / 32768.0 * channelHeight;
		if (l < 0)
			l = 0;
		if (l > channelHeight)
			l = channelHeight;

		int r = right / 32768.0 * channelHeight;
		if (r < 0)
			r = 0;
		if (r > channelHeight)
			r = channelHeight;
		//draw left

		SDLUtils::SDL_DrawReverseLine(surface, DrawingPixel, (channelHeight - l)/2, DrawingPixel, (channelHeight + l) / 2,
			SDL_MapRGB(surface->format, 34, 37, 39),
			SDL_MapRGB(surface->format, 12, 200, 12),
			SDL_MapRGB(surface->format, 178, 37, 39));
		//draw right
		if (m_MediaFile->m_FileInfo.channelNum > 1) {
			SDLUtils::SDL_DrawReverseLine(surface, DrawingPixel, channelHeight+(channelHeight - r) / 2, DrawingPixel, channelHeight+(channelHeight+r) / 2,
				SDL_MapRGB(surface->format, 34, 37, 39),
				SDL_MapRGB(surface->format, 12, 200, 12),
				SDL_MapRGB(surface->format, 178, 37, 39));
		}
		
		DrawingPixel++;
	}
	//printf("AudioObject:: end DrawingPixel %d  !\n",nextDrawPixelOnScreen);

	if (m_MediaFile->m_FileInfo.channelNum == 1) {
		SDLUtils::SDL_DrawLine(surface, nextDrawPixelOnScreen, channelHeight/2, DrawingPixel, channelHeight/2, SDL_MapRGB(surface->format, 128, 178, 128));

	}
	else {
		SDLUtils::SDL_DrawLine(surface, nextDrawPixelOnScreen, channelHeight / 2, DrawingPixel, channelHeight / 2, SDL_MapRGB(surface->format, 128, 255, 128));
		SDLUtils::SDL_DrawLine(surface, nextDrawPixelOnScreen, channelHeight * 3 / 2, DrawingPixel, channelHeight * 3 / 2, SDL_MapRGB(surface->format, 128, 255, 128));
		SDLUtils::SDL_DrawLine(surface, nextDrawPixelOnScreen, channelHeight , DrawingPixel, channelHeight , SDL_MapRGB(surface->format, 128, 178, 128)); 

	}
	nextDrawPixelOnScreen = DrawingPixel;


	return true;
}

