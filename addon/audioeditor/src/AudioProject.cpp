#include "AudioProject.h"
#include <string>
#include <iostream>

#include "SDLUtils.h"
#include <algorithm>


#define SUCCESS 0
#define SCROLL_HEIGHT 30
#define RULER_HEIGHT 30
#define TIMEINFO_HEIGHT  50

int AudioProject::playingCursorTime = 0;
int AudioProject::RangeStartTime = 0;
int AudioProject::RangeEndTime = 0;
int AudioProject::LeftTimeOnScreen = 0;
double AudioProject::UPP = 10; //dur per pixel. 10 MS per pixel
double AudioProject::MaxUPP = 10;
double AudioProject::MinUPP = 10;
int AudioProject::DurationOfProject = 1000;  //max
int AudioProject::TrackScreenWidth = 1000;
int AudioProject::TrackScreenHeight = 200;
//TTF_Font* AudioProject::font = NULL;
//ZOOMSCALE AudioProject::ZOOM = { 1,1 };

AudioProject::AudioProject()
{
	created=false;
	m_SDLScreenSurface = NULL;
	m_SDLWindow = NULL;
	m_szPrjName[0] = '\0';
	margin = 5;
	trackspace = 5;
	width = 0;
	height = 0;
	TrackCount = 1;

	mousePressedType = MousePressedType::NONE;

	leftmousebtnpressed = false;

	/*playingCursor = 0;
	RangeStartPos=0;
	RangeEndPos=0;
	RangeMask = 0;
	ScreenLeftEdgeTime = 0;
	zoomScale = 0;*/
}
AudioProject::~AudioProject()
{
	TTF_Quit();

}
int AudioProject::Create(const char* szPrjName, int width, int height)
{

	SDL_Init(SDL_INIT_VIDEO);
	TTF_Init();

	m_SDLWindow = SDL_CreateWindow("AudioEditor", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width, height, 0);

	if (m_SDLWindow == NULL)
	{
		SDL_Log("Window could not be created! SDL_Error: %s\n", SDL_GetError());
		return -2;
	}
	m_SDLScreenSurface = SDL_GetWindowSurface(m_SDLWindow);

	SDL_FillRect(m_SDLScreenSurface, NULL, SDL_MapRGB(m_SDLScreenSurface->format, 22, 32, 84));

	this->width = width;
	this->height = height;

	AudioProject::TrackScreenWidth = width - 2 * margin;
	AudioProject::TrackScreenHeight = (height - 2 * margin - TrackCount * trackspace - SCROLL_HEIGHT - RULER_HEIGHT - TIMEINFO_HEIGHT) / TrackCount;

	int y = margin;
	m_TimerRuler.Create(margin, y, AudioProject::TrackScreenWidth, RULER_HEIGHT);

	y += (RULER_HEIGHT + trackspace);
	for (int i = 0; i < TrackCount; i++) {
		AddTrack(margin, y); //  至少加一个音轨
		y += AudioProject::TrackScreenHeight + trackspace;

	}



	AudioProject::LeftTimeOnScreen = 0;

	//AudioProject::FramesOfScreen = AudioProject::TrackScreenWidth; //default a frame is showed in one frame

	m_HScrollBar.setPos(AudioProject::LeftTimeOnScreen);
	m_HScrollBar.setMaxPos(AudioProject::DurationOfProject);
	m_HScrollBar.setThrumbRange(AudioProject::LeftTimeOnScreen, AudioProject::DurationOfProject);
	m_HScrollBar.Create(margin, y, AudioProject::TrackScreenWidth, SCROLL_HEIGHT);

	y += SCROLL_HEIGHT;

	m_TimeinfoWnd.Create(margin, y, AudioProject::TrackScreenWidth, TIMEINFO_HEIGHT,
		std::bind(&AudioProject::OnButtonClick, this, std::placeholders::_1));

	created=true;
	return SUCCESS;
}
int AudioProject::Destroy()
{
	for(int i=0;i<m_TrackList.size();i++){
		delete  m_TrackList[i];
	}
	m_TrackList.clear();

	SDL_DestroyWindow(m_SDLWindow);
	SDL_Quit();
	AudioProject::playingCursorTime = 0;
 	AudioProject::RangeStartTime = 0;
 	AudioProject::RangeEndTime = 0;
 	AudioProject::LeftTimeOnScreen = 0;
 	AudioProject::UPP = 10; //dur per pixel. 10 MS per pixel
 	AudioProject::MaxUPP = 10;
 	AudioProject::MinUPP = 10;
 	AudioProject::DurationOfProject = 1000;  //max
 	AudioProject::TrackScreenWidth = 1000;
	AudioProject::TrackScreenHeight = 200;

	created=false;
	return SUCCESS;
}
int AudioProject::importFile(MediaFile* mf)
{
	if(!created)
		return -1;

	AudioTrack* at = GetActiveTrack();

	AudioProject::DurationOfProject = mf->GetDuration(); // the total duration of project is changed
	AudioProject::LeftTimeOnScreen = 0;
	//AudioProject::EditUnit = mf->GetDurOfFrame();//最小的编辑单元，
	AudioProject::UPP = (double)AudioProject::DurationOfProject / AudioProject::TrackScreenWidth;
	AudioProject::MaxUPP = AudioProject::UPP;

	/*if (AudioProject::DurationOfScreen == 0)
		AudioProject::DurationOfScreen = AudioProject::DurationOfProject;*/

		//AudioProject::EditUnit = std::max(AudioProject::EditUnit,mf->GetDurOfFrame());
	m_HScrollBar.setMaxPos(AudioProject::DurationOfProject);

	m_HScrollBar.setThrumbRange(AudioProject::LeftTimeOnScreen, (int)(AudioProject::DurationOfProject), true);

	//m_HScrollBar.setThrumbRange(20);
	at->Import(mf, 0);

	Render();
	return 0;
}
bool AudioProject::handleEvent(SDL_Event& stEvent) {
	if(!created)
		return false;
	switch (stEvent.type)
	{
	case SDL_QUIT:
		SDL_Log("Event type is %d", stEvent.type);
		return false;
	case SDL_MOUSEBUTTONDOWN:
		if (SDL_BUTTON_LEFT == stEvent.button.button)
		{
			SDL_Point sp = { stEvent.button.x,stEvent.button.y };


			leftmousebtnpressed = true;

			OnMouseLButtonDown(sp);
		}
		break;
	case SDL_MOUSEBUTTONUP:
		if (SDL_BUTTON_LEFT == stEvent.button.button)
		{
			leftmousebtnpressed = false;

			SDL_Point sp = { stEvent.button.x,stEvent.button.y };
			OnMouseLButtonUp(sp);
		}
		break;
	case SDL_MOUSEMOTION:
	{
		/*int px = event.motion.x;
		int py = event.motion.y;*/
		if (leftmousebtnpressed) {
			SDL_Point sp = { stEvent.motion.x,stEvent.motion.y };
			OnMouseMovingWithLeftButton(sp);

		}

	}
	break;
	default:
		break;
	}
	if (m_TimeinfoWnd.handleEvent(stEvent)) {
		m_TimeinfoWnd.Refresh();

	}


	return true;
}

void AudioProject::OnMouseMovingWithLeftButton(SDL_Point sp) {
	//if (SDL_PointInRect(&sp, &m_HScrollBar.locationRect))
	if (mousePressedType == MousePressedType::OnScrollbar)
	{
		//std::cout << "Left Mouse Button is on MousePressedType::OnScrollbar moving"  << std::endl;

		bool redraw = m_HScrollBar.OnMouseMoving(sp);
		if (redraw) {
			//	std::cout << "m_HScrollBar.OnMouseMoving return true"  << std::endl;

			AudioProject::LeftTimeOnScreen = m_HScrollBar.m_ThrumbStartPos;
			AudioProject::UPP = (m_HScrollBar.m_ThrumbEndPos - m_HScrollBar.m_ThrumbStartPos) / (double)AudioProject::TrackScreenWidth;
			AudioProject::UPP = std::min(AudioProject::UPP, AudioProject::MaxUPP);
			//std::cout << "begin m_TrackList refresh"  << std::endl;

			for (int i = 0; i < m_TrackList.size(); i++) {
				m_TrackList[i]->Refresh();
			}

			//	std::cout << "begin m_TimerRuler refresh"  << std::endl;

			m_TimerRuler.Refresh();
			//std::cout << "end m_TimerRuler refresh"  << std::endl;


		}


	}
	else if (mousePressedType == MousePressedType::OnTrack) {

		//std::cout << "Left Mouse Button is on MousePressedType::OnTrack moving"  << std::endl;

		for (int i = 0; i < m_TrackList.size(); i++) {
			if (SDL_PointInRect(&sp, &m_TrackList[i]->locationRect)) {
				m_TrackList[i]->OnMouseMove(sp);
				m_TrackList[i]->Refresh();
				break;
			}

		}

	}



}
void AudioProject::OnMouseLButtonDown(SDL_Point sp) {
	if (SDL_PointInRect(&sp, &m_HScrollBar.locationRect))
	{
		mousePressedType = MousePressedType::OnScrollbar;
		m_HScrollBar.OnMouseLButtonDown(sp);

	}

	else {
		for (int i = 0; i < m_TrackList.size(); i++) {
			if (SDL_PointInRect(&sp, &m_TrackList[i]->locationRect)) {
				m_TrackList[i]->OnMouseLButtonDown(sp);
				m_TrackList[i]->Refresh();
				mousePressedType = MousePressedType::OnTrack;

				break;

			}

		}
		m_TimeinfoWnd.Refresh();
	}


}
void AudioProject::OnMouseLButtonUp(SDL_Point sp) {
	if (SDL_PointInRect(&sp, &m_HScrollBar.locationRect))
		m_HScrollBar.OnMouseLButtonUp(sp);
	else {
		for (int i = 0; i < m_TrackList.size(); i++) {
			if (SDL_PointInRect(&sp, &m_TrackList[i]->locationRect)) {
				m_TrackList[i]->OnMouseLButtonUp(sp);
				break;
			}

		}
	}
	mousePressedType = MousePressedType::NONE;
}


bool AudioProject::AddTrack(int x, int y)
{
	AudioTrack* at = new AudioTrack();

	//int trackcount = m_TrackList.size();
	/*int validh = height - 2 * margin - trackcount * trackspace;
	int trackheight = validh / (trackcount + 1);*/

	at->Create(x, y, AudioProject::TrackScreenWidth, AudioProject::TrackScreenHeight);
	m_TrackList.push_back(at);

	Render();
	return true;
}
void AudioProject::Render()
{
	if(!created)
		return;
	int tracknum = m_TrackList.size();
	//int track_x = margin;
	//int track_y = margin;
	//SDL_Rect sr, tr;

	if (m_TimerRuler.m_pSurface) {
		SDL_BlitSurface(m_TimerRuler.m_pSurface, &m_TimerRuler.contentRect, m_SDLScreenSurface, &m_TimerRuler.locationRect);
		// std::cout << "SDL_BlitSurface m_TimerRuler"  << std::endl;

	}

	for (int i = 0; i < tracknum; i++)
	{
		SDL_Surface* sf = m_TrackList[i]->GetSurface();
		if (sf)
			SDL_BlitSurface(sf, &m_TrackList[i]->contentRect, m_SDLScreenSurface, &m_TrackList[i]->locationRect);
		//track_y += m_TrackList[i]->locationRect.h + trackspace;
	}

	if (m_HScrollBar.m_pSurface)
		SDL_BlitSurface(m_HScrollBar.m_pSurface, &m_HScrollBar.contentRect, m_SDLScreenSurface, &m_HScrollBar.locationRect);
	if (m_TimeinfoWnd.m_pSurface)
		SDL_BlitSurface(m_TimeinfoWnd.m_pSurface, &m_TimeinfoWnd.contentRect, m_SDLScreenSurface, &m_TimeinfoWnd.locationRect);

	// SDL_Rect sr;
	// sr.x = 0; sr.y =300; sr.w = 50; sr.h = 50;
	// SDL_FillRect(m_SDLScreenSurface, &sr, SDL_MapRGB(m_SDLScreenSurface->format, 33, 187, 33));


	// sr.x = 60; sr.y = 300; sr.w = 50; sr.h = 50;
	// SDL_FillRect(m_SDLScreenSurface, &sr, SDL_MapRGB(m_SDLScreenSurface->format, 187, 33, 33));


	if (m_SDLWindow)
		SDL_UpdateWindowSurface(m_SDLWindow);
}
bool AudioProject::Play() {
	//根据磁头位置，获得，当前的数据 
	return true;
}
bool AudioProject::Stop() {
	return true;
}
bool AudioProject::RemoveTrack()
{
	return true;
}

AudioTrack* AudioProject::GetActiveTrack()
{
	return m_TrackList[0]; //avtive track index
}

bool AudioProject::Copy()
{
	return true;
}
bool AudioProject::Paste()
{
	return true;
}
void AudioProject::OnButtonClick(const CBrButton* pButton) {
	switch (pButton->m_iId)
	{
	case ID_CUT:
		Cut();
		break;
	case ID_COPY:
		break;
	case ID_PASTE:
		break;
	case ID_UNDO:
		Undo();
		break;
	case ID_PLAY:
		break;
	case ID_STOP:
		break;
	default:
		break;

	}
}

bool AudioProject::Cut()
{
	bool update = false;
	if (AudioProject::RangeEndTime > AudioProject::RangeStartTime) {
		for (int i = 0; i < m_TrackList.size(); i++) {
			if (m_TrackList[i]->Cut(AudioProject::RangeStartTime, AudioProject::RangeEndTime) == 0)
				update = true;

		}
	}
	if (update) {
		AudioProject::MaxUPP = (AudioProject::DurationOfProject) / AudioProject::TrackScreenWidth;
		m_HScrollBar.setMaxPos(AudioProject::DurationOfProject, true);
		m_TimerRuler.Refresh();


	}


	return true;
}

bool AudioProject::Undo()
{
	bool update = false;

	for (int i = 0; i < m_TrackList.size(); i++) {
		if (m_TrackList[i]->Undo() == 0)
			update = true;

	}
	if (update) {
		AudioProject::MaxUPP = (AudioProject::DurationOfProject) / AudioProject::TrackScreenWidth;
		m_HScrollBar.setMaxPos(AudioProject::DurationOfProject, true);
		m_TimerRuler.Refresh();


	}
	return true;
}
bool AudioProject::Redo()
{
	return true;
}

bool AudioProject::Save()
{
	return true;
}
