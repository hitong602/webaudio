#pragma once

#include <SDL2/SDL.h>
#include "AudioTrack.h"
#include "MediaFile.h"
#include "scrollbar.h"
#include "timeruler.h"
#include "timeInfoWnd.h"
#include <vector>
//#include  <SDL2/SDL_ttf.h>

/* 
audioeditor ��һ�����̣�����������죬�������ʾ������
�����ṩһϵ�еĽӿ�
*/

enum class MousePressedType{OnScrollbar,OnTrack,NONE};
class AudioProject
{

	bool created;
	std::vector<AudioTrack*> m_TrackList;  //��ŵ�ǰ������

	char m_szPrjName[50];

	SDL_Window* m_SDLWindow;
	SDL_Surface* m_SDLScreenSurface;   //��������ռ�õ�screen
	ScrollBar m_HScrollBar; //
	TimeRuler m_TimerRuler;
	TimeInfoWnd m_TimeinfoWnd;
	int width;
	int height;
	int margin;
	int trackspace;

	int TrackCount;

	bool leftmousebtnpressed;


	MousePressedType  mousePressedType;
public:
	static int playingCursorTime;// unit is MS
	static int RangeStartTime;
	static int RangeEndTime;
	static int LeftTimeOnScreen; // time on the screen left, unit is MS , 
	static double UPP;// dur per pixel    
	static double MaxUPP;// 最大的UPP
	static double MinUPP;// 最小的UPP
	static int DurationOfProject; // duration of the project 
	static int TrackScreenWidth; // track width
	static int TrackScreenHeight; //track height
//	static double EditUnit; //最小的编辑单元，例如
//	static 	TTF_Font* font;

	//static ZOOMSCALE ZOOM;  //���� 2

	

		 

		 
public :
	AudioProject();
	~AudioProject();
public:
	int Create(const char* szPrjName, int width,int height);
	int Destroy();
	int importFile(MediaFile *mf);
	void Render();
	void OnButtonClick(const CBrButton* pButton);
public:
	bool handleEvent(SDL_Event& stEvent);
	void OnMouseMovingWithLeftButton(SDL_Point sp);
	void OnMouseLButtonDown(SDL_Point sp);
	void OnMouseLButtonUp(SDL_Point sp);
protected:
	bool AddTrack(int x, int y);
	bool RemoveTrack();

public:
	bool Copy();
	bool Paste();
	bool Cut();

	bool Undo();
	bool Redo();

	bool Save();

	bool Play();
	bool Stop();

	AudioTrack* GetActiveTrack();



};

