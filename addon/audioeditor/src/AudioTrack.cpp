#include <iostream>
#include "AudioTrack.h"
#include "SDLUtils.h"
#include "AudioProject.h"
#include <algorithm>
AudioTrack::AudioTrack() {
	m_pWaveformSurface = NULL;
	locationRect = { 0 };
	Dragging = false;
}
AudioTrack::~AudioTrack() {
	
};

// bisearch 
int AudioTrack::GetObjectIndex(int timepos, POSTYPE& postype) {
	
	int size = m_AudioObjectList.size();
	if (size == 0) {
		postype = POSTYPE::obj_none;
		return -1;//��ʾû��
	}
	postype = POSTYPE::obj_on;

	int low = 0, high = m_AudioObjectList.size() - 1;
	int mid;
	while (low <= high) {
		mid = low + (high - low) / 2;
		int endtimeintrack = m_AudioObjectList[mid].GetEndTimeInTrack();
		if (timepos >= m_AudioObjectList[mid].m_startTimeInTrack && timepos < endtimeintrack)
			return mid;
		else if (endtimeintrack == timepos)
			return std::min(size - 1, mid + 1);
		else if(timepos<m_AudioObjectList[mid].m_startTimeInTrack)
			high = mid - 1;
		else if(timepos> endtimeintrack)
			low = mid + 1;
	}
	return -1;
}

	

int AudioTrack::Create(int x, int y,int width,int height) {


	void* pixels;
	int  depth, pitch;

	


	//SDLUtils::GetRGBA8888Data(pixels, width, height);
	SDLUtils::GetRGB888Data(pixels, width, height);
	pitch = width * 3;	// ÿ��ͼ����ռ�ֽ���
	depth =3 * 8;		// ÿ������ռλ��		

	int rmask, gmask, bmask, amask;
	//rmask = 0xFF000000; gmask = 0x00FF0000; bmask = 0x0000FF00; amask = 0x000000FF;	// RGBA8888ģʽ
	rmask = 0x0000FF; gmask = 0x0000FF00; bmask = 0x00FF30000; amask = 0x00000000;	// RGB8888ģʽ
	m_pWaveformSurface = SDL_CreateRGBSurfaceFrom(pixels, width, height, depth, pitch, rmask, gmask, bmask, amask);
	if (NULL == m_pWaveformSurface) {
		const char *err=SDL_GetError();
		return -1;

	}

	SDL_Rect sr;
	sr.x = 0; sr.y = 0; sr.w = width; sr.h = height;
	SDL_FillRect(m_pWaveformSurface, &sr, SDL_MapRGB(m_pWaveformSurface->format, 34, 37, 39));

	//SDLUtils::SDL_DrawLine(m_pWaveformSurface, 0, height / 2, width, height / 2, SDL_MapRGB(m_pWaveformSurface->format, 128, 255, 128));
	
	locationRect.x = x;
	locationRect.y = y;
	locationRect.w = width;
	locationRect.h = height;

	contentRect.x = 0;
	contentRect.y = 0;
	contentRect.w = width;
	contentRect.h = height;

	return true;
}
int AudioTrack::Destroy() {
	// for(int i=0; i<m_AudioObjectList.size();i++){
	// 	delete m_AudioObjectList[i];
	// }
	m_AudioObjectList.clear();
	if(m_pWaveformSurface)
		SDL_FreeSurface(m_pWaveformSurface);
	m_pWaveformSurface = NULL;
	return 0;

}


void AudioTrack::clean()
{
	return ;
}

//根据 输入文件的性质，创建 立体声 还是单声道的
int AudioTrack::Import(MediaFile* mf, int timeInTrack){
	char* pBuf;
	int bufSize;
	bool got = mf->GetWaveformBuffer(&pBuf, &bufSize);
	if (!got)
		return -1;
	int samplecount = bufSize / sizeof(WFM_Stereo_SampleBlock);
	
	AudioObject ao;
	ao.m_MediaFile = mf;
	ao.m_startIndexInWFMFile = 0;
	ao.m_EndIndexInWFMFile = samplecount - 1;
	ao.m_startTimeInTrack = timeInTrack;

	POSTYPE pt;
	int inx=GetObjectIndex(timeInTrack,pt);
	if (inx == -1) {
		m_AudioObjectList.push_back(ao);
	}
	Refresh();

	return 0;
}


//根据 单声道 还是立体声，去画 图
void AudioTrack::Refresh() {

	POSTYPE pt;
	int inx = GetObjectIndex(AudioProject::LeftTimeOnScreen, pt);
	if (inx == -1)
		return;
	SDL_FillRect(m_pWaveformSurface, &contentRect, SDL_MapRGB(m_pWaveformSurface->format, 34, 37, 39));

	int RightTimeOnScreen = AudioProject::LeftTimeOnScreen + contentRect.w*AudioProject::UPP;


	if (AudioProject::playingCursorTime > AudioProject::LeftTimeOnScreen&& AudioProject::playingCursorTime < RightTimeOnScreen) {
		
		double pos=((double)AudioProject::playingCursorTime - AudioProject::LeftTimeOnScreen) / AudioProject::UPP;
		SDLUtils::SDL_DrawLine(m_pWaveformSurface,(int)pos, 0, (int)pos, contentRect.h, SDL_MapRGB(m_pWaveformSurface->format, 255, 128, 128));

	}

	SDL_Rect sr = { AudioProject::RangeStartTime,0,AudioProject::RangeEndTime- AudioProject::RangeStartTime,10 };
	SDL_Rect tr= { AudioProject::LeftTimeOnScreen,0,RightTimeOnScreen- AudioProject::LeftTimeOnScreen,10 };

	SDL_Rect res;

	if (SDL_IntersectRect(&sr, &tr, &res)){

		int left = res.x;
		int right = res.x + res.w;

		//SDL_SetSurfaceBlendMode(m_pMaskSurface, SDL_BLENDMODE_ADD);
		//SDL_SetSurfaceColorMod(m_pWaveformSurface, 0, 30, 0);
		

		double rspos = ((double)left - AudioProject::LeftTimeOnScreen) / AudioProject::UPP;
		double repos = ((double)right - AudioProject::LeftTimeOnScreen) / AudioProject::UPP;

		//
		/*SDL_Rect sr;
		sr.x = 0; sr.y = 0; sr.w = repos - rspos; sr.h = contentRect.h;*/
		SDL_Rect tr;

		tr.x = rspos; tr.y = 0; tr.w = repos - rspos; tr.h = contentRect.h;

		SDL_FillRect(m_pWaveformSurface, &tr, SDL_MapRGB(m_pWaveformSurface->format, 60, 60, 60));
		//SDL_BlitSurface(m_pMaskSurface, &sr, m_pWaveformSurface, &tr);

		//SDL_SetSurfaceBlendMode(m_pWaveformSurface, SDL_BLENDMODE_NONE);
	//	SDL_SetSurfaceColorMod(m_pWaveformSurface, 0, 0, 0);



	}
	//track is stereo ~~~~~

	//在 object 中画
	/*SDLUtils::SDL_DrawLine(m_pWaveformSurface, 0, contentRect.h / 4, contentRect.w, contentRect.h / 4, SDL_MapRGB(m_pWaveformSurface->format, 128, 255, 128));
	SDLUtils::SDL_DrawLine(m_pWaveformSurface, 0, contentRect.h*3 / 4, contentRect.w, contentRect.h*3 / 4, SDL_MapRGB(m_pWaveformSurface->format, 128, 255, 128));
	SDLUtils::SDL_DrawLine(m_pWaveformSurface, 0, contentRect.h  / 2, contentRect.w, contentRect.h/ 2, SDL_MapRGB(m_pWaveformSurface->format, 128, 178, 128));*/


	int drawPixel = 0;
	for(int i= inx;i<m_AudioObjectList.size(); i++) {

		int startDrawTimeOnTrack = std::max(m_AudioObjectList[i].m_startTimeInTrack, AudioProject::LeftTimeOnScreen);

		bool b= m_AudioObjectList[i].Draw(m_pWaveformSurface, startDrawTimeOnTrack, drawPixel);
		
		//printf("track drawing object %d, pixel  %d  track width:%d \r\n", i,drawPixel, AudioProject::TrackScreenWidth);

		if (drawPixel >= AudioProject::TrackScreenWidth)
			break;

		
	}
	if (drawPixel < AudioProject::TrackScreenWidth) { //后面没有数据了，要话成
		int ddd = 0;
	}

	//printf("track fresh ok drawPixel: %d  track width:%d \r\n", drawPixel, AudioProject::TrackScreenWidth);

	//draw playing cursor

	
}
int AudioTrack::Copy(){
	return 0;
}
void AudioTrack::Move(int sIndex, int eIndex, int offset) {
	if (eIndex == -1)
		eIndex = m_AudioObjectList.size() - 1;
	for (int i = sIndex; i <= eIndex; i++) {
		m_AudioObjectList[i].m_startTimeInTrack += offset;
		if (m_AudioObjectList[i].m_startTimeInTrack < 0) {
			int gggg = 0;
		}

	}

}

int AudioTrack::Cut(int from, int to){
	/*
	1. get  postion 'from" of object index ，
	2. split the object 
	3. get position  'to' of object index
	4 split the index object
	4. remove
	*/


	POSTYPE pt;
	int si=GetObjectIndex(from, pt);
	if (si == -1)
		return -1;
	int ei = GetObjectIndex(to, pt);
	if (ei == -1)
		return -1;
	//保存当前的 range，track time，UPP 链表 等参数，以便能恢复现场。

	m_trackStash.Push(&m_AudioObjectList);

	int frames_from_offset = (int)((from - (double)m_AudioObjectList[si].m_startTimeInTrack) / m_AudioObjectList[si].m_MediaFile->GetDurOfFrame());
	int frames_to_offset = (int)((to - (double)m_AudioObjectList[ei].m_startTimeInTrack) / m_AudioObjectList[ei].m_MediaFile->GetDurOfFrame());
	//frames_offset = frames_offset;
	int fromEnergyIndex = m_AudioObjectList[si].m_startIndexInWFMFile + frames_from_offset;
	int toEnergyIndex = m_AudioObjectList[ei].m_startIndexInWFMFile + frames_to_offset;

	from = m_AudioObjectList[si].m_startTimeInTrack + frames_from_offset * m_AudioObjectList[si].m_MediaFile->GetDurOfFrame();
	to= m_AudioObjectList[ei].m_startTimeInTrack + frames_to_offset * m_AudioObjectList[ei].m_MediaFile->GetDurOfFrame();
	int range = to - from;// (toEnergyIndex - fromEnergyIndex)* m_AudioObjectList[si].m_MediaFile->GetDurOfFrame();
	
	if (range <= 0)
		return -1;

	if (si == ei) {  //在一个index 上
		//共4种情况
		
		int endtimeontrack = m_AudioObjectList[si].GetEndTimeInTrack();
		if (from == m_AudioObjectList[si].m_startTimeInTrack && to < endtimeontrack) {
			m_AudioObjectList[si].m_startTimeInTrack = to;
			m_AudioObjectList[si].m_startIndexInWFMFile = toEnergyIndex;
			Move(si , -1, -range);//向前移动
		}
		else if (from == m_AudioObjectList[si].m_startTimeInTrack && to == endtimeontrack) {
			Move(si + 1, -1, -range);//向前移动

			m_AudioObjectList.erase(m_AudioObjectList.begin() + si);
		}
		else if (from > m_AudioObjectList[si].m_startTimeInTrack&& to < endtimeontrack) {
			//当前的index 改开始位置
			AudioObject ao = m_AudioObjectList[si].Clone();

			m_AudioObjectList[si].m_startTimeInTrack = to;
			m_AudioObjectList[si].m_startIndexInWFMFile = toEnergyIndex;


			//插入新的
			ao.m_EndIndexInWFMFile = fromEnergyIndex;
			m_AudioObjectList.insert(m_AudioObjectList.begin() + si, ao);
			//后面的数据都提前
			Move(si+1, -1, -range);//因为，插了一个新的，所有 si+1,向前移动


		}
		else if (from > m_AudioObjectList[si].m_startTimeInTrack&& to == endtimeontrack) {
			m_AudioObjectList[si].m_EndIndexInWFMFile = fromEnergyIndex;
			Move(si + 1, -1, -range);//向前移动

		}
	}
	else {
		// si 所在的index 更改尾巴
		int si_endtimeontrack = m_AudioObjectList[si].GetEndTimeInTrack();

		int cut_startindex = 0;

		if (from == m_AudioObjectList[si].m_startTimeInTrack)
			cut_startindex = si;
		else if (from > m_AudioObjectList[si].m_startTimeInTrack&& from < si_endtimeontrack) {

			m_AudioObjectList[si].m_EndIndexInWFMFile = fromEnergyIndex;
			cut_startindex = si+1;
		}
		else
		{
			cut_startindex = si + 1;
		}

		int ei_endtimeontrack = m_AudioObjectList[ei].GetEndTimeInTrack();
		int cut_endindex = 0;

		if (to == m_AudioObjectList[ei].m_startTimeInTrack)
			cut_endindex = ei-1;
		else if (to > m_AudioObjectList[ei].m_startTimeInTrack&& to < ei_endtimeontrack) {
			m_AudioObjectList[ei].m_startTimeInTrack = to;
			m_AudioObjectList[ei].m_startIndexInWFMFile = toEnergyIndex;
			cut_endindex = ei-1;
		}
		else
		{
			cut_endindex = ei;
		}

		//移动后面的链表
		Move(cut_endindex + 1, -1, -range);

		//删除si+1 和 ei -1之间的链表
		if (cut_startindex <= cut_endindex)
		{
			m_AudioObjectList.erase(m_AudioObjectList.begin() + cut_startindex, m_AudioObjectList.begin() + cut_endindex + 1);

		}


	}


	/*for (int i = 0; i < m_AudioObjectList.size(); i++) {
		if (m_AudioObjectList[i].m_startTimeInTrack < 0) {
			int gggg = 0;
		}
		if (m_AudioObjectList[i].m_EndIndexInWFMFile * 24 > m_AudioObjectList[i].m_MediaFile->GetDuration())
		{
			int gggg = 0;
		}
	}*/
	AudioProject::DurationOfProject -= (to-from);

	AudioProject::RangeEndTime = AudioProject::RangeStartTime;
	Refresh();
	return 0;
}
int AudioTrack::Undo(){

	int res=m_trackStash.Pop(&m_AudioObjectList);
	if (res == 0)
		Refresh();
	return res ;
}
int AudioTrack::OnMouseLButtonDown(SDL_Point sp) {

	SDL_Point sr = locationPt2content(&sp);

	AudioProject::playingCursorTime = AudioProject::LeftTimeOnScreen+(int)(sr.x * AudioProject::UPP);
	AudioProject::RangeStartTime = AudioProject::playingCursorTime;
	AudioProject::RangeEndTime = AudioProject::playingCursorTime;
	Dragging = true;
	return 0;
}
int AudioTrack::OnMouseLButtonUp(SDL_Point sp) {
	Dragging = false;


	return 0;

}
int AudioTrack::OnMouseMove(SDL_Point sp) {
	if (Dragging) {
		SDL_Point sr = locationPt2content(&sp);

		int pos = AudioProject::LeftTimeOnScreen + (int)(sr.x * AudioProject::UPP);

		if (pos > AudioProject::RangeStartTime)
			AudioProject::RangeEndTime = std::min(AudioProject::DurationOfProject, pos);
		else if (pos < AudioProject::RangeStartTime) {
			AudioProject::RangeStartTime = std::max(0,pos);
		}


		Dragging = true;
	}
	return 0;

}

