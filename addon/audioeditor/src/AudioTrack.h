#pragma once
#include <SDL2/SDL_surface.h>
#include "MediaFile.h"
#include "AudioObject.h"
#include <vector>
#include "winui.h"
#include "TrackStash.h"

 enum class POSTYPE {obj_on,obj_before,obj_after,obj_none} ; //����һ��ʱ�䣬�õ��Ƕ��� һ��object�ϣ�
class AudioTrack: public WinUI
{
	SDL_Surface* m_pWaveformSurface; //����surface ���е�Ԫ�ض��������

	std::vector<AudioObject > m_AudioObjectList;//��ŵ� AudioObjectList

	bool Dragging;

	TrackStash m_trackStash;
public:
	AudioTrack();
	~AudioTrack();
private:
	int GetObjectIndex(int timepos,POSTYPE &postype); //����ʱ��λ�ã��õ���ǰ��AudioObject ����

public:
	void Refresh();//�ػ���ǰ��track

public :
	int Create(int x,int y,int width, int height);
	int Destroy();
	void clean();
	SDL_Surface* GetSurface() {
		return m_pWaveformSurface;

	}

	//�� λ�� position ���� ����һ��AudioObject ����
	void Move(int sIndex, int eIndex,int offset);
	int Import(MediaFile *mf, int timeInTrack);
	//int Split(int timeInTrack);

	
	int Copy();
	int Cut(int from,int to); //unit is ms 
	int Undo();
	int OnMouseLButtonDown(SDL_Point sp);
	int OnMouseLButtonUp(SDL_Point sp);
	int OnMouseMove(SDL_Point sp);

	int DataBegin(int posInTrack);
	int DataNext(int dataLen,std::vector<DATAINFO> datainfos);//得到一个链表，包括文件名，要读取的文件位置 和长度

};

