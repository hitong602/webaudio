#include <stdio.h>
#include <SDL2/SDL.h>
#include <string>
#include <iostream>
#include <emscripten.h>
#include <emscripten/bind.h>

#include "AudioProject.h"
#include "FileManager.h"
#include "MediaFile.h"

static AudioProject g_AudioProject;
static FileManager g_FileManager;

void loop()
{

	int i;
	SDL_Event event;
	/* Check for events */
	SDL_PollEvent(&event);

	g_AudioProject.handleEvent(event);
	g_AudioProject.Render();

}

extern "C" int main(int argc, char **argv)
{
	//   g_AudioProject.Create("zhdtest",800,600);
	//    printf("this is an audioeditor for every one!\n");
	emscripten_set_main_loop(loop, 0, 1);
	//loop();
}
EMSCRIPTEN_KEEPALIVE
extern "C" int CreateAudioEditor(int width, int height)
{
	printf("CreateAudioEditor width:%d height:%d!\n", width, height);

	if (g_AudioProject.Create("zhdtest", width, height) == 0)
	{
	}
	return 0;
}
EMSCRIPTEN_KEEPALIVE
extern "C" int UnloadAudioEditor()
{

	g_AudioProject.Destroy();
	g_FileManager.Destroy();

	return 0;
}
/*
typedef struct _AudioFileInfo
{
	int sampleRate;
	int channelNum;
	int bitrate;
	int codeID;
	int duration;
	int  totalFrames; // by this paramete ,fps,spf can be calc
	
} AudioFileInfo;
*/
EMSCRIPTEN_KEEPALIVE
extern "C" 
void Cut(){
	g_AudioProject.Cut();
}
EMSCRIPTEN_KEEPALIVE
extern "C" 
void Undo(){
	g_AudioProject.Undo();
}

EMSCRIPTEN_KEEPALIVE
extern "C" 
int ImportFile(char *key, int sampleRate,int channelNum,int bitrate,int codeID,int duration,int  totalFrames,char *energybuf, int energylen)
{
	std::cout << "energybuf:" << (int)energybuf << std::endl;
	std::cout << "len:" << energylen << std::endl;
	//std::cout << "AudioFileInfo:" << afi << std::endl;

	g_FileManager.earse(key);
	MediaFile *mf = g_FileManager.Add(key);
	std::cout << "MediaFile:" << (int)mf << std::endl;

	// AudioFileInfo *afi_header = (AudioFileInfo *)energybuf;
	// WFM_Stereo_SampleBlock *energybody = (WFM_Stereo_SampleBlock *)(energybuf + sizeof(AudioFileInfo));
	// mf->m_pEnergyBuf = (WFM_Stereo_SampleBlock *)energybody;
	// mf->m_nEnergyBytes = energylen - sizeof(AudioFileInfo);

	WFM_Stereo_SampleBlock *energybody = (WFM_Stereo_SampleBlock *)(energybuf);
	mf->m_pEnergyBuf = (WFM_Stereo_SampleBlock *)energybody;
	mf->m_nEnergyBytes = energylen;

	mf->m_FileInfo.sampleRate = sampleRate;//*afi_header;
	mf->m_FileInfo.channelNum = channelNum;//*afi_header;
	mf->m_FileInfo.bitrate = bitrate;//*afi_header;
	mf->m_FileInfo.codeID = codeID;//*afi_header;
	mf->m_FileInfo.duration = duration;//*afi_header;
	mf->m_FileInfo.totalFrames = totalFrames;//*afi_header;


	std::cout << "samplerate:" << sampleRate <<"totalFram:"<<totalFrames <<"codeid:"<<codeID<< std::endl;

	return g_AudioProject.importFile(mf);
	
}
// EMSCRIPTEN_BINDINGS(AudioEditor)
// {
// 	emscripten::value_array<AudioFileInfo>("AudioFileInfo")
// 		.element(&AudioFileInfo::sampleRate)
// 		.element(&AudioFileInfo::channelNum)
// 		.element(&AudioFileInfo::bitrate)
// 		.element(&AudioFileInfo::codeID)
// 		.element(&AudioFileInfo::duration);

// 	emscripten::function("ImportFile", &ImportFile, emscripten::allow_raw_pointers());
// }
