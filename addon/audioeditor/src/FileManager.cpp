#include  "FileManager.h"
#include "MediaFile.h"
FileManager::FileManager(){

}
FileManager::~FileManager(){

  Destroy();
}

MediaFile *  FileManager::Add(std::string key){

    std::map<std::string,MediaFile *>::iterator it=m_mapFiles.find(key);
    if(it == m_mapFiles.end()){
        MediaFile * mf=new MediaFile();
        m_mapFiles.insert(std::pair<std::string,MediaFile *>(key,mf)); //增加
        return mf;
    }else{
        return it->second;
    }

}
void FileManager::earse(std::string key){
    std::map<std::string,MediaFile *>::iterator it=m_mapFiles.find(key);
    if(it != m_mapFiles.end()){
        delete it->second;
        m_mapFiles.erase(it);

    }

}
MediaFile * FileManager::Get(std::string key){
    std::map<std::string,MediaFile *>::iterator it=m_mapFiles.find(key);
    if(it == m_mapFiles.end()){
        return NULL;
    }else
    {
        return it->second;
    }


}
void FileManager::Destroy(){
    std::map<std::string,MediaFile *>::iterator it = m_mapFiles.begin();
    for (it=m_mapFiles.begin(); it!=m_mapFiles.end(); ++it){
        delete it->second;
    }
    m_mapFiles.clear();
}

