#pragma once
#include <map>
#include <string>

class MediaFile;
class FileManager{
    std::map<std::string,MediaFile *> m_mapFiles;//所有使用的meidafile 集合
public:
    FileManager();
    ~FileManager();

public:
    MediaFile *  Add(std::string key);
    MediaFile *Get(std::string key);
    void earse( std::string key);

    void Destroy();


};
