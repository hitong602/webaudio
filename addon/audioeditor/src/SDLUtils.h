#pragma once
#include <stdlib.h>
#include <SDL2/SDL_surface.h>
class SDLUtils
{
public:
	static void GetRGB888Data(void*& pixels, int& width, int& height);
	static void GetRGBA8888Data(void*& pixels, int& width, int& height);

	static void PutPixel32_nolock(SDL_Surface* surface, int x, int y, Uint32 color);
	static void PutPixel24_nolock(SDL_Surface* surface, int x, int y, Uint32 color);
	static void PutPixel16_nolock(SDL_Surface* surface, int x, int y, Uint32 color);
	static void PutPixel8_nolock(SDL_Surface* surface, int x, int y, Uint32 color);
	static void PutPixel32(SDL_Surface* surface, int x, int y, Uint32 color);
	static void PutPixel24(SDL_Surface* surface, int x, int y, Uint32 color);
	static void PutPixel16(SDL_Surface* surface, int x, int y, Uint32 color);
	static void PutPixel8(SDL_Surface* surface, int x, int y, Uint32 color);

	static void SDL_DrawLine(SDL_Surface* pDestination, int pX1, int pY1, int pX2, int pY2, Uint32 pColour);
	static void FillRect(SDL_Surface* pDestination, int x, int y,int width, int height, int color = 1);
	static void SDL_DrawReverseLine(SDL_Surface* pDestination, int pX1, int pY1, int pX2, int pY2, Uint32 bkColour, Uint32 frColour, Uint32 frColour2);

	static void PutPixel24_2_nolock(SDL_Surface* surface, int x, int y, Uint32 bkColour, Uint32 frColour, Uint32 frColour2);

};

