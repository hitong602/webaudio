#include "TrackStash.h"
#include "AudioProject.h"
#define MAX_STASH_NUM  10
TrackStash::TrackStash() {

}
TrackStash::~TrackStash() {

}
void TrackStash::Push(std::vector<AudioObject>* aol) {
	StashItem *si=new StashItem();
	si->DurationOfProject = AudioProject::DurationOfProject;
	si->LeftTimeOnScreen = AudioProject::LeftTimeOnScreen;
	si->RangeStartTime = AudioProject::RangeStartTime;
	si->RangeEndTime = AudioProject::RangeEndTime;
	si->UPP = AudioProject::UPP;
	si->DurationOfProject = AudioProject::DurationOfProject;

	si->m_AudioObjectList.assign(aol->begin(), aol->end());
	if (m_Stash.size() > MAX_STASH_NUM) {
		StashItem* si = m_Stash.back();
		delete si;
		m_Stash.pop_back();
	}
		
	m_Stash.push_front(si);
}
int  TrackStash::Pop(std::vector<AudioObject>* aol) {

	if (m_Stash.size() == 0)
		return -1;
	StashItem *si=m_Stash.front();
	m_Stash.pop_front();
	aol->clear();
	aol->assign(si->m_AudioObjectList.begin(), si->m_AudioObjectList.end());

	AudioProject::DurationOfProject = si->DurationOfProject;
	AudioProject::LeftTimeOnScreen = si->LeftTimeOnScreen;
	AudioProject::RangeStartTime = si->RangeStartTime;
	AudioProject::RangeEndTime = si->RangeEndTime;
	AudioProject::UPP = si->UPP;
	AudioProject::DurationOfProject = si->DurationOfProject;
	delete si;
	return 0;

}