﻿#pragma once
#include <vector>
#include <list>

#include "AudioObject.h"


class StashItem {
public:
	StashItem(){}
	~StashItem(){}
public:
	 int playingCursorTime;// unit is MS
	 int RangeStartTime;
	 int RangeEndTime;
	 int LeftTimeOnScreen; // time on the screen left, unit is MS , 
	 double UPP;// dur per pixel    
	 int DurationOfProject; // duration of the project 

	 std::vector<AudioObject> m_AudioObjectList;//AudioObjectList

};

class TrackStash {

	std::list< StashItem *> m_Stash;
public:

	TrackStash();
	~TrackStash();

	void Push(std::vector<AudioObject>* aol);
	int Pop(std::vector<AudioObject>* aol);
};