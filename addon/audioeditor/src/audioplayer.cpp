#include "audioplayer.h"



AudioPlayer::AudioPlayer() {
	devID = 0;
	playAudioSpec = { 0 };
}
AudioPlayer::~AudioPlayer() {
}
int AudioPlayer::OpenDevice(SDL_AudioCallback audiocallback, int samplerate, int channelnum) {
	SDL_AudioSpec want;

	SDL_memset(&want, 0, sizeof(want)); /* or SDL_zero(want) */
	want.freq = samplerate;
	want.format = AUDIO_F32;
	want.channels = channelnum;
	want.samples = 4096;
	want.callback = audiocallback; /* you wrote this function elsewhere -- see SDL_AudioSpec for details */

	devID = SDL_OpenAudioDevice(NULL, 0, &want, &playAudioSpec, SDL_AUDIO_ALLOW_FORMAT_CHANGE);
	if (devID == 0) {
		return -1;
	}
	else {

		SDL_PauseAudioDevice(devID, 1); /* start audio playing. */
	}
	return 0;

}

int AudioPlayer::CloseDevice() {
	if (devID != 0)
	{
		SDL_CloseAudioDevice(devID);
		devID = 0;

	}
	return 0;

}
int AudioPlayer::Start() {
	if (devID != 0)
	{
		SDL_PauseAudioDevice(devID, 0);


	}
	return 0;
}
int AudioPlayer::Stop() {
	if (devID != 0)
	{
		SDL_PauseAudioDevice(devID, 1);
	

	}
	return 0;
}