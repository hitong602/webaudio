#pragma once
#include "SDL2/SDL_audio.h"
class AudioPlayer {
	SDL_AudioDeviceID devID;
	SDL_AudioSpec  playAudioSpec;
public:
	AudioPlayer();
	~AudioPlayer();
public:
	int OpenDevice(SDL_AudioCallback audiocallback, int samplerate, int channelnum);
	int CloseDevice();



	int Start();
	int Stop();

};
