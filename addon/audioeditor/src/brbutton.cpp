#include "brbutton.h"
#include "SDL_image.h"
CBrButton::CBrButton() {
	m_bShow = true;
	m_bListen = true;
	m_status = BrButtonStatus::BR_BUTTON_STATUS_OUT;
	m_func_pressed = NULL;
	m_iId = 0;
	pSurfaceIn = NULL;
	pSurfaceOut = NULL;
	pSurfacePre = NULL;
	m_pSurface = NULL;
}

CBrButton::~CBrButton() {
	if (NULL != pSurfaceIn)
	{
		SDL_FreeSurface(pSurfaceIn);
	}
	if (NULL != pSurfaceOut)
	{
		SDL_FreeSurface(pSurfaceOut);
	}
	if (NULL != pSurfacePre)
	{
		SDL_FreeSurface(pSurfacePre);
	}

}
void CBrButton::setFuncPressed(std::function<void(const CBrButton * pButton)> call) {
	m_func_pressed = call;
}

bool CBrButton::bPointIn(int x, int y) {
	if (x >= contentRect.x && x <= (contentRect.x + contentRect.w) && y >= contentRect.y && y <= (contentRect.y + contentRect.h))
	{
		return true;
	}
	else
	{
		return false;
	}

}
bool CBrButton::handleEvent(SDL_Event& stEvent) {
	if (!m_bListen) {
		return false;
	}

	bool bRet = false;
	if (stEvent.type == SDL_MOUSEMOTION) {
		if (m_status == BrButtonStatus::BR_BUTTON_STATUS_IN
			|| m_status == BrButtonStatus::BR_BUTTON_STATUS_OUT) {
			if (bPointIn(stEvent.motion.x, stEvent.motion.y)) {
				m_status = BrButtonStatus::BR_BUTTON_STATUS_IN;
				bRet = true;
			}
			else {
				m_status = BrButtonStatus::BR_BUTTON_STATUS_OUT;
			}
		}
		else if (m_status == BrButtonStatus::BR_BUTTON_STATUS_PRESSED) {
			if (!bPointIn(stEvent.motion.x, stEvent.motion.y)) {
				m_status = BrButtonStatus::BR_BUTTON_STATUS_OUT;
			}
		}

	}
	else if (stEvent.type == SDL_MOUSEBUTTONDOWN) {
		if (bPointIn(stEvent.button.x, stEvent.button.y)) {
			m_status = BrButtonStatus::BR_BUTTON_STATUS_PRESSED;
			bRet = true;
		}
	}
	else if (stEvent.type == SDL_MOUSEBUTTONUP) {
		BrButtonStatus oldStatus = m_status;
		if (bPointIn(stEvent.button.x, stEvent.button.y)) {
			m_status = BrButtonStatus::BR_BUTTON_STATUS_IN;
			bRet = true;
		}
		else {
			m_status = BrButtonStatus::BR_BUTTON_STATUS_OUT;
		}
		if (oldStatus == BrButtonStatus::BR_BUTTON_STATUS_PRESSED) {
			if (m_func_pressed != NULL) {
				m_func_pressed(this);
				bRet = true;
			}
		}
	}
	return bRet;
}

SDL_Surface* CBrButton::GetShowSurface() {
	if (m_status == BrButtonStatus::BR_BUTTON_STATUS_IN) {
		return pSurfaceIn;
	}
	else if (m_status == BrButtonStatus::BR_BUTTON_STATUS_PRESSED) {
		return pSurfacePre;
	}
	else {
		return pSurfaceOut;
	}
}

//void CBrButton::SetRect(const SDL_Rect& rc)
//{
//	m_rcButton = rc;
//}
//
//SDL_Rect CBrButton::GetRect()
//{
//	return m_rcButton;
//}

void CBrButton::Create(int id,SDL_Surface* pParentSurface, const char* inpath, const char* outpath, const char* pressedpath)
{
	m_iId = id;
	if (NULL != pParentSurface)
	{
		m_pSurface = pParentSurface;
		if(inpath)
			pSurfaceIn = IMG_Load(inpath);
		if(outpath)
			pSurfaceOut = IMG_Load(outpath);
		if(pressedpath)
			pSurfacePre = IMG_Load(pressedpath);
	}
}

void CBrButton::Draw()
{
	if (NULL != m_pSurface)
	{
		SDL_BlitSurface(GetShowSurface(), NULL, m_pSurface, &contentRect);//将文字复制到屏幕的surface上面

	}
}
