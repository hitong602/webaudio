#pragma once
#include "winui.h"
#include <functional>
#include <SDL2/SDL_surface.h>
#include <SDL2/SDL.h>
#define  ID_CUT  0x8000
#define  ID_COPY 0x8001
#define  ID_PASTE 0X8002
#define  ID_UNDO 0X8003

#define  ID_PLAY 0X8005
#define  ID_STOP 0X8006




 enum class BrButtonStatus {
	BR_BUTTON_STATUS_OUT = 1,
	BR_BUTTON_STATUS_IN = 2,
	BR_BUTTON_STATUS_PRESSED = 3,
} ;

class CBrButton :public WinUI {
public:
	/**��ť����ʾ����**/
//	SDL_Rect m_rcButton;
	/**��ť�Ƿ���ʾ**/
	bool m_bShow;
	/**��ť�Ƿ�����¼�**/
	bool m_bListen;
	/**��ť��ʶ**/
	int m_iId;

	CBrButton();
	~CBrButton();
	/*void CreateTexture(SDL_Surface* effectIn, SDL_Surface* effectOut,
		SDL_Surface* effectPressed);*/
	//�����¼�
	bool handleEvent(SDL_Event& stEvent);
	/**�ص�����**/
	void setFuncPressed(std::function<void(const CBrButton * pButton)>);

	/*void SetRect(const SDL_Rect& rc);
	SDL_Rect GetRect();*/

	void Create(int id,SDL_Surface* pParentSurface,const char * inpath,const char *outpath,const char *pressedpath);

	void Draw();

private:

	/**����ڰ�ť�ڵ���ʾ**/
	SDL_Surface* pSurfaceIn;
	/**����ڰ�ť�����ʾ**/
	SDL_Surface* pSurfaceOut;
	/**����ڰ�ť���µ���ʾ**/
	SDL_Surface* pSurfacePre;
	/**��굱ǰ״̬**/
	BrButtonStatus m_status;
	SDL_Surface* m_pSurface;


	bool bPointIn(int x, int y);
	std::function<void(const CBrButton * pButton)> m_func_pressed;
	//void (*m_func_pressed)(const CBrButton* pButton);
	SDL_Surface* GetShowSurface();
};

