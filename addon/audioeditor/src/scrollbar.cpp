﻿#include "scrollbar.h"
#include "SDLUtils.h"
#include "AudioProject.h"
#define STRIPE_WIDTH    10 
#define STRIPE_LINE_WIDHT 2
ScrollBar::ScrollBar() {
	mouselbtnpressed = false;
	m_nMaxPos = 100;
	
	m_pSurface = NULL;
	dragging_type = DRAGGING_TYPE::NONE;
	rcLeftStripe = { 0 };
	rcRightStripe = { 0 };
	m_ThrumbMinRange = 10000; // 一个屏幕最小显示100MS ,24*4,  //这个 要根据
	m_ThrumbStartPos = 0;
	m_ThrumbEndPos = 100;
	locationRect = { 0 };
	contentRect = { 0 };
	lastDragingPos = 0;
}
ScrollBar::~ScrollBar() {

}
int ScrollBar::Create(int x, int y, int width, int height) {
	void* pixels;
	int  depth, pitch;

	//SDLUtils::GetRGBA8888Data(pixels, width, height);
	SDLUtils::GetRGB888Data(pixels, width, height);
	pitch = width * 3;	// ÿ��ͼ����ռ�ֽ���
	depth = 3 * 8;		// ÿ������ռλ��		

	int rmask, gmask, bmask, amask;
	//rmask = 0xFF000000; gmask = 0x00FF0000; bmask = 0x0000FF00; amask = 0x000000FF;	// RGBA8888ģʽ
	rmask = 0x0000FF; gmask = 0x0000FF00; bmask = 0x00FF30000; amask = 0x00000000;	// RGB8888ģʽ
	m_pSurface = SDL_CreateRGBSurfaceFrom(pixels, width, height, depth, pitch, rmask, gmask, bmask, amask);
	if (NULL == m_pSurface) {
		const char* err = SDL_GetError();
		return -1;

	}
	SDL_Rect sr;
	sr.x = 0; sr.y = 0; sr.w = width; sr.h = height;
	SDL_FillRect(m_pSurface, &sr, SDL_MapRGB(m_pSurface->format, 128, 128, 128));

	//一个地板，一个滚动条
	locationRect.x = x;
	locationRect.y = y;
	locationRect.w = width;
	locationRect.h = height;

	contentRect.x = 0;
	contentRect.y = 0;
	contentRect.w = width;
	contentRect.h = height;

	// handcursor = SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_HAND);
	// arrowcursor = SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_ARROW);


	return true;
}

int ScrollBar::DrawThrumb() {

	
	int startPixel = (int)((double)m_ThrumbStartPos / m_nMaxPos * locationRect.w);

	int endPixel= (int)((double)m_ThrumbEndPos / m_nMaxPos * locationRect.w);


	SDL_LockSurface(m_pSurface);

	SDL_FillRect(m_pSurface, &contentRect, SDL_MapRGB(m_pSurface->format, 128, 128, 128));

	//draw left stripe 
	//SDL_Rect sr;
	rcLeftStripe.x = startPixel; rcLeftStripe.y = 0; rcLeftStripe.w = STRIPE_WIDTH; rcLeftStripe.h = locationRect.h;
	SDL_FillRect(m_pSurface, &rcLeftStripe, SDL_MapRGB(m_pSurface->format, 128, 255, 128));

	for (int i = 0; i < STRIPE_LINE_WIDHT; i++) {
		SDLUtils::SDL_DrawLine(m_pSurface, startPixel, i, endPixel, i, SDL_MapRGB(m_pSurface->format, 128, 255, 128));

	}

	for (int i = 0; i < STRIPE_LINE_WIDHT; i++) {
		SDLUtils::SDL_DrawLine(m_pSurface, startPixel, locationRect.h -1 - i, endPixel, locationRect.h -1 - i, SDL_MapRGB(m_pSurface->format, 128, 255, 128));

	}

	rcThrumb.x = startPixel; rcThrumb.y = 2; rcThrumb.w = endPixel - startPixel; rcThrumb.h = locationRect.h - 4;

	rcRightStripe.x = endPixel - STRIPE_WIDTH; rcRightStripe.y = 0; rcRightStripe.w = STRIPE_WIDTH; rcRightStripe.h = locationRect.h;
	SDL_FillRect(m_pSurface, &rcRightStripe, SDL_MapRGB(m_pSurface->format, 128, 255, 128));

	//SDL_LockSurface(m_pSurface);
	SDL_UnlockSurface(m_pSurface);

	return 0;
}

int ScrollBar::setMaxPos(int maxPos, bool refresh) {

	if (maxPos == m_nMaxPos || locationRect.w ==0)
		return -1;
	m_nMaxPos = maxPos;

	m_ThrumbMinRange=(int)(m_nMaxPos  * 25/ contentRect.w);  //25 个像素
	//m_ThrumbMinRange = m_ThrumbMinRange * (int)AudioProject::EditUnit;

	if (m_ThrumbEndPos > m_nMaxPos)
		m_ThrumbEndPos = m_nMaxPos;
	if (m_ThrumbStartPos > m_nMaxPos)
		m_ThrumbStartPos = 0;

	

	if(refresh)
		DrawThrumb();
	
	return 0;
}
int ScrollBar::setPos(int pos, bool refresh ) {
	if (pos == m_ThrumbStartPos || locationRect.w == 0)
		return -1;
	m_ThrumbStartPos = pos;
	if(refresh)
		DrawThrumb();

	return 0;
}
int ScrollBar::setThrumbRange(int start, int end, bool refresh) {
	if ((m_ThrumbStartPos == start && m_ThrumbEndPos ==end) || locationRect.w == 0)
		return -1;
	if ((end - start) < m_ThrumbMinRange)
		return -1;
	m_ThrumbStartPos = start;
	m_ThrumbEndPos = end;
	if(refresh)
		DrawThrumb();	
	return 0;
}
int ScrollBar::Destroy() {
	if (m_pSurface)
		SDL_FreeSurface(m_pSurface);
	m_pSurface = NULL;
	return 0;

}
void ScrollBar::OnMouseLButtonDown(SDL_Point sp) {
	SDL_Point sr= locationPt2content(&sp);

	if (SDL_PointInRect(&sr, &rcLeftStripe)) {
	//	SDL_SetCursor(handcursor);
		dragging_type = DRAGGING_TYPE::LEFT_STRIPE;

	}
	else if (SDL_PointInRect(&sr, &rcRightStripe)) {
		dragging_type = DRAGGING_TYPE::RIGHT_STRIPE;
		//SDL_SetCursor(handcursor);

	}
	else if (SDL_PointInRect(&sr, &rcThrumb)) {
		dragging_type = DRAGGING_TYPE::THRUMB;
	//	SDL_SetCursor(handcursor);

	}
	//lastMovingPoint= sr;
	lastDragingPos = (double)sr.x / contentRect.w * m_nMaxPos;

}
void ScrollBar::OnMouseLButtonUp(SDL_Point sp) {
	dragging_type = DRAGGING_TYPE::NONE;
	//SDL_SetCursor(arrowcursor);
}

// 每次移动是
bool ScrollBar::OnMouseMoving(SDL_Point sp) {
	if (dragging_type == DRAGGING_TYPE::NONE)
		return false;
	if (dragging_type == DRAGGING_TYPE::LEFT_STRIPE ) //用偏移量
	{
		SDL_Point sr = locationPt2content(&sp);

		int thrumbStartPixelPos = (int)((double)m_ThrumbStartPos / m_nMaxPos * contentRect.w);
		int pixeloffset = sr.x - thrumbStartPixelPos;// lastMovingPoint.x; //向右移动，偏移量是正数，向左移动是负数

		if (pixeloffset > 100) {
			int gggg = 0;
		}

		int nNewPos = m_ThrumbStartPos + (double)pixeloffset / contentRect.w * m_nMaxPos;
		if (nNewPos <= 0)
			nNewPos = 0;

		if ((nNewPos + m_ThrumbMinRange) > m_ThrumbEndPos)
			nNewPos = m_ThrumbEndPos - m_ThrumbMinRange;
	
		//lastMovingPoint = sr;

		if (nNewPos != m_ThrumbStartPos)
		{
			m_ThrumbStartPos = nNewPos;
			DrawThrumb();
			return true;
		}
		

	}
	else if (dragging_type == DRAGGING_TYPE::RIGHT_STRIPE) { //必须用偏移量
		SDL_Point sr = locationPt2content(&sp);
	
		int thrumbendPixelPos = (int)((double)m_ThrumbEndPos / m_nMaxPos * contentRect.w);

		int pixeloffset = sr.x - thrumbendPixelPos;// lastMovingPoint.x;

		if (pixeloffset > 100) {
			int gggg = 0;
		}

		int nNewPos = m_ThrumbEndPos + (double)pixeloffset / contentRect.w * m_nMaxPos;
		if (nNewPos <= m_ThrumbStartPos + m_ThrumbMinRange)
			nNewPos = m_ThrumbStartPos + m_ThrumbMinRange;

		if (nNewPos>= m_nMaxPos)
			nNewPos = m_nMaxPos;
		
	//	lastMovingPoint = sr;

		if(nNewPos!= m_ThrumbEndPos)
		{
			m_ThrumbEndPos = nNewPos;// (nNewPos / (int)AudioProject::EditUnit)* AudioProject::EditUnit;
			DrawThrumb();
			return true;
		}

		

	}
	else if (dragging_type == DRAGGING_TYPE::THRUMB) {
		SDL_Point sr = locationPt2content(&sp);
		//int offsetOfPixel = sr.x - lastMovingPoint.x;

		int thrumbPos = (int)((double)sr.x / contentRect.w * m_nMaxPos);


	//	int posOffset = (int)((double)offsetOfPixel / contentRect.w * m_nMaxPos);

		int posOffset = thrumbPos - lastDragingPos;
		int nThrumbRange = m_ThrumbEndPos - m_ThrumbStartPos;

		if (posOffset < 0) {
			int newSPos = m_ThrumbStartPos + posOffset;
			if (newSPos < 0)
				newSPos = 0;
			if (m_ThrumbStartPos != newSPos) {
				m_ThrumbStartPos = newSPos;
				m_ThrumbEndPos = newSPos + nThrumbRange;
			}
		}
		else if (posOffset > 0) {
			int newEPos = m_ThrumbEndPos + posOffset;
			if (newEPos >m_nMaxPos)
				newEPos = m_nMaxPos;
			if (m_ThrumbEndPos != newEPos) {
				m_ThrumbStartPos = newEPos - nThrumbRange;
				m_ThrumbEndPos = newEPos;
			}

		}
		DrawThrumb();

		//lastMovingPoint = sr;
		lastDragingPos = thrumbPos;
		return true;



	}
	return false;

	

}