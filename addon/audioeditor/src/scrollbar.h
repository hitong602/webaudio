﻿#pragma once
#include <SDL2/SDL_mouse.h>
#include <SDL2/SDL_surface.h>
#include "winui.h"

enum class  DRAGGING_TYPE{LEFT_STRIPE,THRUMB,RIGHT_STRIPE,NONE};

class ScrollBar :public WinUI
{
	
	bool mouselbtnpressed;


	double lastDragingPos; // used by gading thrumb
	//SDL_Point lastMovingPoint;
	// SDL_Cursor* handcursor;
	// SDL_Cursor* arrowcursor;

public:
	//int m_nPos; //当前的位置,虚拟位置，可以是时间 MS
	int m_nMaxPos; //最大位置 ，虚拟位置

	int m_ThrumbMinRange;
	int m_ThrumbStartPos; // 虚拟范围
	int m_ThrumbEndPos; //滑块最小的
	SDL_Surface* m_pSurface; //����surface ���е�Ԫ�ض��������

	SDL_Rect rcLeftStripe;
	SDL_Rect rcRightStripe;
	SDL_Rect rcThrumb;

	enum DRAGGING_TYPE dragging_type;

	


	

private:
	int DrawThrumb();
public:
	ScrollBar();
	~ScrollBar();


	int Create(int x,int y,int width,int height);
	int Destroy();

	int setMaxPos( int maxPos,bool refresh=false); //
	int setPos(int pos, bool refresh = false);
	int setThrumbRange(int start,int end, bool refresh = false);

	void OnMouseLButtonDown(SDL_Point sp);
	void OnMouseLButtonUp(SDL_Point sp);
	bool OnMouseMoving(SDL_Point sp);




};
