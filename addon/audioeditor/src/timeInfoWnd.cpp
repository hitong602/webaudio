﻿#include "timeInfoWnd.h"
#include "SDLUtils.h"
#include "AudioProject.h"
#include <iostream>

#define MARGIN_X   5
#define MARGIN_Y   5

TimeInfoWnd::TimeInfoWnd() {
	font = NULL;
}
TimeInfoWnd::~TimeInfoWnd() {

}

int TimeInfoWnd::Destroy() {
	if (m_pSurface)
		SDL_FreeSurface(m_pSurface);
	m_pSurface = NULL;
	if (font) {
		TTF_CloseFont(font);
	}

	return 0;
}

void TimeInfoWnd::Refresh() {

	SDL_Color red = { 255,0,0 ,0 };//颜色
	SDL_Color green = { 0,255,0 ,0 };//颜色

	SDL_Color yellow = { 255,255,0 ,0 };//颜色

	SDL_FillRect(m_pSurface, &contentRect, SDL_MapRGB(m_pSurface->format, bkColor.r, bkColor.g, bkColor.b));

	DrawTimeRect(AudioProject::playingCursorTime, playingCursorRect, red);
	DrawTimeRect(AudioProject::RangeStartTime, rangeStartRect, green);
	DrawTimeRect(AudioProject::RangeEndTime, rangeEndRect, yellow);

	brPlayBtn.Draw();
	brStopBtn.Draw();
	brCutBtn.Draw();
	/*brCopyBtn.Draw();
	brPasteBtn.Draw();*/
}


int TimeInfoWnd::Create(int x, int y, int width, int height, std::function<void(const CBrButton * pButton)> call) {

	font = TTF_OpenFont("assets/eraria_1.ttf", 30);
	if (font == NULL) {
		return -1;
	}

	void* pixels;
	int  depth, pitch;

	//SDLUtils::GetRGBA8888Data(pixels, width, height);
	SDLUtils::GetRGB888Data(pixels, width, height);
	pitch = width * 3;	// ÿ��ͼ����ռ�ֽ���
	depth = 3 * 8;		// ÿ������ռλ��		

	int rmask, gmask, bmask, amask;
	//rmask = 0xFF000000; gmask = 0x00FF0000; bmask = 0x0000FF00; amask = 0x000000FF;	// RGBA8888ģʽ
	rmask = 0x0000FF; gmask = 0x0000FF00; bmask = 0x00FF30000; amask = 0x00000000;	// RGB8888ģʽ
	m_pSurface = SDL_CreateRGBSurfaceFrom(pixels, width, height, depth, pitch, rmask, gmask, bmask, amask);
	if (NULL == m_pSurface) {
		const char* err = SDL_GetError();

		return -1;

	}
	SDL_Rect sr;
	sr.x = 0; sr.y = 0; sr.w = width; sr.h = height;
	bkColor = { 33, 33, 33 ,0 };
	SDL_FillRect(m_pSurface, &sr, SDL_MapRGB(m_pSurface->format, bkColor.r, bkColor.g, bkColor.b));

	//一个地板，一个滚动条
	locationRect.x = x;
	locationRect.y = y;
	locationRect.w = width;
	locationRect.h = height;

	contentRect.x = 0;
	contentRect.y = 0;
	contentRect.w = width;
	contentRect.h = height;

	int w, h;
	TTF_SizeText(font, "00:00:00.000", &w, &h);


	playingCursorRect.x = MARGIN_X;
	playingCursorRect.y = MARGIN_Y;

	playingCursorRect.w = w;
	playingCursorRect.h = h - MARGIN_X - MARGIN_Y;


	rangeStartRect.x = playingCursorRect.x + playingCursorRect.w + MARGIN_X;
	rangeStartRect.y = MARGIN_Y;
	rangeStartRect.w = playingCursorRect.w;
	rangeStartRect.h = playingCursorRect.h;

	rangeEndRect.x = rangeStartRect.x + rangeStartRect.w + MARGIN_X;
	rangeEndRect.y = MARGIN_Y;
	rangeEndRect.w = playingCursorRect.w;
	rangeEndRect.h = playingCursorRect.h;

	SDL_Color red = { 255,0,0 ,0 };//颜色
	SDL_Color green = { 0,255,0 ,0 };//颜色

	SDL_Color yellow = { 255,255,0 ,0 };//颜色


	DrawTimeRect(0, playingCursorRect, red);
	DrawTimeRect(0, rangeStartRect, green);
	DrawTimeRect(0, rangeEndRect, yellow);

	SDL_Rect rc;
	rc.x = rangeEndRect.x + rangeEndRect.w + 100; //确定
	rc.y = 0;
	rc.w = 50;
	rc.h = 50;
	brPlayBtn.contentRect = rc;

	brPlayBtn.Create(ID_PLAY,m_pSurface, "assets/play_in.png", "assets/play_out.png", "assets/play_press.png");
	brPlayBtn.setFuncPressed(call);

	rc.x = rc.x + rc.w + MARGIN_X; //确定
	rc.y = 0;
	rc.w = 50;
	rc.h = 50;
	brStopBtn.contentRect = rc;

	brStopBtn.Create(ID_STOP, m_pSurface, "assets/stop_in.png", "assets/stop_out.png", "assets/stop_press.png");
	brStopBtn.setFuncPressed(call);

	rc.x = rc.x + rc.w + MARGIN_X; //确定
	rc.y = 0;
	rc.w = 50;
	rc.h = 50;
	brCutBtn.contentRect = rc;

	brCutBtn.Create(ID_CUT, m_pSurface, "assets/cut_hover.png", "assets/cut_normal.png", "assets/cut_press.png");
	brCutBtn.setFuncPressed(call);

	//rc.x = rc.x + rc.w + MARGIN_X; //确定
	//rc.y = 0;
	//rc.w = 50;
	//rc.h = 50;
	//brCopyBtn.SetRect(rc);


	//rc.x = rc.x + rc.w + MARGIN_X; //确定
	//rc.y = 0;
	//rc.w = 50;
	//rc.h = 50;
	//brPasteBtn.SetRect(rc);

	return true;
}
 bool TimeInfoWnd::handleEvent(SDL_Event& stEvent) {

	 SDL_Event e=stEvent;

	 e.motion.x-=locationRect.x;
	 e.motion.y -= locationRect.y;
	 brPlayBtn.handleEvent(e);
	brStopBtn.handleEvent(e);
	brCutBtn.handleEvent(e);
	return true;
}
 

void TimeInfoWnd::DrawTimeRect(int Time, SDL_Rect rc, SDL_Color color) {
	if (!font)
		return;
	
	SDL_Surface* text_surface;
	char s[50];
	int secs = Time / 1000;
	int millsecs = Time % 1000;
	int hour = secs / 3600;
	int min = (secs % 3600) / 60;
	int sec = (secs % 3600) % 60;
	snprintf(s, 50, "%02d:%02d:%02d.%03d", hour, min, sec, millsecs);
	 int w, h;
	 TTF_SizeText(font, s, &w, &h);
	text_surface = TTF_RenderUTF8_Solid(font, s, color);


	if (text_surface != NULL)
	{
		//画 背景
		//SDL_FillRect(m_pSurface, &rc, SDL_MapRGB(m_pSurface->format, 33, 128, 33));

		// 写数字
		SDL_BlitSurface(text_surface, NULL, m_pSurface, &rc);//将文字复制到屏幕的surface上面
		SDL_FreeSurface(text_surface);
		//std::cout << "SDL_BlitSurface ok"  <<std::endl;

	}
	
}

