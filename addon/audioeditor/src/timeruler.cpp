﻿#include "timeruler.h"
#include "SDLUtils.h"
#include <math.h>
#include "AudioProject.h"
#include <string>
#include <iostream>


TimeRuler::TimeRuler() {

	m_pSurface = NULL;
	locationRect = { 0 };
	contentRect = { 0 };
	font =NULL;


}
TimeRuler::~TimeRuler() {

	Destroy();
	TTF_Quit();  



}
int TimeRuler::Create(int x, int y, int width, int height) {

	//if (TTF_Init() == 0)
	//{
	//	font = TTF_OpenFont("assets/eraria_1.ttf", 15);
	//	//font = TTF_OpenFont("assets/FreeSans.ttf", 12);
	//}
	font = TTF_OpenFont("assets/eraria_1.ttf", 15);


	//this->font = font;

	if(font ==NULL){
		std::cout << "font create error"  << std::endl;

		return -1;
	}
		
	void* pixels;
	int  depth, pitch;

	//SDLUtils::GetRGBA8888Data(pixels, width, height);
	SDLUtils::GetRGB888Data(pixels, width, height);
	pitch = width * 3;	// ÿ��ͼ����ռ�ֽ���
	depth = 3 * 8;		// ÿ������ռλ��		

	int rmask, gmask, bmask, amask;
	//rmask = 0xFF000000; gmask = 0x00FF0000; bmask = 0x0000FF00; amask = 0x000000FF;	// RGBA8888ģʽ
	rmask = 0x0000FF; gmask = 0x0000FF00; bmask = 0x00FF30000; amask = 0x00000000;	// RGB8888ģʽ
	m_pSurface = SDL_CreateRGBSurfaceFrom(pixels, width, height, depth, pitch, rmask, gmask, bmask, amask);
	if (NULL == m_pSurface) {
		const char* err = SDL_GetError();
		std::cout << err  << std::endl;

		return -1;

	}
	SDL_Rect sr;
	sr.x = 0; sr.y = 0; sr.w = width; sr.h = height;
	SDL_FillRect(m_pSurface, &sr, SDL_MapRGB(m_pSurface->format, 33, 128, 33));

	//一个地板，一个滚动条
	locationRect.x = x;
	locationRect.y = y;
	locationRect.w = width;
	locationRect.h = height;

	contentRect.x = 0;
	contentRect.y = 0;
	contentRect.w = width;
	contentRect.h = height;

	/*if (TTF_Init() == 0) {
		font = TTF_OpenFont("assets/FreeSans.ttf", 12);
		
		
	}*/



	std::cout << "timeruler create ok!!"  << std::endl;


	return true;
}

int TimeRuler::Destroy() {
	
	
	if (m_pSurface)
		SDL_FreeSurface(m_pSurface);
	m_pSurface = NULL;
	/*if (font) {
		TTF_CloseFont(font);
	}*/
	return 0;
}

void TimeRuler::Refresh() {
	//double UPP = AudioProject::(double)AudioProject::DurationOfScreen / contentRect.w;

	if (m_pSurface == NULL)
		return;
	if (AudioProject::UPP == 0)
		return;
	FindLinearTickSizes(AudioProject::UPP);

	SDL_FillRect(m_pSurface, &contentRect, SDL_MapRGB(m_pSurface->format, 33, 128, 33));

	int nLastMinor = AudioProject::LeftTimeOnScreen / mMinor;
	int NextMinor = (nLastMinor + 1) * mMinor; 

	int nLastMajor = AudioProject::LeftTimeOnScreen / mMajor;
	int NextMajor = (nLastMajor + 1) * mMajor;


	int Majorstep = mMajor / mMinor;

	int curIndex = (NextMinor / mMinor) % Majorstep;





	int x =(int)(((double)NextMinor- AudioProject::LeftTimeOnScreen) / AudioProject::UPP);
	//int xx= (int)((NextMajor- AudioProject::LeftTimeOnScreen) / UPP);

	int step_x =(int)( mMinor / AudioProject::UPP);
	//int step_xx = (int)(mMajor / UPP);

	int d = NextMajor;
	

	int word_w=0;
	int  word_h=0;
	TTF_SizeText(font, "0:00:00", &word_w, &word_h);

		//return;
	//(contentRect.w - word_w)

	for (int pix = x; pix < contentRect.w;) {


		//if (abs(i - xx) < 10) {
		if(curIndex== Majorstep){
			SDLUtils::SDL_DrawLine(m_pSurface, pix, contentRect.h-10, pix, contentRect.h-1 , SDL_MapRGB(m_pSurface->format, 33, 33, 33));
			if (font) {
				SDL_Color color = { 0,0,0 };//颜色
				SDL_Surface* text_surface;
				char s[50];
				int secs = d / 1000;
				int hour = secs / 3600;
				int min = (secs % 3600) / 60;
				int sec = (secs % 3600) % 60;
				snprintf (s, 50, "%d:%02d:%02d", hour,min,sec);
				// int w, h;
				// TTF_SizeText(font, s, &w, &h);
				text_surface = TTF_RenderUTF8_Solid(font, s, color);
				if (text_surface != NULL )
				{
					SDL_Rect  sr;
					sr.x = pix -word_w/2;
					sr.y = 2;
					sr.w = word_w;
					sr.h = word_h;

	            //	std::cout << "timerruler pix:"  << pix<<" word_w :"<<word_w<<" contentRect.w :"<<contentRect.w<<std::endl;

					SDL_BlitSurface(text_surface, NULL, m_pSurface, &sr);//将文字复制到屏幕的surface上面
					SDL_FreeSurface(text_surface);
					//std::cout << "SDL_BlitSurface ok"  <<std::endl;

				}
			}
			
			//xx += step_xx;

			d += mMajor;
			curIndex = 0;

		

		}
		else
			SDLUtils::SDL_DrawLine(m_pSurface, pix, contentRect.h-5, pix, contentRect.h-1, SDL_MapRGB(m_pSurface->format, 33, 33, 33));


		//if (pos % mMajor == 0) {//画大的线
		//	SDLUtils::SDL_DrawLine(m_pSurface, i, contentRect.y, i, contentRect.y - 6, SDL_MapRGB(m_pSurface->format, 33, 33, 33));
		//}
		//else if (pos % mMinor == 0) {//画小的先
		//}
		pix += step_x;
		curIndex++;
	}
	//std::cout << "TimeRuler::Refresh ok"  <<std::endl;






}


void TimeRuler::FindLinearTickSizes(double UPP) {

	int units = (int)( 22 * fabs(UPP));

	if (units < 1000) { // 1 sec
		mMinor = 1000;
		mMajor = 5000;
		return;
	}
	if (units < 5000) { // 5 sec
		mMinor = 5000;
		mMajor = 15000;
		return;
	}
	if (units < 10.0) {
		mMinor = 10000;
		mMajor = 30000;
		return;
	}
	if (units < 15000) {
		mMinor = 15000;
		mMajor = 60000;
		return;
	}
	if (units < 30000) {
		mMinor = 30000;
		mMajor = 60000;
		return;
	}
	if (units < 60000) { // 1 min
		mMinor = 60000;
		mMajor = 300000;
		return;
	}
	if (units < 300000) { // 5 min
		mMinor = 300000;
		mMajor = 900000;
		return;
	}
	if (units < 600000) { // 10 min
		mMinor = 600000;
		mMajor = 1800000;
		return;
	}
	if (units < 900000) { // 15 min
		mMinor = 900000;
		mMajor = 3600000;
		return;
	}
	if (units < 1800000) { // 30 min
		mMinor = 1800000;
		mMajor = 3600000;
		return;
	}
	if (units < 3600000) { // 1 hr
		mMinor = 3600000;
		mMajor = 6*3600000;
		return;
	}
	//最多 6个小时
	mMinor = 6 * 3600000;
	mMajor = 24 * 3600000;
	


}

