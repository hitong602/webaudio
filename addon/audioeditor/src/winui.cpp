#include "winui.h"


SDL_Rect WinUI::location2content(SDL_Rect* pr)
{
	SDL_Rect sr;
	sr.x = pr->x - locationRect.x;
	sr.y = pr->y - locationRect.y;
	sr.w = pr->w;
	sr.h = pr->h;
	return sr;

}
SDL_Point WinUI::locationPt2content(SDL_Point* sp) {

	SDL_Point p;
	p.x = sp->x - locationRect.x;
	p.y = sp->y - locationRect.y;

	return p;

}