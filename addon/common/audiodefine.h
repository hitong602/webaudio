﻿#pragma once
typedef struct _AudioFileInfo
{
	int sampleRate;
	int channelNum;
	int bitrate;
	int codeID;
	int duration;
	int  totalFrames; // by this paramete ,fps,spf can be calc
	
} AudioFileInfo;


typedef struct _stereosampleblock {
	int  left;
	int right;
	int filepos;  // bytes pos in the file 
	//int framenum;// frame num in the file
	int mspos;   // time  in file
} WFM_Stereo_SampleBlock;

typedef struct _datainfo {
	std::string filename;
	int readPos;
	int size;
}DATAINFO;