#include <iostream>
#include "ffmpegAPI.h"
#include <algorithm>

using namespace std;


short SAMPLE_F2S16(float x) {
	float sample32 = x;
	if (sample32 > 1.0f)
		sample32 = 1.0f;
	if (sample32 < -1.0f)
		sample32 = -1.0f;
	return (short)(sample32 * 32767);
}

#define AUDIO_INBUF_SIZE 20480
#define AUDIO_REFILL_THRESH 4096
#define WFM_BUFFER_SIZE  4096   //��� �������ݵĻ��棬���ﵽ�����Ŀ�󣬾ͻ���ûص���



						   //����һ֡
FFMPEGAPI::FFMPEGAPI() {

	//memset(&audioInfo,0,sizeof(AudioFileInfo));
	//m_PeakBuffer = new FlowBuffer();
	m_CallbackUserData=NULL;
	readPosInFile=0;

	 avPkt=NULL;
	avParserCxt = NULL;
	 avCodec = NULL;
	 avCodecCxt = NULL;
	 decoded_frame = NULL;

}
FFMPEGAPI::~FFMPEGAPI() {
	// if (m_PeakBuffer)
	// 	delete m_PeakBuffer;

}

//�����=0 Ҫ�������룬ֱ��ret С�� 0
static int decodewfm(AVCodecContext *dec_ctx, AVFrame *frame, int *pcmcount, int *leftwfmPeak, int *rightwfmPeak)
{
	int i, ch;
	int ret, data_size;

	short peak[2];
	

	/* read all the output frames (in general there may be any number of them */

	ret = avcodec_receive_frame(dec_ctx, frame);
	if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF)
		return ret;
	else if (ret < 0) {
		return ret;
	}
	data_size = av_get_bytes_per_sample(dec_ctx->sample_fmt);
	if (data_size < 0) {
		/* This should not occur, checking just for paranoia */
		return  -2;
	}


	
	peak[0] = 0;
	peak[1] = 0;

	for (i = 0; i < frame->nb_samples; i++)
		for (ch = 0; ch < dec_ctx->channels; ch++)
		{
			if (dec_ctx->sample_fmt == AV_SAMPLE_FMT_S16P) {
				short ss= *(short*)(frame->data[ch] + data_size * i);
				short s = std::abs(ss);
			
				peak[ch] = std::max(peak[ch],s);
			}
			else if (dec_ctx->sample_fmt == AV_SAMPLE_FMT_FLTP) {
				float fs = *(float*)(frame->data[ch] + data_size * i);
				short s = std::abs(SAMPLE_F2S16(fs));
				peak[ch] = std::max(peak[ch],s );
			}
			

		}
	*pcmcount = frame->nb_samples;
	*leftwfmPeak =(int) peak[0];
	*rightwfmPeak =(int) peak[1];

				
	
	return ret;
}
static int get_format_from_sample_fmt(const char **fmt,
	enum AVSampleFormat sample_fmt)
{
	int i;
	struct sample_fmt_entry {
		enum AVSampleFormat sample_fmt; const char *fmt_be, *fmt_le;
	} sample_fmt_entries[] = {
		{ AV_SAMPLE_FMT_U8,  "u8",    "u8" },
	{ AV_SAMPLE_FMT_S16, "s16be", "s16le" },
	{ AV_SAMPLE_FMT_S32, "s32be", "s32le" },
	{ AV_SAMPLE_FMT_FLT, "f32be", "f32le" },
	{ AV_SAMPLE_FMT_DBL, "f64be", "f64le" },
	};
	*fmt = NULL;

	for (i = 0; i < FF_ARRAY_ELEMS(sample_fmt_entries); i++) {
		struct sample_fmt_entry *entry = &sample_fmt_entries[i];
		if (sample_fmt == entry->sample_fmt) {
			*fmt = AV_NE(entry->fmt_be, entry->fmt_le);
			return 0;
		}
	}

	/*fprintf(stderr,
		"sample format %s is not supported as output format\n",
		av_get_sample_fmt_name(sample_fmt));*/
	return -1;
}

int FFMPEGAPI::beginDecode(int codecID) {
	avCodec = avcodec_find_decoder((AVCodecID)codecID);
	if (NULL == avCodec)
	{
		goto end;
	}
	avCodecCxt = avcodec_alloc_context3(avCodec);
	if (!avCodecCxt)
	{
		goto end;
	}

	/* open it */
	if (avcodec_open2(avCodecCxt, avCodec, NULL) < 0)
	{
		goto end;
	}

	avParserCxt = av_parser_init(codecID);
	if (!avParserCxt)
	{
		goto end;
	}
	avPkt = av_packet_alloc();

	
	if (!(decoded_frame = av_frame_alloc()))
	{
		goto end;
	}



	return 0;
end:
	if(avCodecCxt)
		avcodec_free_context(&avCodecCxt);
	if(avParserCxt)
		av_parser_close(avParserCxt);
	if(decoded_frame)
		av_frame_free(&decoded_frame);
	if(avPkt)
		av_packet_free(&avPkt);
	return -1;
}
int FFMPEGAPI::decode( unsigned char* in_data, int in_len,  unsigned char* pcmbuf, int& pcmlen) {
	unsigned char* out = pcmbuf;
	int outbytes = 0;

	while (in_len > 0)
	{
		int ret = av_parser_parse2(avParserCxt, avCodecCxt, &avPkt->data, &avPkt->size, in_data, in_len, AV_NOPTS_VALUE, AV_NOPTS_VALUE, 0);
		in_data += ret;
		in_len -= ret;
		if (avPkt->size) {
			int i, ch;
			int r, data_size;

			r = avcodec_send_packet(avCodecCxt, avPkt);
			
			while (r >= 0) {
				/* read all the output frames (in general there may be any number of them */
				r = avcodec_receive_frame(avCodecCxt, decoded_frame);
				if (r == AVERROR(EAGAIN) || r == AVERROR_EOF)
					break;
				else if (r < 0) {
					break;
				}
				data_size = av_get_bytes_per_sample(avCodecCxt->sample_fmt);
				if (data_size < 0) {
					/* This should not occur, checking just for paranoia */
					break;
				}
				for (i = 0; i < decoded_frame->nb_samples; i++)
					for (ch = 0; ch < decoded_frame->channels; ch++) {
						if (avCodecCxt->sample_fmt == AV_SAMPLE_FMT_S16P) {
							memcpy(out, decoded_frame->data[ch] + data_size * i, data_size);
							out += data_size;
							outbytes += data_size;

							/*	decoded_frame->data[ch] + data_size * i, 1, data_size

								short ss = *(short*)(decoded_frame->data[ch] + data_size * i);
								short s = std::abs(ss);

								peak[ch] = std::max(peak[ch], s);*/
						}
						else if (avCodecCxt->sample_fmt == AV_SAMPLE_FMT_FLTP) {
							/*float fs = *(float*)(decoded_frame->data[ch] + data_size * i);
							short s = std::abs(SAMPLE_F2S16(fs));
							peak[ch] = std::max(peak[ch], s);*/
							memcpy(pcmbuf, decoded_frame->data[ch] + data_size * i, data_size);
							pcmbuf += data_size;

						}
					}
			}

		}

	}
	
	

	pcmlen = outbytes;

	return avPkt->size;


}
int FFMPEGAPI::endDecode() {
	if (avCodecCxt)
		avcodec_free_context(&avCodecCxt);
	if (avParserCxt)
		av_parser_close(avParserCxt);
	if (decoded_frame)
		av_frame_free(&decoded_frame);
	if (avPkt)
		av_packet_free(&avPkt);
	avPkt = NULL;
	avParserCxt = NULL;
	avCodec = NULL;
	avCodecCxt = NULL;
	decoded_frame = NULL;
	return 0;
}
static int read_packet(void *opaque, uint8_t *buf, int buf_size)
{
	FFMPEGAPI *caller=(FFMPEGAPI *)opaque;
	int len = caller->Probe_ReadFileCallback((const char *)buf, buf_size,caller->readPosInFile,caller->m_CallbackUserData);
	caller->readPosInFile+=len;
	//fprintf(stdout, "caller in read_packet is : %d %d %d %d %d   \n", len, buf[0], buf[1], buf[2], buf[3]);
	return len;
}
int FFMPEGAPI::extractWaveform(AudioFileInfo* audioinfo, std::function<int (const char*, int size, int pos, int userdata)>readfileback,FlowBuffer *fb, int userdata)
{
	const AVCodec *codec;
	AVCodecContext *c = NULL;
	AVCodecParserContext *parser = NULL;
	int len, ret;
	uint8_t inbuf[AUDIO_INBUF_SIZE + AV_INPUT_BUFFER_PADDING_SIZE];
	uint8_t wfmbuf[WFM_BUFFER_SIZE + AV_INPUT_BUFFER_PADDING_SIZE];

	uint8_t *data;
	size_t   data_size;

	uint8_t *wfmdata;
	size_t  wfmdata_pos;

	AVPacket *pkt;
	AVFrame *decoded_frame = NULL;
	enum AVSampleFormat sfmt;
	int n_channels = 0;
	const char *fmt;
	int  left, right;
	bool isEOF = false; //���ص��ֽ���Ϊ0 

	int FrameFilePos = 0; //��ǰ����֡���ļ��е�λ��
	double FrameTimePos = 0; //��ǰ����֡���ļ��е�λ��
	int decodingSample = 0;  //��ǰ�����������Ŀ

	double durationInMS=0;// total duration (MS)	
	int totalFrameCounts = 0;

	int decret = 0;   //����Ľ��
	if (audioinfo->codeID <=0)  //������ȷ���ļ��ĸ�ʽ
		return -1;

	readPosInFile=0;
	//m_PeakBuffer->Clear();


	pkt = av_packet_alloc();

	/* find the MPEG audio decoder */
	codec = avcodec_find_decoder((AVCodecID)audioinfo->codeID);
	if (!codec) {
		goto end;
	}

	parser = av_parser_init(codec->id);
	if (!parser) {
		goto end;
	}

	c = avcodec_alloc_context3(codec);
	if (!c) {
		goto end;
	}

	/* open it */
	if (avcodec_open2(c, codec, NULL) < 0) {
		goto end;
	}

	wfmdata = wfmbuf;
	wfmdata_pos = 0; //��������

	data = inbuf;
	data_size = readfileback((const char *)inbuf, AUDIO_INBUF_SIZE,readPosInFile,userdata);
	readPosInFile+=data_size;

	while (data_size > 0)
	{
		if (!decoded_frame)
		{

			if (!(decoded_frame = av_frame_alloc()))
			{
				ret = -10;
				goto end;
			}
		}

		ret = av_parser_parse2(parser, c, &pkt->data, &pkt->size,
			data, data_size,
			AV_NOPTS_VALUE, AV_NOPTS_VALUE, 0);
		if (ret < 0)
		{
			ret = -11;
			goto end;
		}
		data += ret;
		data_size -= ret;
	

		if (pkt->size)
		{
			decret = avcodec_send_packet(c, pkt);
			if (decret == 0 ) {


				while (decodewfm(c, decoded_frame, &decodingSample, &left, &right) >= 0) {
					WFM_Stereo_SampleBlock wssb;
					wssb.filepos = FrameFilePos;
					wssb.left = left;
					wssb.right = right;
					//wssb.framenum = totalFrameCounts;
					wssb.mspos = FrameTimePos;
					FrameTimePos += decodingSample * 1000.0 / audioinfo->sampleRate;
					totalFrameCounts += 1;
					fb->Put((char *)&wssb,sizeof(WFM_Stereo_SampleBlock));
				//	m_PeakBuffer->Put((const char *)&wssb, sizeof(WFM_Stereo_SampleBlock));
				}
			}
			//ֻҪ���������ݴ���0 �ͼ�¼
			FrameFilePos += pkt->size;   //parse �������ļ�λ��

		}

		if (data_size < AUDIO_REFILL_THRESH && !isEOF)
		{
			//read data
			memmove(inbuf, data, data_size);
			data = inbuf;
			len = readfileback((const char *)data + data_size, AUDIO_INBUF_SIZE - data_size, readPosInFile, userdata);
	

			if (len > 0){
				data_size += len;
				readPosInFile+= len;
			}
			else
				isEOF = true;
		}
	}

	/* flush the decoder */
	if (decoded_frame) {
		pkt->data = NULL;
		pkt->size = 0;
		decret = avcodec_send_packet(c, pkt);
		if (decret == 0) {

			while (decodewfm(c, decoded_frame, &decodingSample, &left, &right) >= 0) {
				WFM_Stereo_SampleBlock wssb;
				wssb.filepos = 0;
				wssb.left = left;
				wssb.right = right;
				wssb.mspos = FrameTimePos;
				//wssb.framenum = totalFrameCounts;
				FrameTimePos += decodingSample * 1000.0 / audioinfo->sampleRate;
				totalFrameCounts += 1;

				fb->Put((char *)&wssb,sizeof(WFM_Stereo_SampleBlock));

			}
		}

	}
	
	durationInMS = FrameTimePos;

	audioinfo->duration = durationInMS;
	audioinfo->totalFrames = totalFrameCounts;
	
end:
	avcodec_free_context(&c);
	av_parser_close(parser);
	av_frame_free(&decoded_frame);
	av_packet_free(&pkt);
	return 0;
}
// bool FFMPEGAPI::getWaveformBuffer( char** pBuf, int* size) {

// 	int len=m_PeakBuffer->GetEnd();
// 	if (m_PeakBuffer && len>0)
// 	{
// 		*pBuf = m_PeakBuffer->GetHeadPt();
// 		*size= m_PeakBuffer->GetEnd();
// 		return true;
// 	}
// 	return false;
		
// }

AudioFileInfo FFMPEGAPI::probeaudio(std::function<int (const char*, int size, int pos, int userdata)>readfileback,int userdata)
{
	AVFormatContext *fmt_ctx = NULL;
	AVIOContext *avio_ctx = NULL;

	AVStream *avStream = NULL;
	AVCodecContext *audio_dec_ctx = NULL;
	AVCodec *audio_dec = NULL;

	unsigned char *avio_ctx_buffer = NULL;
	size_t avio_ctx_buffer_size = 32768;

	int ret;
	//double duration;
	int audio_stream_idx=-1;

	AudioFileInfo audioInfo;

	memset(&audioInfo, 0, sizeof(AudioFileInfo));

	this->Probe_ReadFileCallback = std::move(readfileback);
	this->m_CallbackUserData = userdata;
	if (!(fmt_ctx = avformat_alloc_context()))
	{
		ret = AVERROR(ENOMEM);
		fprintf(stdout, " avformat_alloc_context error :%d \n", ret);

		goto end;
	}
	/* �����ڴ�, �����Լ����û����С,�������õ���4K */
	avio_ctx_buffer = (unsigned char *)av_malloc(avio_ctx_buffer_size);
	if (!avio_ctx_buffer)
	{
		ret = AVERROR(ENOMEM);
		fprintf(stdout, " av_malloc error:%d \n", ret);

		goto end;
	}

	/* avio_ctx_buffer�ǻ�������
	* avio_ctx_buffer �ĳ�ʼ��ַ��ֵ�� avio_ctx->buffer
	* avio_ctx_buffer_size�ǻ�������С , Ҳ��ÿ�ζ�ȡ���ݵĴ�С
	* bd �������ļ��ļ���ӳ���ļ�
	* read_packet �ص�����,��ȡ���ݵĹ��� , ������ʲô����²Ż�ص� ?
	*/

	avio_ctx = avio_alloc_context(avio_ctx_buffer, avio_ctx_buffer_size,
		0, this, &read_packet, NULL, NULL);
	if (!avio_ctx)
	{
		ret = AVERROR(ENOMEM);
		fprintf(stdout, " avio_alloc_context error:%d \n", ret);

		goto end;
	}
	fmt_ctx->pb = avio_ctx;
	/* ���ó�ʼ����Ϣ
	* read_packet �ص������������ﱻ����, ���������ļ����������ݶ��ȴ��뻺����,
	* �����������Ҫ�õ����ݣ���ô���ʹӻ�����ֱ�ӵ�������
	*/

	ret = avformat_open_input(&fmt_ctx, NULL, NULL, NULL);
	if (ret < 0)
	{
		fprintf(stdout, " avformat_open_input return error:%d \n", ret);

		goto end;
	}
	// fprintf(stdout, " av_probe_input_buffer: \n");
	// ret=av_probe_input_buffer(avio_ctx, &fmt_ctx, "", NULL, 0, 0)
	// if ( < 0){
	// 	goto end;
	// }

	ret = avformat_find_stream_info(fmt_ctx, NULL);
	if (ret < 0)
	{
		fprintf(stderr, "Could not find stream information\n");
		goto end;
	}
	for (int i = 0; i < fmt_ctx->nb_streams; i++)
	{
		avStream = fmt_ctx->streams[i];
		if (avStream->codecpar->codec_type == AVMEDIA_TYPE_AUDIO)
		{
			audio_stream_idx = i;
			break;
		}
	}
	if (audio_stream_idx < 0 || avStream==NULL)
	{
		ret = -1;
		fprintf(stdout, " no audio stream---: \n");
	}
	audio_dec = avcodec_find_decoder(avStream->codecpar->codec_id);
	if (!audio_dec)
	{
		fprintf(stderr, "avcodec_find_decoder %d error\n", avStream->codecpar->codec_id);
		goto end;
	}

	audio_dec_ctx = avcodec_alloc_context3(audio_dec);
	if (audio_dec_ctx == NULL)
	{
		fprintf(stderr, "avcodec_alloc_context3 error\n");
		goto end;
	}
	ret = avcodec_parameters_to_context(audio_dec_ctx, avStream->codecpar);

	if (ret < 0)
	{
		fprintf(stderr, "avcodec_parameters_to_context error\n");
		ret = -5;
		goto end;
	}
	//ͨ������ķ�ʽ���޷�����ļ���ʱ�䳤��
	//	duration = (double)fmt_ctx->duration / AV_TIME_BASE;
	audioInfo.sampleRate = audio_dec_ctx->sample_rate;
	audioInfo.channelNum = audio_dec_ctx->channels;
	audioInfo.bitrate = audio_dec_ctx->bit_rate;
	audioInfo.codeID = (int)audio_dec_ctx->codec_id;
	audioInfo.totalFrames = -1;
	audioInfo.duration = -1;

	// fprintf(stdout, "audio sample_rate: %d\n", audio_dec_ctx->sample_rate);
	// fprintf(stdout, "audio channels: %d\n", audio_dec_ctx->channels);
	// fprintf(stdout, "audio bitrate %d kb/s\n", (int)audio_dec_ctx->bit_rate / 1000);
	//	fprintf(stdout, "audio duration %d \n", (int)duration);

	//���������Ϣ
end:
	if (audio_dec_ctx)
		avcodec_free_context(&audio_dec_ctx);
	if (fmt_ctx)
	{
		avformat_close_input(&fmt_ctx);
		avformat_free_context(fmt_ctx);
	}

	if (avio_ctx)
	{
		av_freep(&avio_ctx->buffer);
		av_freep(&avio_ctx);
	}
	return audioInfo;
	/*if (ret == 0)
		return &audioInfo;
	else
		return NULL;*/

	//fprintf(stdout, "probe ok return %d\n", ret);
	//return ret;
}