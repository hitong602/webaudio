#include <iostream>
#include <functional>
#include "flowbuffer.h"
#include "audiodefine.h"

extern "C"
{
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavutil/frame.h>
#include <libavutil/mem.h>
#include <libavcodec/avcodec.h>
}

class FFMPEGAPI {
	//AudioFileInfo audioInfo ; //��ǰ�ļ��ĸ�ʽ��Ϊ�˷���һ��ָ�루�ڴ�)
	AVPacket* avPkt;
	AVCodecParserContext* avParserCxt;
	AVCodec* avCodec;
	AVCodecContext* avCodecCxt;
	AVFrame* decoded_frame;
public:
	//double durationInMS;// total duration (MS)	
	//int totalFrameCounts; // total frame . for wav ,it's sample counts,for oother format,it's frame num;we can got time of one frame;
	                      // it 's averate value .
	int beginDecode(int codecID);
	int decode(unsigned char* in_data, int in_len, unsigned char* pcmbuf, int &pcmlen); //返回是否成功解码
	int endDecode();


public:
	//Probe_ReadFileBK Probe_ReadFileCallback;  //���ļ��Ļص�
	int m_CallbackUserData;
	int readPosInFile;

	std::function<int (const char*, int size, int pos, int userdata)> Probe_ReadFileCallback;


public:
	FFMPEGAPI();
	~FFMPEGAPI();

	//std::vector<WFM_Stereo_SampleBlock> getWaveformBuffer(){return m_EnergyArray;};
	// û�б�Ҫ��λ�ã����ϲ�Ҫ���ݵ�ʱ���ϲ������֪����ǰ�����Ľ���
	int extractWaveform(AudioFileInfo *audioinfo, std::function<int (const char*, int size, int pos, int  userdata)>readfileback , FlowBuffer *fb, int userdata);
	AudioFileInfo probeaudio(std::function<int (const char*, int size, int pos, int userdata)>readfileback, int userdata);

};