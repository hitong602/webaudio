#include "flowbuffer.h"

void FlowBuffer::Put(const char* c, int len)
{
	if (c == nullptr || len <= 0 || m_buf == nullptr)
	{
		return;
	}
	int left = GetLeft();
	if (left <= len) //expand
	{

		int expand = m_incr;
		while ((left + expand) < len)
		{
			expand *= 2;
		}
		m_cap += expand;
		m_buf=(char *)realloc(m_buf, m_cap);
		if (m_buf == nullptr)
		{
			m_cap -= expand;
			return;
		}
		
	}
	memcpy(m_buf + m_end, c, len);
	m_end += len;
}

//char* FlowBuffer::Get(int len) //�ǵ��ͷ�
//{
//	if (m_buf == nullptr || ((m_end - m_begin) < len))
//	{
//		return nullptr;
//	}
//	char* p = (char*)malloc(len + 1);
//	if (p == nullptr)
//	{
//		return nullptr;
//	}
//	memcpy(p, m_buf + m_begin, len);
//	m_begin += len;
//	return p;
//}

