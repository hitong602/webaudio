#pragma once
#include <cstdio>
#include <cstring>
#include <stdlib.h>
#include <malloc.h>

#include "audiodefine.h"
class FlowBuffer
{
public:
	FlowBuffer()
	{
		m_buf=NULL;
		m_begin=0;
	 	m_end=0;
	 	m_cap=0;
		m_incr = 1024 / sizeof(WFM_Stereo_SampleBlock) * sizeof(WFM_Stereo_SampleBlock);// 512 * sizeof(WFM_Stereo_SampleBlock);
		m_buf = (char*)malloc(m_incr);
		if (m_buf == nullptr)
		{
			return;
		}
		m_cap += m_incr;
		m_begin = m_end = 0;
	}
	~FlowBuffer()
	{
		free(m_buf);
		m_buf = nullptr;
	}
	int GetBegin() { return m_begin; }
	int GetEnd() { return m_end; }
	int GetCap() { return m_cap; }
	int GetLeft() { return (m_cap - m_end); }
	int ContentSize() { return (m_end - m_begin); }
	void Put(const char* c, int len);
	//char* Get(int len);
	char* GetHeadPt() { return &m_buf[0]; }
	void Clear() {
		free(m_buf);
		m_buf = nullptr;
		m_begin = 0;
		m_end = 0;
		m_cap = 0;
		m_incr = 1024 / sizeof(WFM_Stereo_SampleBlock) * sizeof(WFM_Stereo_SampleBlock);
		m_buf = (char*)malloc(m_incr);
		if (m_buf == nullptr)
		{
			return;
		}
		m_cap += m_incr;
		m_begin = m_end = 0;

	}
private:
	char* m_buf;//{ nullptr };
	int m_begin;//{ 0 }; //buffer ���ݿ�ʼ
	int m_end;//{ 0 };   //buffer ���ݽ���
	int m_cap;//{ 0 };   //buffer ����
	int m_incr;//{ 1024 };
};
