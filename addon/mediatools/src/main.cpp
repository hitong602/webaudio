#include <stdio.h>
#include <emscripten.h>
#include <emscripten/bind.h>

using namespace emscripten;
#include "ffmpegAPI.h"

FlowBuffer energyBuffer;

extern "C" int main(int argc, char **argv)
{
    printf("this is an mediatools for audioeditor!\n");
}
EMSCRIPTEN_KEEPALIVE
uintptr_t  BeginDecode(int codecID){
    FFMPEGAPI  *ffapi=new FFMPEGAPI();
    int res=ffapi->beginDecode(codecID);
    if(res==0){
        return reinterpret_cast< uintptr_t> (ffapi);
    }else
    {
        delete ffapi;
        return NULL;
    }

}
EMSCRIPTEN_KEEPALIVE
int EndDecode(uintptr_t  decodeHandle){
    FFMPEGAPI  *ffapi=reinterpret_cast< FFMPEGAPI *>(decodeHandle);
    ffapi->endDecode();
    delete ffapi;
    return 0;
}
EMSCRIPTEN_KEEPALIVE
int Decode(uintptr_t decodeHandle, unsigned char* pData, int size,uintptr_t pcmbuf, uintptr_t pcmlen){
    FFMPEGAPI *ffapi = reinterpret_cast<FFMPEGAPI *> (decodeHandle);

    int  *ppl=reinterpret_cast< int *>(pcmlen);


    int pl=*ppl;
    unsigned char  *pb=reinterpret_cast< unsigned char *>(pcmbuf);

    int usedlen=ffapi->decode(pData,size,pb,pl);

    *ppl=pl;
    return usedlen;
}


typedef int (*READFILECALLBACK) (const char*, int size, int pos, int userdata);

AudioFileInfo  ProbeFile(std::string key ,uintptr_t calback,int userdata)
{

    READFILECALLBACK bc=reinterpret_cast< READFILECALLBACK>(calback);
    printf("ReadFileCallback:%d ",(int)calback);

    FFMPEGAPI ffmpegapi;

    AudioFileInfo afi=ffmpegapi.probeaudio(bc,userdata);
    return afi;
   
}
 AudioFileInfo ExtractEnergy(uintptr_t filecallback, int userdata,uintptr_t energybuf, uintptr_t energylen){
    
    READFILECALLBACK bc=reinterpret_cast< READFILECALLBACK>(filecallback);

    uintptr_t *eb=reinterpret_cast< uintptr_t *>(energybuf);

    int  *el=reinterpret_cast< int *>(energylen);

  //  printf("ReadFileCallback:%d ",(int)filecallback);

    FFMPEGAPI ffmpegapi;
    ffmpegapi.readPosInFile=0;

    AudioFileInfo afi = ffmpegapi.probeaudio(bc, userdata);
    if (afi.codeID == -1){
   
        return afi; 
    }
  // ffmpegapi.readPosInFile = 0;
	energyBuffer.Clear();

	//energyBuffer.Put((const char*)&afi, sizeof(AudioFileInfo));


	int res = ffmpegapi.extractWaveform(&afi, bc, &energyBuffer, 0);
	if (res == 0) {

		 char* h = energyBuffer.GetHeadPt();
		// memcpy(h, (const char*)&afi, sizeof(AudioFileInfo));


		*eb = (uintptr_t)h;


		//memcpy(eb,(const char *)&wh,sizeof(WFM_Header));
		*el = energyBuffer.GetEnd();

	}
	else {
		*eb = 0;
		*el = 0;

	}
	return afi;

    // if(res== 0)
    // {
    //     char *pBuf=NULL;
    //     int nSize;
    //     ffmpegapi.getWaveformBuffer(&pBuf,&nSize);
    //   //  energy=(WFM_Stereo_SampleBlock * )pBuf;
    //     energylen=nSize;
    //     return (WFM_Stereo_SampleBlock *)pBuf;
    // }

    //  return NULL;
  
}
EMSCRIPTEN_BINDINGS(MediaTools) {cp 
    emscripten::value_object<_AudioFileInfo>("AudioFileInfo")
        .field("sampleRate",&_AudioFileInfo::sampleRate)
        .field("channelNum",&_AudioFileInfo::channelNum)
        .field("bitrate",&_AudioFileInfo::bitrate)
        .field("codeID",&_AudioFileInfo::codeID)
        .field("duration",&_AudioFileInfo::duration)
        .field("totalFrames",&_AudioFileInfo::totalFrames)
        ;
        
    emscripten::function("ProbeFile", &ProbeFile);
    emscripten::function("ExtractEnergy", &ExtractEnergy);


}

