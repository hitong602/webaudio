#pragma once

/*
 存放当前项目所需要的所有文件，根据ID，可以得到当前文件对应的数据信息，包括波形数据，音频数据等
 如果音频数据是压缩的，需要在这里来解码
 需要在这里来seek

 这个文件，实际上是通过 javascript 来实现的

 打开文件后，先在这里建立一个file示例

*/

#include <map>
#include "MediaFile.h"
#include "AudioProject.h"

class JSMediaFileManager
{
	AudioProject m_AudioProject;
private:
	static int ReadCallback(const char*, int size, int pos, void* userdata);
	static std::map<void*, MediaFile*> FileList;
public:
	JSMediaFileManager(){}
	~JSMediaFileManager() {}
public:
	int ProbeFile(char* szFileName, AudioFileInfo* afi);
	int CreateProject(int parentid);
	int OpenFile(char* szFileName, int CodeID);
	
};

