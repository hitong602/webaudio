import { Injectable } from '@angular/core';
// import { Observable, BehaviorSubject } from "rxjs";
// import { filter, map } from "rxjs/operators";

// import * as Module from '../../../assets/wasm/ffmpeg/audioproc.js';
// import "!!wasm-loader?name=audioproc.wasm!../../../assets/wasm/ffmpeg/audioproc.wasm";

// let membuffer: any;
// let This: any;

@Injectable({
	providedIn: 'root'
})
export class AudioeditorService {

	webWorker: any;   // 创建一个 线程， 读取文件，并分析文件格式
	constructor() {
	
	}
	LoadWasm(){
		this.startWebWorker();
		//告诉线程，加载一个文件
		this.webWorker.postMessage({eventType: "INITIALIZE", eventData: "../../../../assets/wasm/ffmpeg/audioeditor.wasm"});

	}
	startWebWorker() {
		if (typeof Worker !== 'undefined') {
		  // Create a new
		  this.webWorker = new Worker('./audioeditor.worker', { type: 'module' });
		  this.webWorker.onmessage = ({ data }) => {
			  //监听worker 发过来的数据
			  const { eventType, eventData } = data;
			  if (eventType === "INITIALIZED") {
				  console.log("init wasm ok");

			  } else if  (eventType === 'CREATEPRJFINISHED') {
				console.log("CREATE PRJ FINISHED") ;

			  }
		  };
		//  worker.postMessage(10);
		} else {
		  // Web Workers are not supported in this environment.
		  // You should add a fallback so that your program still executes correctly.
		}
	  }

	
	  public createaudioeditor() {

		this.webWorker.postMessage({eventType: "CREATEPRJ", eventData: ''});

		// // 获取本地文件数据
		// this.readingfile = localaudiofile;
		// this.filereader = new FileReader();
		// this.readingPos = 0;

		// const result = this.waProbeAudio(this.readCallback);

	}

	public probefile(localaudiofile: any) {

		this.webWorker.postMessage({eventType: "PROBE", eventData: localaudiofile});

		// // 获取本地文件数据
		// this.readingfile = localaudiofile;
		// this.filereader = new FileReader();
		// this.readingPos = 0;

		// const result = this.waProbeAudio(this.readCallback);

	}
}
