//// <reference lib="webworker" />
import * as Module from '../../../../assets/wasm/ffmpeg/audioeditor.js';
import { setDefaultService } from 'selenium-webdriver/chrome';

let waTest: any;
let waProbeAudio: any;
let readCallback: any;
let waCreateProject: any;

let fileReader: any;
let fileReadingPos: any;
let localfile: any;
let membuffer: any;
let wasmModule: any;

//这个函数被wasm 调用，读取文件
function readFile(buf: number, pos: number, size: number): number {

  function readBlob(start: any, stepp: any) {
    const blob = blobSlice(localfile, start, start + stepp - 1);
    return fileReader.readAsArrayBuffer(blob);
  }
  function blobSlice(blob, start, end) {
    if (blob.slice) {
      return blob.slice(start, end);
    } else if (blob.webkitSlice) {
      return blob.webkitSlice(start, end);
    } else if (blob.mozSlice) {
      return blob.mozSlice(start, end);
    } else {
      return null;
    }
  }
  const  ab = readBlob(fileReadingPos, size);
  const readlen = ab.byteLength ;

  const  aView = new Uint8Array(ab, 0, readlen);

  fileReadingPos += readlen;

  const heap = new Uint8Array(membuffer.buffer, buf, readlen);
  for (let i = 0; i < readlen; i++) {
      heap[i] = aView[i];
    }

  return readlen;
}
async function instantiateWasm(url: string) {
  // fetch the wasm file
  const wasmFile = await fetch(url);

  // convert it into a binary array
  const buffer = await wasmFile.arrayBuffer();
  const binary = new Uint8Array(buffer);

  // create wasmModule arguments
  // including the wasm-file
  membuffer = new WebAssembly.Memory({
    initial: 256,
    maximum: 256
  });


  const moduleArgs = {
    wasmBinary: binary,
    wasmMemory: membuffer,
    onRuntimeInitialized: () => {
      // waTest = wasmModule._TestCallback;
      // waProbeAudio = wasmModule._probeaudio;
      // readCallback = wasmModule.addFunction(readFile, 'iiii');
      waCreateProject = wasmModule._CreateAudioEditor;
      wasmModule.screen={height:100,width:400};
      console.log(waCreateProject);
//告诉主线程，下载（加载）完成，如何写进度？？？？？
      postMessage({ eventType: "INITIALIZED", eventData: "initok" });
    }

  };

  // instantiate the wasmModule
  wasmModule = Module(moduleArgs);
  // wasmModule.screen = {height:100, width:200};
}
addEventListener('message', ({ data }) => {
  const { eventType, eventData } = data;

  if (eventType === "INITIALIZE") {
    instantiateWasm(eventData);

  } else if (eventType === "PROBE") {

    localfile = eventData;
    fileReader = new FileReaderSync();
    fileReadingPos = 0;

    const result = waProbeAudio(readCallback);
    postMessage({ eventType: "PROBEFINISHED", eventData: result });

  }else if(eventType === "CREATEPRJ"){
    console.log("start to CREATE sPRJ");
    const res = waCreateProject();
    postMessage({ eventType: "CREATEPRJFINISHED", eventData: res });

  }
});
