#include <iostream>
#include "ffmpegapi.h"
#include <math.h>
#include "flowbuffer.h"

using namespace std;
extern "C"
{
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavutil/frame.h>
#include <libavutil/mem.h>
#include <libavcodec/avcodec.h>
}



#define AUDIO_INBUF_SIZE 20480
#define AUDIO_REFILL_THRESH 4096
#define WFM_BUFFER_SIZE  4096   //存放 波形数据的缓存，当达到这个数目后，就会调用回掉，



						   //解码一帧
FFMPEGAPI::FFMPEGAPI() {
	audioInfo = { 0 };
	m_PeakBuffer = new FlowBuffer();

}
FFMPEGAPI::~FFMPEGAPI() {
	if (m_PeakBuffer)
		delete m_PeakBuffer;

}

//如果》=0 要继续解码，直到ret 小于 0
static int decodewfm(AVCodecContext *dec_ctx, AVFrame *frame, int *pcmcount, short *leftwfmPeak, short *rightwfmPeak)
{
	int i, ch;
	int ret, data_size;

	short peak[2];
	

	/* read all the output frames (in general there may be any number of them */

	ret = avcodec_receive_frame(dec_ctx, frame);
	if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF)
		return ret;
	else if (ret < 0) {
		return ret;
	}
	data_size = av_get_bytes_per_sample(dec_ctx->sample_fmt);
	if (data_size < 0) {
		/* This should not occur, checking just for paranoia */
		return  -2;
	}


	
	peak[0] = 0;
	peak[1] = 0;

	for (i = 0; i < frame->nb_samples; i++)
		for (ch = 0; ch < dec_ctx->channels; ch++)
		{
			if (dec_ctx->sample_fmt == AV_SAMPLE_FMT_S16P) {
				short ss= *(short*)(frame->data[ch] + data_size * i);
				short s = ABS(ss);
				if (s < 0)
				{
					int gggg = 0;
				}
				peak[ch] = MAX(peak[ch],s);
			}
			else if (dec_ctx->sample_fmt == AV_SAMPLE_FMT_FLTP) {
				float fs = *(float*)(frame->data[ch] + data_size * i);
				peak[ch] = MAX(peak[ch], ABS(SAMPLE(fs)));
			}
			

		}
	*pcmcount = frame->nb_samples;
	*leftwfmPeak = peak[0];
	*rightwfmPeak = peak[1];

				
	
	return ret;
}
static int get_format_from_sample_fmt(const char **fmt,
	enum AVSampleFormat sample_fmt)
{
	int i;
	struct sample_fmt_entry {
		enum AVSampleFormat sample_fmt; const char *fmt_be, *fmt_le;
	} sample_fmt_entries[] = {
		{ AV_SAMPLE_FMT_U8,  "u8",    "u8" },
	{ AV_SAMPLE_FMT_S16, "s16be", "s16le" },
	{ AV_SAMPLE_FMT_S32, "s32be", "s32le" },
	{ AV_SAMPLE_FMT_FLT, "f32be", "f32le" },
	{ AV_SAMPLE_FMT_DBL, "f64be", "f64le" },
	};
	*fmt = NULL;

	for (i = 0; i < FF_ARRAY_ELEMS(sample_fmt_entries); i++) {
		struct sample_fmt_entry *entry = &sample_fmt_entries[i];
		if (sample_fmt == entry->sample_fmt) {
			*fmt = AV_NE(entry->fmt_be, entry->fmt_le);
			return 0;
		}
	}

	/*fprintf(stderr,
		"sample format %s is not supported as output format\n",
		av_get_sample_fmt_name(sample_fmt));*/
	return -1;
}

static int read_packet(void *opaque, uint8_t *buf, int buf_size)
{
	FFMPEGAPI *caller=(FFMPEGAPI *)opaque;
	int len = caller->ReadFileCallback((const char *)buf, buf_size);
	fprintf(stdout, "caller in read_packet is : %d %d %d %d %d   \n", len, buf[0], buf[1], buf[2], buf[3]);
	return len;
}
int FFMPEGAPI::extractWaveform(int codeID, int(*readfileback)(const char *, int size), int(*writewfmback)(const char *, int size))
{
	const AVCodec *codec;
	AVCodecContext *c = NULL;
	AVCodecParserContext *parser = NULL;
	int len, ret;
	uint8_t inbuf[AUDIO_INBUF_SIZE + AV_INPUT_BUFFER_PADDING_SIZE];
	uint8_t wfmbuf[WFM_BUFFER_SIZE + AV_INPUT_BUFFER_PADDING_SIZE];

	uint8_t *data;
	size_t   data_size;

	uint8_t *wfmdata;
	size_t  wfmdata_pos;

	AVPacket *pkt;
	AVFrame *decoded_frame = NULL;
	enum AVSampleFormat sfmt;
	int n_channels = 0;
	const char *fmt;
	short left, right;
	bool isEOF = false; //返回的字节数为0 

	int FrameFilePos = 0; //当前解码帧在文件中的位置
	double FrameTimePos = 0; //当前解码帧在文件中的位置
	int decodingSample = 0;  //当前解码的样本数目
	int decret = 0;   //解码的结果
	if (audioInfo.codeID == 0)  //必须先确定文件的格式
		return -1;



	pkt = av_packet_alloc();

	/* find the MPEG audio decoder */
	codec = avcodec_find_decoder((AVCodecID)codeID);
	if (!codec) {
		goto end;
	}

	parser = av_parser_init(codec->id);
	if (!parser) {
		goto end;
	}

	c = avcodec_alloc_context3(codec);
	if (!c) {
		goto end;
	}

	/* open it */
	if (avcodec_open2(c, codec, NULL) < 0) {
		goto end;
	}

	wfmdata = wfmbuf;
	wfmdata_pos = 0; //波形数据

	data = inbuf;
	data_size = readfileback((const char *)inbuf, AUDIO_INBUF_SIZE);

	while (data_size > 0)
	{
		if (!decoded_frame)
		{

			if (!(decoded_frame = av_frame_alloc()))
			{
				ret = -10;
				goto end;
			}
		}

		ret = av_parser_parse2(parser, c, &pkt->data, &pkt->size,
			data, data_size,
			AV_NOPTS_VALUE, AV_NOPTS_VALUE, 0);
		if (ret < 0)
		{
			ret = -11;
			goto end;
		}
		data += ret;
		data_size -= ret;
	

		if (pkt->size)
		{
			decret = avcodec_send_packet(c, pkt);
			if (decret == 0 ) {


				while (decodewfm(c, decoded_frame, &decodingSample, &left, &right) >= 0) {
					WFM_Stereo_SampleBlock wssb;
					wssb.filepos = FrameFilePos;
					wssb.left = left;
					wssb.right = right;
					wssb.mspos = FrameTimePos;
					FrameTimePos += decodingSample * 1000.0 / audioInfo.sampleRate;
					
					m_PeakBuffer->Put((const char *)&wssb, sizeof(WFM_Stereo_SampleBlock));
				}
			}
			//只要解析的数据大于0 就记录
			FrameFilePos += pkt->size;   //parse 解析的文件位置
		}

		if (data_size < AUDIO_REFILL_THRESH && !isEOF)
		{
			//read data
			memmove(inbuf, data, data_size);
			data = inbuf;
			len = readfileback((const char *)data + data_size, AUDIO_INBUF_SIZE - data_size);
	

			if (len > 0)
				data_size += len;
			else
				isEOF = true;
		}
	}

	/* flush the decoder */
	if (decoded_frame) {
		pkt->data = NULL;
		pkt->size = 0;
		decret = avcodec_send_packet(c, pkt);
		if (decret == 0) {

			while (decodewfm(c, decoded_frame, &decodingSample, &left, &right) >= 0) {
				WFM_Stereo_SampleBlock wssb;
				wssb.filepos = 0;
				wssb.left = left;
				wssb.right = right;
				wssb.mspos = FrameTimePos;
				FrameTimePos += decodingSample * 1000.0 / audioInfo.sampleRate;
				m_PeakBuffer->Put((const char*)&wssb, sizeof(WFM_Stereo_SampleBlock));
			}
		}

	}
	

	/* print output pcm infomations, because there have no metadata of pcm */
	sfmt = c->sample_fmt;

	if (av_sample_fmt_is_planar(sfmt)) {
		const char *packed = av_get_sample_fmt_name(sfmt);
		printf("Warning: the sample format the decoder produced is planar "
			"(%s). This example will output the first channel only.\n",
			packed ? packed : "?");
		sfmt = av_get_packed_sample_fmt(sfmt);
	}

	n_channels = c->channels;
	if ((ret = get_format_from_sample_fmt(&fmt, sfmt)) < 0)
		goto end;

	//printf("Play the output audio file with the command:\n"
	//	"ffplay -f %s -ac %d -ar %d %s\n",
	//	fmt, n_channels, c->sample_rate,
	//	outfilename);


end:
	avcodec_free_context(&c);
	av_parser_close(parser);
	av_frame_free(&decoded_frame);
	av_packet_free(&pkt);
	return 0;
}
int FFMPEGAPI::getWaveformBuffer(const char** pBuf) {
	if (m_PeakBuffer)
	{
		*pBuf = m_PeakBuffer->GetHeadPt();
		return m_PeakBuffer->GetEnd();
	}
	return 0;
		
}

AudioFileInfo *FFMPEGAPI::probeaudio(int(*readfileback)(const char *, int size))
{
	AVFormatContext *fmt_ctx = NULL;
	AVIOContext *avio_ctx = NULL;

	AVStream *avStream = NULL;
	AVCodecContext *audio_dec_ctx = NULL;
	AVCodec *audio_dec = NULL;

	unsigned char *avio_ctx_buffer = NULL;
	size_t avio_ctx_buffer_size = 32768;

	int ret;
	//double duration;
	int audio_stream_idx=-1;

	this->ReadFileCallback = readfileback;
	m_PeakBuffer->Clear();
	if (!(fmt_ctx = avformat_alloc_context()))
	{
		ret = AVERROR(ENOMEM);
		fprintf(stdout, " avformat_alloc_context error :%d \n", ret);

		goto end;
	}
	/* 分配内存, 可以自己设置缓冲大小,这里设置的是4K */
	avio_ctx_buffer = (unsigned char *)av_malloc(avio_ctx_buffer_size);
	if (!avio_ctx_buffer)
	{
		ret = AVERROR(ENOMEM);
		fprintf(stdout, " av_malloc error:%d \n", ret);

		goto end;
	}

	/* avio_ctx_buffer是缓冲区，
	* avio_ctx_buffer 的初始地址赋值到 avio_ctx->buffer
	* avio_ctx_buffer_size是缓冲区大小 , 也是每次读取数据的大小
	* bd 是输入文件文件的映射文件
	* read_packet 回调函数,读取数据的功能 , 具体在什么情况下才会回调 ?
	*/

	avio_ctx = avio_alloc_context(avio_ctx_buffer, avio_ctx_buffer_size,
		0, this, &read_packet, NULL, NULL);
	if (!avio_ctx)
	{
		ret = AVERROR(ENOMEM);
		fprintf(stdout, " avio_alloc_context error:%d \n", ret);

		goto end;
	}
	fmt_ctx->pb = avio_ctx;
	/* 配置初始化信息
	* read_packet 回调函数会在这里被调用, 它将输入文件的所有数据都先存入缓存中,
	* 如果后面有需要用到数据，那么它就从缓存中直接调用数据
	*/

	ret = avformat_open_input(&fmt_ctx, NULL, NULL, NULL);
	if (ret < 0)
	{
		fprintf(stdout, " avformat_open_input return error:%d \n", ret);

		goto end;
	}
	// fprintf(stdout, " av_probe_input_buffer: \n");
	// ret=av_probe_input_buffer(avio_ctx, &fmt_ctx, "", NULL, 0, 0)
	// if ( < 0){
	// 	goto end;
	// }

	ret = avformat_find_stream_info(fmt_ctx, NULL);
	if (ret < 0)
	{
		fprintf(stderr, "Could not find stream information\n");
		goto end;
	}
	for (int i = 0; i < fmt_ctx->nb_streams; i++)
	{
		avStream = fmt_ctx->streams[i];
		if (avStream->codecpar->codec_type == AVMEDIA_TYPE_AUDIO)
		{
			audio_stream_idx = i;
			break;
		}
	}
	if (audio_stream_idx < 0 || avStream==NULL)
	{
		ret = -1;
		fprintf(stdout, " no audio stream---: \n");
	}
	audio_dec = avcodec_find_decoder(avStream->codecpar->codec_id);
	if (!audio_dec)
	{
		fprintf(stderr, "avcodec_find_decoder %d error\n", avStream->codecpar->codec_id);
		goto end;
	}

	audio_dec_ctx = avcodec_alloc_context3(audio_dec);
	if (audio_dec_ctx == NULL)
	{
		fprintf(stderr, "avcodec_alloc_context3 error\n");
		goto end;
	}
	ret = avcodec_parameters_to_context(audio_dec_ctx, avStream->codecpar);

	if (ret < 0)
	{
		fprintf(stderr, "avcodec_parameters_to_context error\n");
		ret = -5;
		goto end;
	}
	//通过缓存的方式，无法获得文件的时间长度
	//	duration = (double)fmt_ctx->duration / AV_TIME_BASE;
	audioInfo.sampleRate = audio_dec_ctx->sample_rate;
	audioInfo.channelNum = audio_dec_ctx->channels;
	audioInfo.bitrate = audio_dec_ctx->bit_rate;
	audioInfo.codeID = (int)audio_dec_ctx->codec_id;

	// fprintf(stdout, "audio sample_rate: %d\n", audio_dec_ctx->sample_rate);
	// fprintf(stdout, "audio channels: %d\n", audio_dec_ctx->channels);
	// fprintf(stdout, "audio bitrate %d kb/s\n", (int)audio_dec_ctx->bit_rate / 1000);
	//	fprintf(stdout, "audio duration %d \n", (int)duration);

	//获得流的信息
end:
	if (audio_dec_ctx)
		avcodec_free_context(&audio_dec_ctx);
	if (fmt_ctx)
	{
		avformat_close_input(&fmt_ctx);
		avformat_free_context(fmt_ctx);
	}

	if (avio_ctx)
	{
		av_freep(&avio_ctx->buffer);
		av_freep(&avio_ctx);
	}
	if (ret == 0)
		return &audioInfo;
	else
		return NULL;

	//fprintf(stdout, "probe ok return %d\n", ret);
	//return ret;
}