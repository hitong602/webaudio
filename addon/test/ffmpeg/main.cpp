#include <stdio.h>
#include <stdlib.h>
#include <SDL.h>
#include <SDL_surface.h>
#include <math.h>

#include "ffmpegAPI.h"


FFMPEGAPI g_ffmpegAPI;

FILE* f, * outfile;
static int ReadFile(const char* buf, int len) {
	return  fread((void*)buf, 1, len, f);
}

static int WriteFile(const char* buf, int len) {
	return 0;

}
//void DrawPixel(SDL_Surface* screen, int x, int y, Uint32 color) {
//	Uint16* bufp;
//	if (SDL_MUSTLOCK(screen)) {
//		if (SDL_LockSurface(screen) < 0) {
//			return;
//		}
//	}
//	bufp = (Uint16*)screen->pixels + y * screen->pitch / 2 + x;
//	*bufp = color;
//	if (SDL_MUSTLOCK(screen)) {
//		SDL_UnlockSurface(screen);
//	}
//	SDL_UpdateRect(screen, x, y, 1, 1);
//}
void GetRGBA8888Data(void*& pixels, int& width, int& height)
{
	// for debug
	width = 600;
	height = 400;
	pixels = calloc(width * height * 4, 1);	// 一像素占4字节
}
void PutPixel32_nolock(SDL_Surface* surface, int x, int y, Uint32 color)
{
	Uint8* pixel = (Uint8*)surface->pixels;
	pixel += (y * surface->pitch) + (x * sizeof(Uint32));
	*((Uint32*)pixel) = color;
}

void SDL_DrawLine(SDL_Surface* pDestination, int pX1, int pY1, int pX2, int pY2, Uint32 pColour)
{
	//Get line vector
	float lenX = pX2 - pX1;
	float lenY = pY2 - pY1;

	//Normalize line vector
	float vLen = sqrtf(lenX * lenX + lenY * lenY);
	float incX = lenX / vLen;
	float incY = lenY / vLen;
	float x = pX1;
	float y = pY1;

	SDL_LockSurface(pDestination);
	while (!((int)x == pX2 && (int)y == pY2))
	{
		PutPixel32_nolock(pDestination, (int)x, (int)y, pColour);
		x += incX;
		y += incY;
	}
	SDL_UnlockSurface(pDestination);
}

int drawsin() {

	
	if (SDL_Init(SDL_INIT_VIDEO) == 0) {
		SDL_Window* window = NULL;
		SDL_Renderer* renderer = NULL;
		SDL_Texture* texture=NULL;
		SDL_Event event;



		window = SDL_CreateWindow("example04: for mediaplayer", 100, 100, 700, 590, 0);
		if (NULL == window)    return 0;
		renderer = SDL_CreateRenderer(window, -1, 0);
		if (NULL == renderer)    return 0;

		SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);

		SDL_SetRenderDrawColor(renderer, 0, 255, 0, SDL_ALPHA_OPAQUE);

		void* pixels;
		int  width, height, depth, pitch;

		GetRGBA8888Data(pixels, width, height);
		pitch = height * 4;	// 每行图像所占直接数
		depth = 4 * 8;		// 每像素所占位数		

		int rmask, gmask, bmask, amask;
		//rmask = 0xFF000000; gmask = 0x00FF0000; bmask = 0x0000FF00; amask = 0x000000FF;	// RGBA8888模式
		rmask = 0xFF000000; gmask = 0x00FF0000; bmask = 0x0000FF00; amask = 0x00000000;	// RGB8888模式
		SDL_Surface* pTmpSurface = SDL_CreateRGBSurfaceFrom(pixels, width, height, depth, pitch, rmask, gmask, bmask, amask);
		if (NULL == pTmpSurface)    return 0;



		const char* pBuf;
		int samplecount = g_ffmpegAPI.getWaveformBuffer(&pBuf) / sizeof(WFM_Stereo_SampleBlock);
		WFM_Stereo_SampleBlock* pwfmBuf = (WFM_Stereo_SampleBlock*)pBuf;

		
		int count = MIN(1000, samplecount);
		for (int i = 0; i < count; i++) {
			int l = pwfmBuf[i].left / 32768.0 * 240;
			if (l < 0)
				l = 0;
			if (l > 240)
				l = 240;

			int r = pwfmBuf[i].right / 32768.0 * 240;
			if (r < 0)
				r = 0;
			if (r > 240)
				r = 240;
			SDL_DrawLine(pTmpSurface, i, 240 - l, i, 240 + r, 0x00FFFF00);

		}

		texture = SDL_CreateTextureFromSurface(renderer, pTmpSurface);
		SDL_FreeSurface(pTmpSurface);

		if (NULL == texture)    return 0;
		SDL_RenderClear(renderer);
		SDL_RenderCopy(renderer, texture, NULL, NULL); // Copy the texture into render
		SDL_RenderPresent(renderer); // Show render on window

		bool gameRunning = true;
		while (gameRunning)
		{

			if (SDL_PollEvent(&event))
			{

				if (event.type == SDL_QUIT)
				{
					gameRunning = false;
				}

			}
		}


		
		if (renderer) {
			SDL_DestroyRenderer(renderer);
		}
		if (window) {
			SDL_DestroyWindow(window);
		}
	}
	SDL_Quit();
	return 0;
}
int main(int argc, char** argv) {

	const char *outfilename, *filename;

	if (argc <= 2) {
		fprintf(stderr, "Usage: %s <input file> <output file>\n", argv[0]);
		exit(0);
	}
	filename = argv[1];
	outfilename = argv[2];

	f = fopen(filename, "rb");
	if (!f) {
		fprintf(stderr, "Could not open %s\n", filename);
		exit(1);
	}
	outfile = fopen(outfilename, "wb");
	if (!outfile) {
		exit(1);
	}

	AudioFileInfo *afi=g_ffmpegAPI.probeaudio(ReadFile);
	fseek(f, 0, SEEK_SET);
	if (afi != NULL)
		g_ffmpegAPI.extractWaveform(afi->codeID, ReadFile, WriteFile);

	fclose(f);
	fclose(outfile);

	drawsin();


	return 0;
}
