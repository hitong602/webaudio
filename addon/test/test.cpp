#include <stdio.h>
#include <SDL2\SDL.h>
#include <SDL2/SDL_timer.h>

#include "AudioProject.h"
#include "FileManager.h"
#include "ffmpegAPI.h"
AudioProject g_AudioProject;
FileManager  g_FileManager;
typedef int (*READFILECALLBACK) (const char*, int size, int pos, int userdata);
FlowBuffer energyBuffer;

FILE* file=NULL;
 
int ReadCallback(const char* pBuf, int bufsize, int pos, void* userdata) {

	fseek(file, pos, SEEK_SET);
	size_t len = fread((void*)pBuf, 1, bufsize, file);
	return len;
}

AudioFileInfo  ProbeFile(std::string key, uintptr_t calback, int userdata)
{

	READFILECALLBACK bc = reinterpret_cast<READFILECALLBACK>(calback);
	printf("ReadFileCallback:%d ", (int)calback);

	FFMPEGAPI ffmpegapi;

	AudioFileInfo afi = ffmpegapi.probeaudio(bc, userdata);

	return afi;
	//if (pafi != NULL) {
	//	//printf("format sample:%d chn:%d bitrate: %d code:%d!\n",pafi->sampleRate,pafi->channelNum,pafi->bitrate,pafi->codeID);
	//	pafi->duration = -1;
	//	return *pafi;
	//}
	//else {
	//	printf("format error !\n");

	//	AudioFileInfo afi;
	//	memset(&afi, 0, sizeof(AudioFileInfo));
	//	afi.codeID = -1;
	//	return afi;
	//}


}
//std::vector<struct _stereosampleblock> 
AudioFileInfo ExtractEnergy(uintptr_t filecallback, int userdata, uintptr_t energybuf, uintptr_t energylen) {

	READFILECALLBACK bc = reinterpret_cast<READFILECALLBACK>(filecallback);

	uintptr_t* eb = reinterpret_cast<uintptr_t*>(energybuf);

	int* el = reinterpret_cast<int*>(energylen);

	//  printf("ReadFileCallback:%d ",(int)filecallback);


	FFMPEGAPI ffmpegapi;
	ffmpegapi.readPosInFile = 0;

	AudioFileInfo afi = ffmpegapi.probeaudio(bc, userdata);
	if (afi.codeID == 0) {
		return afi;
	}
	ffmpegapi.readPosInFile = 0;
	energyBuffer.Clear();

	if (ffmpegapi.beginDecode(afi.codeID) == 0) {

		unsigned char buf[1024+ AV_INPUT_BUFFER_PADDING_SIZE];
		unsigned char out[4096*10];
		int outlen = 4096 * 10;


		FILE* fpcm = fopen("d:\\audio\\a.pcm", "w+b");
	
		int size=fread(buf, 1, 1024, file);

		while (size > 0) {
			outlen = 4096 * 10;
			int res=ffmpegapi.decode(buf, size, out, outlen);

			if (outlen > 0) {
				fwrite(out, 1, outlen, fpcm);
			}
			size = fread(buf, 1, 1024, file);
		}
		fclose(fpcm);
		ffmpegapi.endDecode();
	}




	//WFM_Header wh;

	//memset(&afi, 0, sizeof(AudioFileInfo));
	energyBuffer.Put((const char*)&afi, sizeof(AudioFileInfo));


	int res = ffmpegapi.extractWaveform(&afi, bc, &energyBuffer, 0);
	if (res == 0) {
		//pafi->duration = ffmpegapi.duration;

		//wh.bitrate = pafi->bitrate;
		//wh.channelnum = pafi->channelNum;
		//wh.chunksize = sizeof(WFM_Stereo_SampleBlock);
		//wh.codeID = pafi->codeID;
		//wh.duration = pafi->duration;
		//wh.msperframe = 0;
		//wh.samplerate = pafi->sampleRate;
		//wh.TotalFrames = 0;
		char* h = energyBuffer.GetHeadPt();
		memcpy(h, (const char*)&afi, sizeof(AudioFileInfo));


		*eb = (uintptr_t)h;


		//memcpy(eb,(const char *)&wh,sizeof(WFM_Header));
		*el = energyBuffer.GetEnd();

	}
	else {
		*eb = 0;
		*el = 0;

	}
	return afi;

}


int UnloadAudioEditor()
{
	g_AudioProject.Destroy();
	return 0;
}

int ImportFile(char* key, char* energybuf, int energylen) {
	std::cout << "energybuf:" << (int)energybuf << std::endl;
	std::cout << "len:" << energylen << std::endl;
	MediaFile* mf = g_FileManager.Add(key);
	std::cout << "len:" << (int)mf << std::endl;

	AudioFileInfo* afi_header = (AudioFileInfo*)energybuf;
	WFM_Stereo_SampleBlock* energybody = (WFM_Stereo_SampleBlock*)(energybuf + sizeof(AudioFileInfo));
	mf->m_pEnergyBuf = (WFM_Stereo_SampleBlock*)energybody;
	mf->m_nEnergyBytes = energylen-sizeof(AudioFileInfo);
	std::cout << "EnergyBytes:" << mf->m_nEnergyBytes << std::endl;
	//mf->m_Duration = afi_header->duration;

	mf->m_FileInfo = *afi_header;
	/*mf->m_FileInfo.sampleRate = afi_header->samplerate;
	mf->m_FileInfo.channelNum = afi_header->channelnum;
	mf->m_FileInfo.bitrate = afi_header->bitrate; 
	mf->m_FileInfo.codeID = afi_header->codeID;
	mf->m_FileInfo.duration = afi_header->duration;
	printf("samplerate :%d codeID %d", (int)header->samplerate, header->codeID);*/

	std::cout << "samplerate:" << afi_header->sampleRate << std::endl;


	return g_AudioProject.importFile(mf);


}
int main(int argc, char** argv)
{
	printf("this is an audioeditor for every one!\n");

	errno_t r= fopen_s(&file,"d:\\audio\\0.s48","rb");
	if (r != 0)
		return 0;

	FFMPEGAPI ffmpegapi;
	ffmpegapi.readPosInFile = 0;


	
	g_AudioProject.Create("test", 1024, 400);
	AudioFileInfo afi;

	char* energybuf;
	int  energysize;
	afi=ExtractEnergy((uintptr_t)ReadCallback, 0,(uintptr_t)&energybuf, (uintptr_t )&energysize);

	if (afi.codeID == 0) {
		std::cout << "afi error" << std::endl;

	}
	else
	{
		int res = ImportFile((char*)"zhd", energybuf, energysize);

	}
	//g_AudioProject.RenderTrack();

	

	int quit = 1;
	do {
		SDL_Event event;
		SDL_PollEvent(&event);
		if (!g_AudioProject.handleEvent(event))
			break;
		
		g_AudioProject.Render();
		SDL_Delay(50);




	} while (quit);


	g_AudioProject.Destroy();
	return 0;
}

