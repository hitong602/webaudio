#include "AudioObject.h"
#include "AudioProject.h"
#include "SDLUtils.h"

AudioObject::AudioObject() {
	m_startIndexInWFMFile = 0;
	m_EndIndexInWFMFile = 0;

	m_startTimeInTrack = 0;
	m_endTimeInTrack = 0;

}
AudioObject::~AudioObject() {

}
void AudioObject::Draw(SDL_Surface* surface) {

	char* pBuf;
	int bufSize;
	bool got = m_MediaFile->GetWaveformBuffer(&pBuf, &bufSize);
	if (!got)
		return;
	int samplecount = bufSize / sizeof(WFM_Stereo_SampleBlock);
	WFM_Stereo_SampleBlock* pwfmStartBuf = (WFM_Stereo_SampleBlock*)(pBuf+ m_startIndexInWFMFile* sizeof(WFM_Stereo_SampleBlock));
	WFM_Stereo_SampleBlock* pwfmEndBuf = (WFM_Stereo_SampleBlock*)(pBuf + m_EndIndexInWFMFile * sizeof(WFM_Stereo_SampleBlock));

	int StartTimeInAudioFile = pwfmStartBuf->mspos;
	int EndTimeInAudioFile = pwfmEndBuf->mspos;


	int DrawingPixelStartPos = 0;
	/*int DrawingPixelEndPos = MIN((AudioProject::ScreenRightEdgeTime - AudioProject::ScreenLeftEdgeTime) * AudioProject::zoomScale,
		(EndTimeInAudioFile - AudioProject::ScreenLeftEdgeTime) * AudioProject::zoomScale);*/

	


	if (AudioProject::ScreenLeftEdgeTime < m_endTimeInTrack && AudioProject::ScreenLeftEdgeTime >= m_startTimeInTrack) {
		//从中间，从头画一直划到object尾
		DrawingPixelStartPos = 0;
	}
	else if (m_startTimeInTrack > AudioProject::ScreenLeftEdgeTime&& m_startTimeInTrack < AudioProject::TrackScreenWidth * AudioProject::zoomScale) {
		//
		DrawingPixelStartPos =(int)( (m_startTimeInTrack - AudioProject::ScreenLeftEdgeTime) * AudioProject::zoomScale);
	}
	else
		return;

	//确定结束位置
	int DrawingPixel = DrawingPixelStartPos;
	//for (int pos = DrawingPixelStartPos; pos < DrawingPixelEndPos; pos++) {
	while(DrawingPixel< AudioProject::TrackScreenWidth){
		if (AudioProject::zoomScale >= 1) {

			int l = pwfmStartBuf->left / 32768.0 * AudioProject::TrackScreenHeight/2;
			if (l < 0)
				l = 0;
			if (l > AudioProject::TrackScreenHeight / 2)
				l = AudioProject::TrackScreenHeight / 2;

			int r = pwfmStartBuf->right / 32768.0 * AudioProject::TrackScreenHeight / 2;
			if (r < 0)
				r = 0;
			if (r > AudioProject::TrackScreenHeight / 2)
				r = AudioProject::TrackScreenHeight / 2;
			SDLUtils::SDL_DrawLine(surface, DrawingPixel, AudioProject::TrackScreenHeight / 2 - l, DrawingPixel, AudioProject::TrackScreenHeight / 2 + r, 0x00FF00);
			DrawingPixel += AudioProject::zoomScale;
			pwfmStartBuf += 1;
			if (pwfmStartBuf > pwfmEndBuf)
				break;
		}
		else {

			DrawingPixel += 1;
		}

	}


}

