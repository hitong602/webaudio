#pragma once
#include <SDL_surface.h>
#include "MediaFile.h"
class AudioObject
{
public:
	MediaFile* m_MediaFile;
	
	int  m_startIndexInWFMFile;  //在波形文件索引，可以根据这个索引获得对应的时间和
	int  m_EndIndexInWFMFile; //在文件中的结束位置，单位是ms

	int  m_startTimeInTrack;  //在音轨上开始时间,单位是ms
	int  m_endTimeInTrack; //在音轨上的结束时间，单位是ms
public:
	AudioObject();
	~AudioObject();

public:
	//在这个surface 上画数据
	void Draw(SDL_Surface* surface);  



};

