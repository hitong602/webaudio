#include "AudioProject.h"

#define SUCCESS 0

int AudioProject::playingCursor = 0;
int AudioProject::RangeStartPos = 0;
int AudioProject::RangeEndPos = 0; 
int AudioProject::ScreenLeftEdgeTime = 0;
int AudioProject::TrackScreenWidth = 0;
int AudioProject::TrackScreenHeight = 100;
double AudioProject::zoomScale = 1;

AudioProject::AudioProject() {
	m_SDLScreenSurface = NULL;
	m_SDLWindow = NULL;
	m_szPrjName[0] = '\0';
	margin = 10;
	trackspace = 5;
	/*playingCursor = 0;
	RangeStartPos=0;
	RangeEndPos=0;
	RangeMask = 0;
	ScreenLeftEdgeTime = 0;
	zoomScale = 0;*/

}
AudioProject::~AudioProject() {

}
int AudioProject::Create(const char* szPrjName, int width, int height, int hParentWnd) {
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_TIMER| SDL_INIT_EVENTS) != 0) {
		SDL_Log("Unable to initialize SDL: %s", SDL_GetError());
		return -1;
	}
	sprintf_s(m_szPrjName, 50,szPrjName);

//	m_SDLWindow = SDL_CreateWindowFrom((void*)hParentWnd);
	m_SDLWindow = SDL_CreateWindow("SHOW BMP", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width, height, SDL_WINDOW_SHOWN);

	
	if (m_SDLWindow == NULL)
	{
		SDL_Log("Window could not be created! SDL_Error: %s\n", SDL_GetError());
		return -2;
	}
	m_SDLScreenSurface = SDL_GetWindowSurface(m_SDLWindow);
	SDL_FillRect(m_SDLScreenSurface, NULL, SDL_MapRGB(m_SDLScreenSurface->format, 22, 32, 84));

	this->width = width;
	this->height = height;

	AudioProject::TrackScreenWidth = width - 2 * margin;
	return SUCCESS;
}
int AudioProject::Destroy() {
	SDL_DestroyWindow(m_SDLWindow);
	SDL_Quit();
	return SUCCESS;
}

bool AudioProject::AddTrack() {
	AudioTrack* at = new AudioTrack();

	//int trackcount = m_TrackList.size();
	/*int validh = height - 2 * margin - trackcount * trackspace;
	int trackheight = validh / (trackcount + 1);*/

	at->Create(AudioProject::TrackScreenWidth, AudioProject::TrackScreenHeight);
	m_TrackList.push_back(at);

	RenderTrack();
	return true;
}
void AudioProject::RenderTrack() {
	int tracknum= m_TrackList.size();
	int track_x=margin;
	int track_y= margin;
	for (int i = 0; i < tracknum; i++) {

		SDL_Rect sr,tr;
		sr.x = 0; sr.y = 0;sr.h = m_TrackList[i]->height;sr.w = m_TrackList[i]->width;
		tr.x = track_x; tr.y = track_y; tr.h = m_TrackList[i]->height; tr.w = m_TrackList[i]->width;
		SDL_BlitSurface(m_TrackList[i]->GetSurface(), &sr, m_SDLScreenSurface, &tr);
		track_y += m_TrackList[i]->height + trackspace;
	}
	SDL_UpdateWindowSurface(m_SDLWindow);

}

bool AudioProject::RemoveTrack() {
	return true;
}

AudioTrack* AudioProject::GetActiveTrack() {
	return m_TrackList[0];//avtive track index
}

bool AudioProject::Copy() {
	return true;
}
bool AudioProject::Paste() {
	return true;
}
bool AudioProject::Cut() {
	return true;
}

bool AudioProject::Undo() {
	return true;
}
bool AudioProject::Redo() {
	return true;
}

bool AudioProject::Save() {
	return true;
}
