#pragma once

#include "AudioTrack.h"
#include "MediaFile.h"
#include <vector>
/* 
audioeditor 的一个工程，包含多个音轨，音轨的显示，播放
对外提供一系列的接口
*/
class AudioProject
{
	std::vector<AudioTrack*> m_TrackList;  //存放当前的音轨

	char m_szPrjName[50];

	SDL_Window* m_SDLWindow;
	SDL_Surface* m_SDLScreenSurface;   //整个工程占用的screen
	int width;
	int height;
	int margin;
	int trackspace;
public:
	static int playingCursor;//磁头的位置
	static int RangeStartPos;
	static int RangeEndPos;
//	static int RangeMask; //在哪几个柜显示文件
	static int ScreenLeftEdgeTime; // 屏幕（track ） 左边界对应的时间
	static int TrackScreenWidth; // 屏幕（track ） 的像素宽度
	static int TrackScreenHeight; // 屏幕（track ） 的像素宽度
	static double zoomScale;  //级别 2

	

		 

		 
public :
	AudioProject();
	~AudioProject();
public:
	int Create(const char* szPrjName, int width,int height,int hParentWnd);
	int Destroy();

	bool AddTrack();
	bool RemoveTrack();
	void RenderTrack();


	bool Copy();
	bool Paste();
	bool Cut();

	bool Undo();
	bool Redo();

	bool Save();

	AudioTrack* GetActiveTrack();



};

