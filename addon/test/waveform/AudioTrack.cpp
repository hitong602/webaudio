#include "AudioTrack.h"
#include "SDLUtils.h"
#include "AudioProject.h"

AudioTrack::AudioTrack() {
	m_pWaveformSurface = NULL;
}
AudioTrack::~AudioTrack() {
};

//最好是对半寻找，
int AudioTrack::GetObjectIndex(int timepos, POSTYPE& postype) {
	
	int size = m_AudioObjectList.size();
	if (size == 0) {
		postype = POSTYPE::none;
		return -1;//表示没有
	}
	int MidIndex = size / 2;
	if (MidIndex == 0) {

	}
	
	return 0;
	
}

int AudioTrack::Create(int width,int height) {
	void* pixels;
	int  depth, pitch;

	//SDLUtils::GetRGBA8888Data(pixels, width, height);
	SDLUtils::GetRGB888Data(pixels, width, height);
	pitch = width * 3;	// 每行图像所占字节数
	depth =3 * 8;		// 每像素所占位数		

	int rmask, gmask, bmask, amask;
	//rmask = 0xFF000000; gmask = 0x00FF0000; bmask = 0x0000FF00; amask = 0x000000FF;	// RGBA8888模式
	rmask = 0x0000FF; gmask = 0x0000FF00; bmask = 0x00FF30000; amask = 0x00000000;	// RGB8888模式
	m_pWaveformSurface = SDL_CreateRGBSurfaceFrom(pixels, width, height, depth, pitch, rmask, gmask, bmask, amask);
	if (NULL == m_pWaveformSurface) {
		const char *err=SDL_GetError();
		return -1;

	}
	SDL_Rect sr;
	sr.x = 0; sr.y = 0; sr.w = width; sr.h = height;
	SDL_FillRect(m_pWaveformSurface, &sr, 0xFF0000);
	SDLUtils::SDL_DrawLine(m_pWaveformSurface, 0, height / 2, width, height / 2, 0x00FF00);
	this->width = width;
	this->height = height;
	
	return true;
}
int AudioTrack::Destroy() {
	if(m_pWaveformSurface)
		SDL_FreeSurface(m_pWaveformSurface);
	m_pWaveformSurface = NULL;
	return 0;

}


void AudioTrack::clean()
{
	return ;
}
int AudioTrack::Import(MediaFile* mf, int posInTrack){
	char* pBuf;
	int bufSize;
	bool got = mf->GetWaveformBuffer(&pBuf, &bufSize);
	if (!got)
		return -1;
	int samplecount = bufSize / sizeof(WFM_Stereo_SampleBlock);
	
	AudioObject* ao = new AudioObject();
	ao->m_MediaFile = mf;
	ao->m_startIndexInWFMFile = 0;
	ao->m_EndIndexInWFMFile = samplecount - 1;
	ao->m_startTimeInTrack = posInTrack;
	ao->m_endTimeInTrack = posInTrack + mf->GetDuration();

	POSTYPE pt;
	int inx=GetObjectIndex(posInTrack,pt);
	if (inx == -1) {
		m_AudioObjectList.push_back(ao);
	}
	Refresh();

	return 0;
}
void AudioTrack::Refresh() {
	POSTYPE pt;
	int inx = GetObjectIndex(AudioProject::ScreenLeftEdgeTime, pt);
	if (inx == -1)
		return;
	m_AudioObjectList[inx]->Draw(m_pWaveformSurface);
	
}
int AudioTrack::Copy(){
	return 0;
}
int AudioTrack::Cut(){
	return 0;
}
int AudioTrack::Undo(){
	return 0;
}

