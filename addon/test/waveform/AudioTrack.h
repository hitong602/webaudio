#pragma once
#include <SDL.h>
#include <SDL_surface.h>
#include "MediaFile.h"
#include "AudioObject.h"
#include <vector>

typedef enum _posType{on,before,after,none} POSTYPE; //根据一个时间，得到是都在 一个object上，
class AudioTrack
{
	SDL_Surface* m_pWaveformSurface; //波形surface 所有的元素都在这里的

	std::vector<AudioObject *> m_AudioObjectList;//存放的 AudioObjectList
public:
	AudioTrack();
	~AudioTrack();
private:
	int GetObjectIndex(int timepos,POSTYPE &postype); //根据时间位置，得到当前的AudioObject 索引
	//int  SplitObject(int timepos,AudioObject* obj);  //返回 当前位置后面的object 索引
	void Refresh();//重画当前的track
//	void MoveObjectFrom(int timepos, int duration);
public:
	int width;
	int height;
public :
	int Create(int width, int height);
	int Destroy();
	void clean();
	SDL_Surface* GetSurface() {
		return m_pWaveformSurface;

	}

	//在 位置 position 处， 添加一个AudioObject 对象
	int Import(MediaFile *mf, int posInTrack);
	int Copy();
	int Cut();
	int Undo();

};

