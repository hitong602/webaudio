#include "JSMediaFileManager.h"


//javascript 定义
int JSMediaFileManager::ReadCallback(const char* pBuf, int bufsize, int pos, void* userdata) {

	FILE* f = (FILE*)userdata;
	size_t len=fread((void *)pBuf, 1, bufsize,f);
	return len;
}


//emscripten 定义
int JSMediaFileManager::ProbeFile(char* szFileName, AudioFileInfo* afi) {
	MediaFile mf;
	FILE *f=fopen(szFileName, "r+b");
	if (f) {
		mf.SetReadFileCallback(ReadCallback, f);
		AudioFileInfo  *a=mf.Probe();
		fclose(f);
		memcpy(afi, a, sizeof(AudioFileInfo));
		return 0;
		
		// 有一个全局的变量才行吧
	
	}
	
	return -1;
}

//emscripten 定义,实际int parentid可能不需要
int JSMediaFileManager::CreateProject(int parentid) {
	int ret=m_AudioProject.Create("default", 600,300,parentid);
	if (ret < 0)
		return ret;

	//默认创建一个，后面可以自己增加
	ret=m_AudioProject.AddTrack();
	if (ret < 0)
		return ret;

	return true;

}

//emscripten 定义

int  JSMediaFileManager::OpenFile(char *szFileName,int CodeID) {
	FILE* f = fopen(szFileName, "r+b");
	if (f == NULL)
		return -1;
	MediaFile* mf = new MediaFile();
	mf->SetReadFileCallback(ReadCallback, f);

	AudioFileInfo* a = mf->Probe();
	if (a == NULL)
		return -2;


	int ret=mf->ExtractWfm(a->codeID);
	if (ret==0) {
		AudioTrack *at=m_AudioProject.GetActiveTrack();
		at->Import(mf,0);
		m_AudioProject.RenderTrack();

	}
	return 0;

	//要保存 MediaFile 这个链表， 统一释放。
}

