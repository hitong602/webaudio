#include "MediaFile.h"
#include <stdio.h>




MediaFile::MediaFile() {
	ReadFileCallback = NULL;
	m_Userdata = 0;
	m_Duration = 0;
}
MediaFile::~MediaFile() {

}
bool  MediaFile::GetWaveformBuffer( char** pBuf, int* size) {
	return m_ffmpegApi.getWaveformBuffer(pBuf, size);
}

void MediaFile::SetReadFileCallback(int(*readfileback)(const char*, int size, int pos, void* userdata), void* userdata) {
	ReadFileCallback = readfileback;
	m_Userdata = userdata;

}

AudioFileInfo* MediaFile::Probe() {
	if (ReadFileCallback == nullptr)
		return NULL;
	AudioFileInfo *afi= m_ffmpegApi.probeaudio(ReadFileCallback, m_Userdata);
	return afi;
	
}

int MediaFile::ExtractWfm(int CodeID) {
	if (ReadFileCallback == nullptr)
		return -1;
//此时界面上 应该有一个进度条，表示当前地提取的进度
	int res=m_ffmpegApi.extractWaveform(CodeID, ReadFileCallback, (void*)m_Userdata);
	if (res < 0)
		return res;
	m_Duration = m_ffmpegApi.duration;
	return 0;
}