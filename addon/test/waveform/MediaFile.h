#pragma once

#include "ffmpegAPI.h"
//#include <functional>

typedef int (*READFILECALLBACK)(const char*, int size, int pos, void* userdata);
class MediaFile
{

	FFMPEGAPI m_ffmpegApi;

	READFILECALLBACK ReadFileCallback;  //读文件的回调
	void* m_Userdata;
	int m_Duration;  //文件的长度

public:
	//// 这几个函数，如果是本地文件，可以被javascript 程序代替
	//// 如何是 服务端 的文件，可以直接来读，也可以通过javascript 来读
	// int Read(const char* buf, int len);
	// int Open(char* filePath);
	// int Close();
	int GetDuration() { return m_Duration; }
	bool  GetWaveformBuffer(char** pBuf, int* size);  //文件对应的波形

public:
	MediaFile();
	~MediaFile();
public:
	
	// userdata 应该是保存一个 id，上层文件根据这个id，可以检索的文件的的句柄，对于javascript，可以检索到具体的file object
	void SetReadFileCallback(int(*readfileback)(const char*, int size, int pos, void* userdata), void* userdata);

	AudioFileInfo * Probe();
	int ExtractWfm(int CodeID);
};

