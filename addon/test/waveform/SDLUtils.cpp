#include "SDLUtils.h"
#include <stdlib.h>
#include <math.h>
void SDLUtils::GetRGB888Data(void*& pixels, int& width, int& height) {
	pixels = malloc(width * height * 3);	// 一像素占3字节
}
void SDLUtils::GetRGBA8888Data(void*& pixels, int& width, int& height) {
	pixels = malloc(width * height * 4);	// 一像素占3字节
}


void SDLUtils::PutPixel32_nolock(SDL_Surface* surface, int x, int y, Uint32 color)
{
	Uint8* pixel = (Uint8*)surface->pixels;
	pixel += (y * surface->pitch) + (x * sizeof(Uint32));
	*((Uint32*)pixel) = color;
}

void SDLUtils::PutPixel24_nolock(SDL_Surface* surface, int x, int y, Uint32 color)
{
	Uint8* pixel = (Uint8*)surface->pixels;
	pixel += (y * surface->pitch) + (x * sizeof(Uint8) * 3);
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
	pixel[0] = (color >> 24) & 0xFF;
	pixel[1] = (color >> 16) & 0xFF;
	pixel[2] = (color >> 8) & 0xFF;
#else
	pixel[0] = color & 0xFF;
	pixel[1] = (color >> 8) & 0xFF;
	pixel[2] = (color >> 16) & 0xFF;
#endif
}

void SDLUtils::PutPixel16_nolock(SDL_Surface* surface, int x, int y, Uint32 color)
{
	Uint8* pixel = (Uint8*)surface->pixels;
	pixel += (y * surface->pitch) + (x * sizeof(Uint16));
	*((Uint16*)pixel) = color & 0xFFFF;
}

void SDLUtils::PutPixel8_nolock(SDL_Surface* surface, int x, int y, Uint32 color)
{
	Uint8* pixel = (Uint8*)surface->pixels;
	pixel += (y * surface->pitch) + (x * sizeof(Uint8));
	*pixel = color & 0xFF;
}

void SDLUtils::PutPixel32(SDL_Surface* surface, int x, int y, Uint32 color)
{
	if (SDL_MUSTLOCK(surface))
		SDL_LockSurface(surface);
	PutPixel32_nolock(surface, x, y, color);
	if (SDL_MUSTLOCK(surface))
		SDL_UnlockSurface(surface);
}

void SDLUtils::PutPixel24(SDL_Surface* surface, int x, int y, Uint32 color)
{
	if (SDL_MUSTLOCK(surface))
		SDL_LockSurface(surface);
	PutPixel24_nolock(surface, x, y, color);
	if (SDL_MUSTLOCK(surface))
		SDL_LockSurface(surface);
}

void SDLUtils::PutPixel16(SDL_Surface* surface, int x, int y, Uint32 color)
{
	if (SDL_MUSTLOCK(surface))
		SDL_LockSurface(surface);
	PutPixel16_nolock(surface, x, y, color);
	if (SDL_MUSTLOCK(surface))
		SDL_UnlockSurface(surface);
}

void SDLUtils::PutPixel8(SDL_Surface* surface, int x, int y, Uint32 color)
{
	if (SDL_MUSTLOCK(surface))
		SDL_LockSurface(surface);
	PutPixel8_nolock(surface, x, y, color);
	if (SDL_MUSTLOCK(surface))
		SDL_UnlockSurface(surface);
}
void SDLUtils::SDL_DrawLine(SDL_Surface* pDestination, int pX1, int pY1, int pX2, int pY2, Uint32 pColour)
{
	//Get line vector
	float lenX = pX2 - pX1;
	float lenY = pY2 - pY1;

	//Normalize line vector
	float vLen = sqrtf(lenX * lenX + lenY * lenY);
	float incX = lenX / vLen;
	float incY = lenY / vLen;
	float x = pX1;
	float y = pY1;

	SDL_LockSurface(pDestination);
	while (!((int)x == pX2 && (int)y == pY2))
	{
		//PutPixel32_nolock(pDestination, (int)x, (int)y, pColour);
		PutPixel24_nolock(pDestination, (int)x, (int)y, pColour);
		x += incX;
		y += incY;
	}
	SDL_UnlockSurface(pDestination);
}
