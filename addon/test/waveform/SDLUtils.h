#pragma once
#include <stdlib.h>
#include <SDL_surface.h>
class SDLUtils
{
public:
	static void GetRGB888Data(void*& pixels, int& width, int& height);
	static void GetRGBA8888Data(void*& pixels, int& width, int& height);

	static void PutPixel32_nolock(SDL_Surface* surface, int x, int y, Uint32 color);
	static void PutPixel24_nolock(SDL_Surface* surface, int x, int y, Uint32 color);
	static void PutPixel16_nolock(SDL_Surface* surface, int x, int y, Uint32 color);
	static void PutPixel8_nolock(SDL_Surface* surface, int x, int y, Uint32 color);
	static void PutPixel32(SDL_Surface* surface, int x, int y, Uint32 color);
	static void PutPixel24(SDL_Surface* surface, int x, int y, Uint32 color);
	static void PutPixel16(SDL_Surface* surface, int x, int y, Uint32 color);
	static void PutPixel8(SDL_Surface* surface, int x, int y, Uint32 color);

	static void SDL_DrawLine(SDL_Surface* pDestination, int pX1, int pY1, int pX2, int pY2, Uint32 pColour);


};

