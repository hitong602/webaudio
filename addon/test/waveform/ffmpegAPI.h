 #include <functional>

#define MAX(a,b) (a>b?a:b)
#define MIN(a,b) (a<b?a:b)
#define SAMPLE(x) MIN(0x7FFFF,MAX(0x8000,x>0?(x*32767.0+0.49999):(x*32767.0-0.49999)))
#define ABS(x) (x>=0?x:-x)
typedef struct _AudioFileInfo
{
	int sampleRate;
	int channelNum;
	int bitrate;
	int codeID;

} AudioFileInfo;

typedef struct _Header {
	char header[8];//audioGOO
	int  samplerate;
	int  channelnum;
	int  duration;
	int  TotalFrames;
	int  msperframe;
}WFM_Header;
typedef struct _stereosampleblock {
	short left;
	short right;
	int filepos;  // 在文件中的文字
	int mspos;   // 这一帧对应的时间位置（相对于时间）
} WFM_Stereo_SampleBlock;
typedef struct _sampleblock {
	short left;
	int filepos;  //
	int mspos;
} WFM_Mono_SampleBlock;
class FlowBuffer;
class FFMPEGAPI {
	AudioFileInfo audioInfo; //当前文件的格式，为了返回一个指针（内存)

public:
	void* m_CallbackUserData;
	double duration;// 必须解码完成后，才可以知道最准确的时间长度

public:
	std::function<int(const char*, int size, int pos, void* userdata)> Probe_ReadFileCallback;  //读文件的回调

	FlowBuffer *m_PeakBuffer;

public:
	FFMPEGAPI();
	~FFMPEGAPI();

	bool getWaveformBuffer( char ** pBuf,int *size);
	// 没有必要给位置，和上层要数据的时候，上层程序能知道当前处理的进度
	int extractWaveform(int codeID, int (*readfileback)(const char*, int size, int pos, void* userdata),  void* userdata);
	AudioFileInfo *probeaudio(int (*readfileback)(const char*, int size, int pos, void* userdata), void* userdata);

};