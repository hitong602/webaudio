import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import {WorkshopComponent} from './workshop/workshop.component';

import {PageNotFoundComponent} from './error-page/page-not-found.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', loadChildren: './home/home.module#HomeModule' },
  { path: 'login', loadChildren: () => import('./auth/login/login.module').then(m => m.LoginModule) },
  { path: 'workshop', component: WorkshopComponent , children: [
      { path: '', loadChildren : () => import('./workshop/mediatools/mediatools.module').then(m => m.MediatoolsModule) },
      { path: 'netdisk', loadChildren : () => import('./workshop/netdisk/netdisk.module').then(m => m.NetdiskModule) },
      { path: 'workstream', loadChildren : () => import('./workshop/workstream/workstream.module').then(m => m.WorkstreamModule) },
      { path: 'mediatools', loadChildren : () => import('./workshop/mediatools/mediatools.module').then(m => m.MediatoolsModule) }
  ]},
  { path: 'register', loadChildren : () => import('./auth/register/register.module').then(m => m.RegisterModule) },
  { path: '**', component: PageNotFoundComponent }
];


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
