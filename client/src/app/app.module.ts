import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {PageNotFoundComponent} from './error-page/page-not-found.component';

import { AppSettings } from './app.settings';
import { MaterialSharedModule} from './shared/material.shared.module';
import {API_URL} from './app.tokens';
import { HttpClientModule } from "@angular/common/http";


import {WorkshopComponent} from './workshop/workshop.component';



@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    WorkshopComponent
    
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
	MaterialSharedModule,
    HttpClientModule
  ],
  providers: [
    AppSettings,
    {
      provide: API_URL,
      // useValue: 'https://www.classon.top/api'
       useValue: 'http://localhost:3000/api'
    }
  ],
  exports: [

  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
