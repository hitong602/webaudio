import { Injectable } from '@angular/core';
import { Settings } from './app.settings.model';
import { User } from './model/userInfo.model';

@Injectable()
export class AppSettings {
    public settings = new Settings(
        'myclass',   // theme name
        true,       // loadingSpinner
        false,       // fixedHeaders
        true,       // sidenavIsOpened
        true,       // sidenavIsPinned
        true,       // sidenavUserBlock
        'vertical', // horizontal , vertical
        'compact',  // default, compact, mini
        'teal-light',   // indigo-light, teal-light, red-light, blue-dark, green-dark, pink-dark
        false       // true = rtl, false = ltr
	);
	public currentUser = new User(0, false, '', '', '', '我', '', '', null);


}

