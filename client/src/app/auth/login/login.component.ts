import { Component, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AppSettings } from '../../app.settings';
import { Company } from "../../model/userInfo.model";
import { MatSnackBar } from "@angular/material/snack-bar";
import { MatTabChangeEvent } from '@angular/material/tabs';
import { MatDialog } from "@angular/material/dialog";

import { UserService } from "../../webapi/user.service";

import {SelectCompanyComponent} from './select-company/select-company.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  providers: [UserService]


})
export class LoginComponent implements AfterViewInit {
  public loginForm: FormGroup;
  private tabindex: any;
  constructor(public appSettings: AppSettings,
              private snackBar: MatSnackBar,
              public fb: FormBuilder,
              private userService: UserService,
              public router: Router,
              public selectDialog: MatDialog) {
    this.tabindex = 0;
    this.loginForm = this.fb.group({
      telnumber: [null, Validators.compose([
        Validators.required,
        Validators.maxLength(11),
        Validators.pattern(/^((1[3,5,8][0-9])|(14[5,7])|(17[0,6,7,8])|(19[7]))\d{8}$/)])],
      password: [null, Validators.compose([
        Validators.required, Validators.minLength(6),
        Validators.maxLength(12),
        Validators.pattern(/^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,20}$/)])],
      verificationcode: [null, Validators.compose([
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(6),
        Validators.pattern(/^[0-9]{1,20}$/)])],
        username: [null, Validators.compose([
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(12)
          ])]
    });
  }
  get telnumber() { return this.loginForm.get("telnumber"); }

  get smscode() { return this.loginForm.get("verificationcode"); }

  get password() { return this.loginForm.get("password"); }

  get username() { return this.loginForm.get("username"); }

  getUsernameErrorString() {
		if (this.username.hasError("required") === true) {
			return "请输入姓名";
		}
		if (this.username.hasError("maxlength") === true) {
			return "姓名的长度最大为20个字符";
		}
		if (this.username.hasError("minlength") === true) {
			return "姓名的长度最小为2个字符";
		}

		return "";
	}
  getTelnumberErrorString() {
    if (this.loginForm.controls.telnumber.hasError("required") === true) {
      return "请输入手机号";
    }
    if (this.loginForm.controls.telnumber.hasError("maxlength") === true) {
      return "手机号的长度为11位";
    }
    if (this.loginForm.controls.telnumber.hasError("pattern") === true) {
      return "手机号码格式不正确";
    }

    return "";
  }
  getVerificationCodeErrorString() {
    if (this.loginForm.controls.verificationcode.hasError("required") === true) {
      return "验证码不能为空";
    }
    if (this.loginForm.controls.verificationcode.hasError("maxlength") === true ||
      this.loginForm.controls.verificationcode.hasError("minlength") === true) {
      return "验证码长度为6个数字字符";
    }
    if (this.loginForm.controls.verificationcode.hasError("pattern") === true) {
      return "验证码只能为数字";
    }
    if (this.loginForm.controls.verificationcode.hasError("notequal") === true) {
      return "验证码不正确";
    }

    return "";
  }
  getPasswordErrorString() {
    if (this.password.hasError("required") === true) {
      return "请输入密码";
    }
    if (this.password.hasError("maxlength") === true) {
      return "密码的长度最大为12位";
    }
    if (this.password.hasError("minlength") === true) {
      return "密码的长度为6位";
    }
    if (this.password.hasError("pattern") === true) {
      return "密码由6~12位的数字和字母组成";
    }

    return "";
  }

  onSelectedTabChange($event) {
    this.tabindex = $event instanceof MatTabChangeEvent ? $event.index : $event;
  }
  OnSmsChanged(code: string) {
    // this.smsVertifyCode = code;
    // 有错误 发生了
    this.snackBar.open(code);

  }
  openSelectDialog(): void {
		const dialogRef = this.selectDialog.open(SelectCompanyComponent, {
			width: "40vw",
			height: "50vh",
			disableClose: true,
			data: { companys: this.appSettings.currentUser.companylist }
		});

		dialogRef.afterClosed().subscribe(result => {

			if (result === "gotoworkshop") {
				this.router.navigateByUrl("/workshop");

			} else if (result === "RegUserInfo") {
				this.router.navigateByUrl("/register/profile");

			}

		});
	}
  public onSubmit(): void {

    let username;
    let pwd;
    let type;
    if (this.tabindex === 0) {
      if (this.username.invalid || this.password.invalid) {
        return;
      }
      username = this.username.value;
      pwd = this.password.value;
      type = "pwd";
    } else {
      if (this.telnumber.invalid || this.smscode.invalid) {
        return;
      }
      username = this.telnumber.value;
      pwd = this.smscode.value;
      type = "sms";

    }
    this.userService.login(username, pwd, type).subscribe(
      (data: any) => {
          // 当前应该是一个人，
          this.appSettings.currentUser.id = data.PersonInfo[0].personid;
          this.appSettings.currentUser.nickName = data.PersonInfo[0].nickname;
          this.appSettings.currentUser.token = data.token;
          const companys: Array<Company> = new Array<Company>();

          // tslint:disable-next-line: prefer-for-of
          for (let i = 0; i < data.PersonInfo.length; i++) {
            if (data.PersonInfo[i].companyid != null) {
              const c = {
                id: data.PersonInfo[i].companyid,
                name: data.PersonInfo[i].companyname
              };

              companys.push(c);
            }
          }
          this.appSettings.currentUser.companylist = companys;
          this.snackBar.dismiss();
          // this.router.navigateByUrl('/workshop');
          this.openSelectDialog();
      },
      err => {
        this.snackBar.open(err.error);
      });



  }

  ngAfterViewInit() {
		setTimeout(() => { this.appSettings.settings.loadingSpinner = false; }, 300);
  }
}
