import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialSharedModule } from '../../shared/material.shared.module';
import {ThemeSharedModule} from '../../shared/theme.shared.module';

import { LoginComponent } from './login.component';
import { HttpClientModule } from "@angular/common/http";
import { SelectCompanyComponent } from './select-company/select-company.component';


export const routes = [
  { path: '', component: LoginComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    MaterialSharedModule,
    ThemeSharedModule
  ],
  declarations: [
    LoginComponent,
    SelectCompanyComponent
  ],
  entryComponents: [
    SelectCompanyComponent
  ],
  bootstrap: [LoginComponent]

})
export class LoginModule { }
