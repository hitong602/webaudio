import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

@Component({
  selector: 'app-select-company',
  templateUrl: './select-company.component.html',
  styleUrls: ['./select-company.component.scss']
})
export class SelectCompanyComponent implements OnInit {

  public comanylist: any;
  constructor(public dialogRef: MatDialogRef<SelectCompanyComponent>,
		            @Inject(MAT_DIALOG_DATA) public data: any) {
      this.comanylist = data.companys;
     }

     SelectCompany(id: any) {
			this.dialogRef.close("gotoworkshop");


		}
  ngOnInit() {
  }

}
