import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from "@angular/forms";
import { Router } from "@angular/router";

import { AppSettings } from "../../../app.settings";
import { UserService } from "../../../webapi/user.service";
import { MatSnackBar } from "@angular/material/snack-bar";


@Component({
	selector: 'app-profile',
	templateUrl: './profile.component.html',
	styleUrls: ['./profile.component.scss'],
	providers: [UserService]
})
export class ProfileComponent implements OnInit {

	profileForm: FormGroup;

	constructor(
		private fb: FormBuilder,
		private snackBar: MatSnackBar,
		private appSettings: AppSettings,
		private userService: UserService,
		private router: Router,

		) {
		this.profileForm = this.fb.group({
			username: [
				"",
				{
					validators: [Validators.required,
					Validators.maxLength(20),
					Validators.minLength(2)]
				}
			],
			password: [
				"",
				{
					validators: [Validators.required,
					Validators.minLength(6),
					Validators.maxLength(12),
					Validators.pattern(/^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,20}$/)]
				}
			],
			companyname: [
				"",
				{
					validators: [Validators.required,
					Validators.minLength(2),
					Validators.maxLength(20)]
				}
			]

		});
	}
	get username() { return this.profileForm.get("username"); }

	get password() { return this.profileForm.get("password"); }

	get companyname() { return this.profileForm.get("companyname"); }

	ngOnInit() {
	}
	getUsernameErrorString() {
		if (this.username.hasError("required") === true) {
			return "请输入姓名";
		}
		if (this.username.hasError("maxlength") === true) {
			return "姓名的长度最大为20个字符";
		}
		if (this.username.hasError("minlength") === true) {
			return "姓名的长度最小为2个字符";
		}

		return "";
	}
	getPasswordErrorString() {
		if (this.password.hasError("required") === true) {
			return "请输入密码";
		}
		if (this.password.hasError("maxlength") === true) {
			return "密码的长度最大为12位";
		}
		if (this.password.hasError("minlength") === true) {
			return "密码的长度为6位";
		}
		if (this.password.hasError("pattern") === true) {
			return "密码由6~12位的数字和字母组成";
		}

		return "";
	}

	getCompanynameErrorString() {
		if (this.companyname.hasError("required") === true) {
			return "请输入单位名称";
		}
		if (this.companyname.hasError("maxlength") === true) {
			return "单位名称的最大长度为20个字符";
		}
		if (this.companyname.hasError("minlength") === true) {
			return "单位名称的最小长度为2个自负";
		}

		return "";
	}
	onSubmit() {
		const formModel = this.profileForm.value;

		this.userService.updateprofile(formModel.companyname, formModel.username, formModel.password).subscribe(
			(data: any) => {
				const c = {
					id: data.companyid,
					name: formModel.companyname
				};

				this.appSettings.currentUser.companylist.push(c);
				this.router.navigateByUrl("/workshop");
			},
			(err) => {
				this.snackBar.open(err.error.message);

			});
	}
}
