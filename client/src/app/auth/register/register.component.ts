import { Component, AfterViewInit } from "@angular/core";
import { Router } from "@angular/router";
import { Validators, FormGroup, FormBuilder } from "@angular/forms";

import { AppSettings } from "../../app.settings";
import { UserService } from "../../webapi/user.service";

import { MatSnackBar } from "@angular/material/snack-bar";
import { Company } from "../../model/userInfo.model";
import { MatDialog } from "@angular/material/dialog";

import { SelectCompanyDialogComponent } from "./select-company-dialog/select-company-dialog.component";
// export function smsCodeMatcher(smsvalue: string): ValidatorFn {
// 	return (c: AbstractControl): { [key: string]: any } => {

// 		const v = c.value; // 获取应用该指令，控件上的值

// 		if (v && (isNaN(v)) || (v !== smsvalue)) {

// 			return { notequal: true };
// 		}

// 		return null;
// 	};
// }


@Component({
	selector: "app-register",
	templateUrl: "./register.component.html",
	providers: [UserService]
})
export class RegisterComponent implements AfterViewInit {

	registerForm: FormGroup;
	constructor(
		private fb: FormBuilder,
		public appSettings: AppSettings,
		private router: Router,
		private snackBar: MatSnackBar,
		private userService: UserService,
		public selectDialog: MatDialog) {
		this.registerForm = this.fb.group({

			telnumber: [
				"",
				{
					validators: [Validators.required,
					Validators.maxLength(11),
					Validators.pattern(/^((1[3,5,8][0-9])|(14[5,7])|(17[0,6,7,8])|(19[7]))\d{8}$/)]
				}
			],
			verificationcode: [
				"",
				{
					validators: [Validators.required,
					Validators.minLength(6),
					Validators.maxLength(6),
					Validators.pattern(/^[0-9]{1,20}$/)]

				}
			]

		});
	}
	get phonenumber() { return this.registerForm.get("telnumber"); }

	get smscode() { return this.registerForm.get("verificationcode"); }

	ngAfterViewInit() {
		setTimeout(() => { this.appSettings.settings.loadingSpinner = false; }, 300);

	}

	getTelnumberErrorString() {
		if (this.registerForm.controls.telnumber.hasError("required") === true) {
			return "请输入手机号";
		}
		if (this.registerForm.controls.telnumber.hasError("maxlength") === true) {
			return "手机号的长度为11位";
		}
		if (this.registerForm.controls.telnumber.hasError("pattern") === true) {
			return "手机号码格式不正确";
		}

		return "";
	}
	getVerificationCodeErrorString() {
		if (this.registerForm.controls.verificationcode.hasError("required") === true) {
			return "验证码不能为空";
		}
		if (this.registerForm.controls.verificationcode.hasError("maxlength") === true ||
			this.registerForm.controls.verificationcode.hasError("minlength") === true) {
			return "验证码长度为6个数字字符";
		}
		if (this.registerForm.controls.verificationcode.hasError("pattern") === true) {
			return "验证码只能为数字";
		}
		if (this.registerForm.controls.verificationcode.hasError("notequal") === true) {
			return "验证码不正确";
		}

		return "";
	}
	OnSmsChanged(code: string) {
		// this.smsVertifyCode = code;
		// 有错误 发生了
		this.snackBar.open(code);

	}
	openSelectDialog(): void {
		const dialogRef = this.selectDialog.open(SelectCompanyDialogComponent, {
			width: "40vw",
			height: "50vh",
			disableClose: true,
			data: { companys: this.appSettings.currentUser.companylist }
		});

		dialogRef.afterClosed().subscribe(result => {

			if (result === "gotoworkshop") {
				this.router.navigateByUrl("/workshop");

			} else if (result === "RegUserInfo") {
				this.router.navigateByUrl("/register/profile");

			}

		});
	}


	onSubmit() {
		const formModel = this.registerForm.value;

		const telnumber = formModel.telnumber;


		if (!this.registerForm.valid) {
			return;
		}

		this.userService.create(telnumber, formModel.verificationcode).subscribe(
			(data: any) => {
				// 当前应该是一个人，
				this.appSettings.currentUser.id = data.PersonInfo[0].personid;
				this.appSettings.currentUser.nickName = data.PersonInfo[0].nickname;
				this.appSettings.currentUser.token = data.token;
				const companys: Array<Company> = new Array<Company>();


				// tslint:disable-next-line: prefer-for-of
				for (let i = 0; i < data.PersonInfo.length; i++) {
					if (data.PersonInfo[i].companyid != null) {
						const c = {
							id: data.PersonInfo[i].companyid,
							name: data.PersonInfo[i].companyname
						};

						companys.push(c);
					}
				}
				if (companys.length === 0) {
					this.appSettings.currentUser.companylist = null;
				} else {
					this.appSettings.currentUser.companylist = companys;
					this.openSelectDialog();

				}
				// 到下一步注册步骤
				this.snackBar.dismiss();

				//  this.router.navigateByUrl('/login');
			},
			err => {
				this.snackBar.open(err.message);
			});


	}
}
