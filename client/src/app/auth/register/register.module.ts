import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MaterialSharedModule } from "../../shared/material.shared.module";
import {ThemeSharedModule} from '../../shared/theme.shared.module';
import { RegisterComponent } from "./register.component";
import { HttpClientModule } from "@angular/common/http";

// import {SmsverifycodeComponent} from "../../theme/components/smsverifycode/smsverifycode.component";
import {BlurValidateDirective} from "../../theme/directives/blur-validate.directive";
import { SelectCompanyDialogComponent } from "./select-company-dialog/select-company-dialog.component";
import {ProfileComponent} from "./profile/profile.component";
export const routes = [
  { path: '', component: RegisterComponent, pathMatch: 'full'},
  { path: 'profile', component: ProfileComponent, pathMatch: 'full'}

];



@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MaterialSharedModule,
    ThemeSharedModule
  ],
  declarations: [
    RegisterComponent,
    BlurValidateDirective,
    SelectCompanyDialogComponent,
    ProfileComponent

  ],
  entryComponents: [
    SelectCompanyDialogComponent
  ],
  bootstrap: [RegisterComponent]

})
export class RegisterModule {}


