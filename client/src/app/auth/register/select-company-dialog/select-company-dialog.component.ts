import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

@Component({
	selector: "app-select-company-dialog",
	templateUrl: "./select-company-dialog.component.html",
	styleUrls: ["./select-company-dialog.component.scss"]
})
export class SelectCompanyDialogComponent  {

	public comanylist: any;
	constructor(
		public dialogRef: MatDialogRef<SelectCompanyDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any) {
			this.comanylist = data.companys;
		}
		SelectCompany(id: any) {
			this.dialogRef.close("gotoworkshop");


		}
		FillProfile() {
			this.dialogRef.close("RegUserInfo");


		}



}
