import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';

import {AppSettings} from '../app.settings';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, AfterViewInit {


  constructor(public appSettings: AppSettings, public router: Router) { }

  ngOnInit() {
  }
  ngAfterViewInit() {
    setTimeout(() => { this.appSettings.settings.loadingSpinner = false; }, 300);
  }
  Register() {

    this.router.navigateByUrl('/register');
  }
  Login() {

    this.router.navigateByUrl('/login');
  }

}
