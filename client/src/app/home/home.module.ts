import { NgModule  } from '@angular/core';
import { CommonModule  } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {ReactiveFormsModule} from '@angular/forms'
import { HomeRoutingModule } from './home-routing.module';


import {HomeComponent} from './home.component';

import { MaterialSharedModule} from '../shared/material.shared.module';
import { Section1Component } from './section1/section1.component';
import { Section2Component } from './section2/section2.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HomeRoutingModule,
    MaterialSharedModule
  ],
  declarations: [
    HomeComponent,
    Section1Component,
    Section2Component
    ]
})
export class HomeModule {


 }
