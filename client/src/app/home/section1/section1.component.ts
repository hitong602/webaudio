import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { UserService } from "../../webapi/user.service";
import { AppSettings } from '../../app.settings';
import { Company } from "../../model/userInfo.model";

@Component({
  selector: 'app-section1',
  templateUrl: './section1.component.html',
  styleUrls: ['./section1.component.scss'],
  providers: [UserService]
})
export class Section1Component implements OnInit {

  constructor(public appSettings: AppSettings,
              private userService: UserService,
              public router: Router) { }

  ngOnInit() {
  }

  OnTryDemo() {
    const username = '18610310204';
    const pwd = '123456';
    const type = 'sms';

    //this.appSettings.currentUser.companylist = companys;
    this.router.navigateByUrl('/workshop');


    // this.userService.login(username, pwd, type).subscribe(

      
    //   (data: any) => {
    //       // 当前应该是一个人，
    //       this.appSettings.currentUser.id = data.PersonInfo[0].personid;
    //       this.appSettings.currentUser.nickName = data.PersonInfo[0].nickname;
    //       this.appSettings.currentUser.token = data.token;
    //       const companys: Array<Company> = new Array<Company>();

    //       // tslint:disable-next-line: prefer-for-of
    //       for (let i = 0; i < data.PersonInfo.length; i++) {
    //         if (data.PersonInfo[i].companyid != null) {
    //           const c = {
    //             id: data.PersonInfo[i].companyid,
    //             name: data.PersonInfo[i].companyname
    //           };

    //           companys.push(c);
    //         }
    //       }
    //       this.appSettings.currentUser.companylist = companys;
    //       this.router.navigateByUrl('/workshop');

    //   },
    //   err => {
    //   });
  }

}
