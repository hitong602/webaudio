
export class Company {
  public id: number;
  public name: string;
}
export class User {
  constructor(
  public id: number,
  public login: boolean,
  public token: string,
  public name: string,
  public avatar: string,
  public nickName: string,
  public eMail: string,
  public phoneNumber: string,
  public companylist: any) {}
}

