import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CommonModule } from '@angular/common';
// import { MaterialSharedModule } from './material.shared.module';

import {MatDialogModule ,
	MatButtonModule,
	MatCardModule,
	MatIconModule,
	MatToolbarModule,
	MatListModule} from '@angular/material';

import { SmsverifycodeComponent } from '../theme/components/smsverifycode/smsverifycode.component';

import { SidenavComponent } from '../theme/components/sidenav/sidenav.component';
import { VerticalMenuComponent } from '../theme/components/menu/vertical-menu/vertical-menu.component';
import { BreadcrumbComponent } from '../theme/components/breadcrumb/breadcrumb.component';
import { UploadingdialogComponent } from '../theme/components/uploadingdialog/uploadingdialog.component';
@NgModule({
	declarations: [
		SmsverifycodeComponent,
		SidenavComponent,
		VerticalMenuComponent,
		BreadcrumbComponent,
 		UploadingdialogComponent
	],
	imports: [
		CommonModule,
		RouterModule,
		MatDialogModule,
		MatButtonModule,
		MatCardModule,
		MatIconModule,
		MatToolbarModule,
		MatListModule
	// 	MaterialSharedModule
	],
	exports: [
		SmsverifycodeComponent,
		SidenavComponent,
		VerticalMenuComponent,
		BreadcrumbComponent,
		UploadingdialogComponent
	],
	entryComponents: [
		UploadingdialogComponent
	  ]

})
export class ThemeSharedModule { }
