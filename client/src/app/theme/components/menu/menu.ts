import { Menu } from './menu.model';

// export const mainMenuItems = [
//     new Menu (1, '网盘', '/workshop/netdisk', null, 'iconwangpan', null, false, 0),
//     new Menu (2, '工作', '/workshop/workstream', null, 'iconaudio-editing', null, false, 0),
//     new Menu (3, '工具', '/workshop/mediatools', null, 'iconaudio-editing', null, false, 0),

// ];

export const mainMenuItems = [
    new Menu (1, '工具', '/workshop/mediatools', null, 'iconaudio-editing', null, false, 0),

];
export const netDriverMenuItems = [
    new Menu (1, '公共网盘', null, null, 'iconwangpan', null, true, 0),
    new Menu (2, '个人网盘', null, null, 'iconaudio-editing', null, true, 0),
    new Menu (3, '我的共享', '/workshop/netdisk/shareme', null, 'iconzhuanhuanzhe', null, false, 0),
    new Menu (4, '音频文件', '/workshop/netdisk/audfilemanager', null, 'iconzhuanhuanzhe', null, false, 1),
    new Menu (5, '文稿', '/workshop/netdisk/news', null, 'iconzhuanhuanzhe', null, false, 1),
    new Menu (6, '回收站', '/workshop/netdisk/recyclebin', null, 'iconzhuanhuanzhe', null, false, 0),
    new Menu (7, '音频文件', '/workshop/netdisk/audfilemanager', null, 'iconzhuanhuanzhe', null, false, 2),
    new Menu (8, '文稿', '/workshop/netdisk/news', null, 'iconzhuanhuanzhe', null, false, 2)
];

export const WorksMenuItems = [
    new Menu (1, '日常工作', '/workshop', null, 'iconwangpan', null, false, 0),
    new Menu (2, '临时工作', '/workshop', null, 'iconaudio-editing', null, false, 0)
];

export const mediaToolsMenuItems = [
    //new Menu (1, '公共网盘', null, null, 'iconwangpan', null, true, 0),
    new Menu (1, '编辑', null, null, 'iconaudio-editing', null, true, 0),

];

