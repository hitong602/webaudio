import { Component, OnInit, ViewEncapsulation , Input} from '@angular/core';
import { AppSettings } from '../../../app.settings';
import { Settings } from '../../../app.settings.model';
import { MenuService } from '../menu/menu.service';

import {User} from '../../../model/userInfo.model';
@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [ MenuService ]
})
export class SidenavComponent implements OnInit {
  public settings: Settings;
  @Input() menuItems: Array<any>;

  public user: User;
  constructor(public appSettings: AppSettings, public menuService: MenuService) {
      this.settings = this.appSettings.settings;
      this.user = this.appSettings.currentUser;
  }

  ngOnInit() {
     // this.menuItems = this.menuService.getVerticalMenuItems();
     // 依靠 外部传递进来
  }

  public closeSubMenus() {
    const menu = document.querySelector(".sidenav-menu-outer");
    if (menu) {
      // tslint:disable-next-line: prefer-for-of
      for (let i = 0; i < menu.children[0].children.length; i++) {
        const child = menu.children[0].children[i];
        if (child) {
          if (child.children[0].classList.contains('expanded')) {
              child.children[0].classList.remove('expanded');
              child.children[1].classList.remove('show');
          }
        }
      }
    }
  }

}
