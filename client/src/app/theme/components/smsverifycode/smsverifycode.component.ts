import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { SMSService } from '../../../webapi/sms.service';


@Component({
  selector: 'app-smsverifycode',
  templateUrl: './smsverifycode.component.html',
  styleUrls: ['./smsverifycode.component.scss'],
  providers: [SMSService]

})
export class SmsverifycodeComponent implements OnInit {

  @Input() public phoneNumber = '';
  @Input() public Disabled = true;

  @Output() SMSReceived = new EventEmitter<string>();


  public codeBtnTips = '获取短信验证码';
  constructor(private smsService: SMSService) {

  }
  ngOnInit() {
  }
  getVerificationCodeErrorString() {
    if (this.phoneNumber === '') {
      return '手机号不能为空';
    }
    return '';
  }

  getCode() {
    if (this.phoneNumber === '') {
      return;
    }
    this.smsService.send(this.phoneNumber).subscribe(res => {
      const data = res as any;
      if (data.code === 0) {
       // this.SMSReceived.emit('ok');
        let ticks = 60;
        this.Disabled = true;
        this.codeBtnTips = ticks + 's后重新获取';
        const timer = setInterval(() => {
          ticks--;
          if (ticks === 0) {
            this.Disabled = false;
            this.codeBtnTips = '获取短信验证码';
            clearInterval(timer);
            this.SMSReceived.emit('');
          } else {
            this.codeBtnTips = ticks + 's后重新获取';
          }
        }, 1000);

      }

    }, err => {
       this.SMSReceived.emit(err.error);
     });
  }

}
