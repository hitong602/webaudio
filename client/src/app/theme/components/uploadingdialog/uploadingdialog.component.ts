import { Component, ElementRef, OnInit, Inject, ViewChild , AfterViewInit} from '@angular/core';
import {  MatDialogRef, MatDialogConfig, MAT_DIALOG_DATA} from "@angular/material/dialog";
import { UploadService } from '../../../webapi/upload.service';
import { forkJoin } from 'rxjs/index';


@Component({
	selector: 'app-uploadingdialog',
	templateUrl: './uploadingdialog.component.html',
	styleUrls: ['./uploadingdialog.component.scss'],
	providers: [UploadService]
})
export class UploadingdialogComponent implements OnInit, AfterViewInit {

	public files: Array<File> = [];
	public minimize = false;

	@ViewChild('title', {static: true}) dlgTitle;

	public dlgHeight = 400;
	public dlgWidth = 500;


	public dockerRect;

	progress: any;
	canBeClosed = true;
	primaryButtonText = 'Upload';
	showCancelButton = true;
	uploading = false;
	uploadSuccessful = false;

	constructor(private elementRef: ElementRef, public dialogRef: MatDialogRef<UploadingdialogComponent>,
		           @Inject(MAT_DIALOG_DATA) public data: any) {
		this.files = data.files;
		this.dockerRect = data.dockerrect;
	}
	ngAfterViewInit() {
		this.Show(1);  // 显示全部界面

	/**开始启动上传
	 * 1. 检查文件 本地是否可以播放
	 * 2. 检查是否是s48,通过javascript 检查
	 * 3. 没问题了，就上传到服务端，同时转码形成mp3 和 波形文件
	 *  */	

	}
	ngOnInit() {
	}
	OnClose() {
		return this.dialogRef.close();
	}
	Show(status: any) {
		const dlgTitleRect = 	this.dlgTitle.nativeElement.getBoundingClientRect();
		const dlgTitleHeight = dlgTitleRect.bottom - dlgTitleRect.top;

		const matDialogConfig: MatDialogConfig = new MatDialogConfig();

		if ( status === 0 ) {
			matDialogConfig.width = `${this.dlgWidth}px`;
			matDialogConfig.height = `${dlgTitleHeight}px`;
			matDialogConfig.position = { left: `${this.dockerRect.right - this.dlgWidth}px`,
										  top: `${this.dockerRect.bottom - dlgTitleHeight}px` };

			this.dialogRef.updatePosition(matDialogConfig.position);
			this.dialogRef.updateSize(matDialogConfig.width, matDialogConfig.height);


		} else {
			matDialogConfig.width = `${this.dlgWidth}px`;
			matDialogConfig.height = `${this.dlgHeight}px`;
			matDialogConfig.position = { left: `${this.dockerRect.right - this.dlgWidth}px`,
										  top: `${this.dockerRect.bottom - this.dlgHeight}px` };

			this.dialogRef.updatePosition(matDialogConfig.position);
			this.dialogRef.updateSize(matDialogConfig.width, matDialogConfig.height);
		}
	}
	OnMinimize() {
		this.minimize = !this.minimize;
		this.Show(this.minimize ? 0 : 1);
		// 更新位置
	}
	closeDialog() {
		// if everything was uploaded already, just close the dialog
		// if (this.uploadSuccessful) {
		// 	return this.dialogRef.close();
		// }

		// // set the component state to "uploading"
		// this.uploading = true;

		// // start the upload and save the progress map
		// this.progress = this.uploadService.upload(this.files);

		// // convert the progress map into an array
		// const allProgressObservables = [];
		// // tslint:disable-next-line:forin
		// for (const key in this.progress) {
		// 	allProgressObservables.push(this.progress[key].progress);
		// }

		// // Adjust the state variables

		// // The OK-button should have the text "Finish" now
		// this.primaryButtonText = 'Finish';

		// // The dialog should not be closed while uploading
		// this.canBeClosed = false;
		// this.dialogRef.disableClose = true;

		// // Hide the cancel-button
		// this.showCancelButton = false;

		// // When all progress-observables are completed...
		// forkJoin(allProgressObservables).subscribe(end => {
		// 	// ... the dialog can be closed again...
		// 	this.canBeClosed = true;
		// 	this.dialogRef.disableClose = false;

		// 	// ... the upload was successful...
		// 	this.uploadSuccessful = true;

		// 	// ... and the component is no longer uploading
		// 	this.uploading = false;
		// });
	}




}
