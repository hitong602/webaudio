import { Directive , HostListener } from '@angular/core';
import { NgControl } from '@angular/forms';


@Directive({
  selector: '[appBlurValidate]'
})
export class BlurValidateDirective {

  constructor(private _control: NgControl) { }
  @HostListener('focus')

  public onFocus(): void {
    this._control.control.markAsUntouched();
    this._control.control.markAsDirty();

   // this._control.control.markAsPristine();
  }



}
