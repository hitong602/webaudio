import { Injectable, Inject } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';

import { API_URL } from '../app.tokens';
import { AppSettings } from '../app.settings';

@Injectable()
export class FileService {

  private currentUser;

  constructor(private http: HttpClient, private appsetting: AppSettings, @Inject(API_URL) private apiUrl) {
    this.currentUser = appsetting.currentUser;
  }

  dataURItoBlob(dataURI) {
    const byteString = atob(dataURI.split(',')[1]);
    const mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
    const ab = new ArrayBuffer(byteString.length);
    const ia = new Uint8Array(ab);
    for (let i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }
    return new Blob([ab], { type: mimeString });
  }
  upload(cropedimage: any, dataname: any, urlpath: string , filename: any) {
    const headers = new HttpHeaders({
      Authorization: 'Bearer ' + this.currentUser.token
    });

    const myForm = new FormData();
    const  blob = this.dataURItoBlob(cropedimage);

    myForm.append(dataname, blob, filename);
   // myForm.append('id', this.currentUser.id);


    const url = this.apiUrl + urlpath;
    // '/users/avatar';

    return this.http.post(url, myForm, { headers: headers });
  }

}
