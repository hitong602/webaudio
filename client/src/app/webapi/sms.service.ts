import { Injectable , Inject} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';


import {API_URL} from '../app.tokens';

@Injectable()
export class SMSService {
  url: string ;
    constructor(private http: HttpClient, @Inject(API_URL) private apiUrl) { }

    send(phonenumber: string) {

      // const headers = new Headers();
      // headers.append('Content-Type', 'application/x-www-form-urlencoded');


      const headers = new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded'
      });
     // const options = new RequestOptions({ headers: headers });


      const body = 'tel=' + phonenumber;

      this.url = this.apiUrl + '/auth/sms';
      return this.http.post(this.url,
              body,
              {
                headers
              });
              // .pipe(map((response: Response) => {
              //   const res = response.json() as any;
              //   if (res && (res).code) {
              //     return res.code;
              //   } else {
              //     return '';
              //   }

              // }));

    }
}
