import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { API_URL } from '../app.tokens';
import { User } from '../model/userInfo.model';
import {AppSettings} from '../app.settings';

@Injectable()
export class UserService {
  private currentUser: User;

  constructor(private http: HttpClient, private appsetting: AppSettings, @Inject(API_URL) private apiUrl) {
    this.currentUser = appsetting.currentUser;
   }

  getAll() {
    return this.http.get('/api/users');
  }

  getById(id: number) {
    const headers = new HttpHeaders({
      'Content-type': 'application/json',
      Authorization: 'Bearer ' + this.currentUser.token
    });

    const url = this.apiUrl + '/users/';
    return this.http.get(url + id, { headers: headers });
  }

  create(user: any, smscode: any) {

    const myheader = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
    const url = this.apiUrl + '/auth/reg';

    const body = 'phonenumber=' + user + "&smscode=" + smscode;

    return this.http.post(url, body, { headers: myheader });

  }

  updateprofile(companyname: any, personname: any, password: any) {

    const myheaders = new HttpHeaders({
      'Content-type': 'application/x-www-form-urlencoded',
      Authorization: 'Bearer ' + this.currentUser.token
    });

    const body = 'personid=' + this.currentUser.id + "&companyname=" + companyname + "&personname=" + personname + "&password=" + password;


    const url = this.apiUrl + '/users';

    return this.http.put(url, body, {headers: myheaders});
  }
  login(username: any, password: any, type: any) {
    const myheaders = new HttpHeaders({
      'Content-type': 'application/x-www-form-urlencoded',
    });

    const body = 'loginname=' + username + "&password=" + password + "&logintype=" + type ;


    const url = this.apiUrl + '/auth';

    return this.http.post(url, body, {headers: myheaders});
  }

  delete(id: number) {
    return this.http.delete('/api/users/' + id);
  }
  setCurrentUser(userdata) {
    this.currentUser.id = userdata.id;
    this.currentUser.nickName = userdata.nickname;
  }

  getCurrentUser() {
    return this.currentUser;
  }
}
