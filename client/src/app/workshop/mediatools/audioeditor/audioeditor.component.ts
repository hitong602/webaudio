import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';

import * as AEModule from '../../../../assets/wasm/ffmpeg/audioeditor.js';

let membuffer: any;
let wasmModule: any;
function LoopUpdate() {
  wasmModule._loop();
  window.requestAnimationFrame(LoopUpdate);
}



@Component({
  selector: 'app-audioeditor',
  templateUrl: './audioeditor.component.html',
  styleUrls: ['./audioeditor.component.scss']
})
export class AudioeditorComponent implements OnInit {

  @ViewChild('file', { static: true }) file;
  @ViewChild('canvas', { static: true }) canvas: ElementRef;
  mediatoolsWorker: any;   // 创建一个 线程， 读取文件，并分析文件格式
  readingpercent: any;


  constructor() {
    this.mediatoolsWorker = new Worker(`./mediatools.worker`, { type: `module` });
    // this.mediatoolsWorker.addEventListener('message', ({ data }) => {
    this.mediatoolsWorker.onmessage = (e) => {
      const data = e.data;

      if (data.eventType === "PROBEFILEOK") {
        console.log(data);

      } else if (data.eventType === "INITIALIZED") {
        console.log(data);

      } else if (data.eventType === "EXTRACTENERGYEOK") {
        //data.eventData
        console.log(data);

        const energy_ptr = wasmModule._malloc(data.len);
        wasmModule.writeArrayToMemory(data.energy, energy_ptr);
        wasmModule._ImportFile("l", data.audioinfo.sampleRate, data.audioinfo.channelNum, data.audioinfo.bitrate,
          data.audioinfo.codeID, data.audioinfo.duration, data.audioinfo.totalFrames, energy_ptr, data.len);


      } else if (data.eventType === "READFILEPERCENT") {
        this.readingpercent = data.eventData;


      }
    };
  }

  public async  LoadAudioEditorsWasm(canvas: any) {
    const wasmFile = await fetch("../../../../assets/wasm/ffmpeg/audioeditor.wasm");

    // convert it into a binary array
    const buffer = await wasmFile.arrayBuffer();
    const binary = new Uint8Array(buffer);

    const that = this;
    // create wasmModule arguments
    // including the wasm-file
    membuffer = new WebAssembly.Memory({
      initial: 256,
      maximum: 256
    });


    const moduleArgs = {
      wasmBinary: binary,
      wasmMemory: membuffer,
      onRuntimeInitialized: () => {
        canvas.width = 1024;
        canvas.height = 400;
        wasmModule.canvas = canvas;
        wasmModule._CreateAudioEditor(canvas.width, canvas.height);
        //  window.requestAnimationFrame(LoopUpdate);

      }

    };

    // instantiate the wasmModule
    wasmModule = AEModule(moduleArgs);
  }
  public LoadMediatoolsWasm() {
    this.mediatoolsWorker.postMessage({ eventType: "INITIALIZE", eventData: "../../../../assets/wasm/ffmpeg/mediatools.wasm" });
  }

  public OnCutClick() {
    if (wasmModule) {
      wasmModule._Cut();
    }
  }
  public OnUndoClick() {
    if (wasmModule) {
      wasmModule._Undo();
    }
  }
  public importFile(lfile: any) {


    this.mediatoolsWorker.postMessage({ eventType: "EXTRACTENERGY", eventData: lfile });


  }
  getPointOnCanvas(canvas, x, y) {
    const bbox = canvas.getBoundingClientRect();
    return {
      x: x - bbox.left * (canvas.width / bbox.width),
      y: y - bbox.top * (canvas.height / bbox.height)
    };
  }


  ngOnInit() {
    const that = this;
    const canvasReference = this.canvas.nativeElement as HTMLCanvasElement;
    canvasReference.addEventListener(
      'webglcontextlost',
      (e) => {
        alert('WebGL context lost. You will need to reload the page.');
        e.preventDefault();
      },
      false
    );
    // canvasReference.addEventListener("mousemove", (event) => {
    //   const loc = that.getPointOnCanvas(canvasReference, event.clientX, event.clientY);
    //   wasmModule._OnMouseOption(2, loc.x, loc.y);


    // }, true);
    // canvasReference.addEventListener("mousedown", (event) => {
    //   const loc = that.getPointOnCanvas(canvasReference, event.clientX, event.clientY);
    //   wasmModule._OnMouseOption(0, loc.x, loc.y);


    // }, true);
    // canvasReference.addEventListener("mouseup", (event) => {
    //   const loc = that.getPointOnCanvas(canvasReference, event.clientX, event.clientY);
    //   wasmModule._OnMouseOption(1, loc.x, loc.y);


    // }, true);

    this.LoadMediatoolsWasm();  //在webworker 中加载，用于 解码和提取波形
    this.LoadAudioEditorsWasm(canvasReference); // 
  }
  onFilesAdded() {
    const files: { [key: string]: File } = this.file.nativeElement.files;
    const selectfiles: Array<File> = [];

    for (const key in files) {
      // 把这些文件 传入到当前的组件中
      if (!isNaN(parseInt(key, 10))) {

        const localfile = files[key];
        this.importFile(localfile);
      }
    }

  }
  OnUploadClick() {
    this.file.nativeElement.click();
  }

}
