export class PingPongBuffer {
    pingBuffer: any;
    pongBuffer: any;
    length: number;
    nextUseBufferIndex: number;

    constructor(size: number) {
        this.pingBuffer = new ArrayBuffer(size);
        this.pongBuffer = new ArrayBuffer(size);
        this.length = size;
        this.nextUseBufferIndex = 0; // 
    }
    getBuffer(): any {
        if (this.nextUseBufferIndex === 0) {
            this.nextUseBufferIndex = 1;
            return this.pingBuffer;
        } else {
            this.nextUseBufferIndex = 0;

            return this.pongBuffer;

        }
    }
    setBuffer(pb: any) {
        if (this.nextUseBufferIndex === 0) {
            this.pongBuffer = pb;
        } else {
            this.pingBuffer = pb;
        }

    }
    setPingBuffer(pb: any) {
        this.pingBuffer = pb;
        // this.pingBuffer.length=1010;
    }
    setPongBuffer(pb: any) {
        this.pongBuffer = pb;

    }
}
