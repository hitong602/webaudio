//// <reference lib="webworker" />
import * as Module from '../../../../assets/wasm/ffmpeg/mediatools.js';
import { setDefaultService } from 'selenium-webdriver/chrome';

let readCallback: any;

let fileReader: any;
let localfile: any;
let membuffer: any;
let wasmModule: any;
let totalSize:any;


//这个函数被wasm 调用，读取文件
function readFile(buf: number, size: number, pos: number, userdata: number): number {

  function readBlob(start: any, stepp: any) {
    const blob = blobSlice(localfile, start, start + stepp - 1);
    return fileReader.readAsArrayBuffer(blob);
  }
  function blobSlice(blob, start, end) {
    if (blob.slice) {
      return blob.slice(start, end);
    } else if (blob.webkitSlice) {
      return blob.webkitSlice(start, end);
    } else if (blob.mozSlice) {
      return blob.mozSlice(start, end);
    } else {
      return null;
    }
  }
  const ab = readBlob(pos, size);
  const readlen = ab.byteLength;

  const aView = new Uint8Array(ab, 0, readlen);

  //fileReadingPos += readlen;
  postMessage({ eventType: "READFILEPERCENT", eventData: pos*100/totalSize });


  const heap = new Uint8Array(membuffer.buffer, buf, readlen);
  for (let i = 0; i < readlen; i++) {
    heap[i] = aView[i];
  }

  return readlen;
}
async function instantiateWasm(url: string) {
  // fetch the wasm file
  const wasmFile = await fetch(url);

  // convert it into a binary array
  const buffer = await wasmFile.arrayBuffer();
  const binary = new Uint8Array(buffer);

  // create wasmModule arguments
  // including the wasm-file
  membuffer = new WebAssembly.Memory({
    initial: 256,
    maximum: 256
  });


  const moduleArgs = {
    wasmBinary: binary,
    wasmMemory: membuffer,
    onRuntimeInitialized: () => {
      // waTest = wasmModule._TestCallback;
      // waProbeAudio = wasmModule._probeaudio;
      readCallback = wasmModule.addFunction(readFile, 'iiiii');
      //waCreateProject = wasmModule._CreateAudioEditor;
      //告诉主线程，下载（加载）完成，如何写进度？？？？？
      postMessage({ eventType: "INITIALIZED", eventData: "initok" });
    }

  };

  // instantiate the wasmModule
  wasmModule = Module(moduleArgs);
  // wasmModule.screen = {height:100, width:200};
}
addEventListener('message', ({ data }) => {
  const { eventType, eventData } = data;

  if (eventType === "INITIALIZE") {
    instantiateWasm(eventData);

  } else if (eventType === "PROBEFILE") {

    localfile = eventData;
    fileReader = new FileReaderSync();


    const result = wasmModule.ProbeFile(localfile.name, readCallback, 0);
    postMessage({ eventType: "PROBEFILEOK", eventData: result });


  } else if (eventType === "EXTRACTENERGY") {
    localfile = eventData;

    totalSize = localfile.size;
    if (totalSize === 0) {
      return;
    }

    fileReader = new FileReaderSync();


   
    //in facr prelen and ptrenergy is data pointer,  by this pointer ,get energy 
    const array = new Int32Array(1);
    const ptrlen = wasmModule._malloc(array.length);

    // const uarray = new Uint32Array(1);

    const ptrenergy = wasmModule._malloc(array.length);

    const result = wasmModule.ExtractEnergy(readCallback, 0, ptrenergy, ptrlen);
    console.log(result);

    const energyoffset = wasmModule.getValue(ptrenergy, 'i32');
    const energylen = wasmModule.getValue(ptrlen, 'i32');

    const energydata = new Uint8Array(energylen);

    const heap = new Uint8Array(membuffer.buffer, energyoffset, energylen);
    for (let i = 0; i < energylen; i++) {
      energydata[i] = heap[i];
    }

    wasmModule._free(ptrlen);
    wasmModule._free(ptrenergy);
    data = { eventType: "EXTRACTENERGYEOK", audioinfo: result, energy: energydata, len: energylen };

    postMessage(data, [data.energy.buffer]);

  }
});
