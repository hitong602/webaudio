import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MaterialSharedModule} from '../../shared/material.shared.module';

import { MediatoolsComponent } from './mediatools.component';
import {AudioeditorComponent} from './audioeditor/audioeditor.component';
// import {AudioeditorService} from './audioeditor/audioeditor.service';
// eport const routes = [
//   { path: '', component: MediatoolsComponent, pathMatch: 'full' }
// ];x
export const routes = [
  {
    path: '', component: MediatoolsComponent, children: [
      { path: '', component: AudioeditorComponent, data: { breadcrumb: '音频编辑器' }, pathMatch: 'full' },
      { path: 'audioeditor', component: AudioeditorComponent, data: { breadcrumb: '音频编辑器' }, pathMatch: 'full' }
    ]
  },
];

@NgModule({
  declarations: [
    MediatoolsComponent,
    AudioeditorComponent
  ],
  imports: [
    CommonModule,
    MaterialSharedModule,
    RouterModule.forChild(routes)


  ], providers: [
   // AudioeditorService
    ],
})
export class MediatoolsModule { }
