import { Component, OnInit, ViewChild, ElementRef , AfterViewInit } from '@angular/core';

import { NetdiskdataService } from '../netdiskdata.service';


export interface PeriodicElement {
	name: string;
	size: number;
	duration: number;
	format: string;
}




@Component({
	selector: 'app-audio-file-manager',
	templateUrl: './audio-file-manager.component.html',
	styleUrls: ['./audio-file-manager.component.scss'],
	providers: []
})
export class AudioFileManagerComponent implements OnInit, AfterViewInit {

	displayedColumns: string[] = ['name', 'size', 'duration', 'format'];

	@ViewChild('table', { static: true }) table;

	dataSource: PeriodicElement[] = [];

	public selectedRow: any;
	public selectedIndex: any;
	public hoveredIndex: any;

	public rect: any;

	constructor(private elementRef: ElementRef , private netdiskDataService: NetdiskdataService) {
		this.selectedIndex = -1;

		this.netdiskDataService.pageNameChange$.subscribe(
			v => {
				const pe = { name: v.name, size: 100, duration: 80, format: 's48/mp3' };
				this.dataSource.push(pe);
				this.table.renderRows();
				// 一个数组的内容改变，还不能刷新data， 如果做到内容改变，table自动刷新
				// this.dataSource =   this.ELEMENT_DATA;


			}
		);

		// audiotools.probeaudio('\\file\aa.mp3');

	}

	TestWasm(){
	//	this.audiotools.probefile('sdsd');

	}
	ngOnInit() {
	}

	ngAfterViewInit() {
		this.rect = this.elementRef.nativeElement.getBoundingClientRect();

	}

	selectRow(row, index) {
		this.selectedRow = row;
		this.selectedIndex = index;



	}
	HoverRow(index) {
		this.hoveredIndex = index;
	}
	selectionToggle(row) {
		const sss = row.index;
	}




}
