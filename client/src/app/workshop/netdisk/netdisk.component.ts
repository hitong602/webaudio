import { Component, OnInit, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
import { MenuService } from '../../theme/components/menu/menu.service';
import { MatDialog } from '@angular/material';

import { NetdiskdataService } from './netdiskdata.service';
import { UploadingdialogComponent } from '../../theme/components/uploadingdialog/uploadingdialog.component';

@Component({
  selector: 'app-netdisk',
  templateUrl: './netdisk.component.html',
  styleUrls: ['./netdisk.component.scss'],
  providers: [MenuService]

})


export class NetdiskComponent implements OnInit, AfterViewInit {

  public netdiskmenuitems: Array<any>;
  @ViewChild('freespace', { static: true }) freeSpace;

  ActivePage: any;

  constructor(
              public dialog: MatDialog,
              public netdiskDataService: NetdiskdataService,
              public menuService: MenuService) {

  }

  ngOnInit() {
    this.netdiskmenuitems = this.menuService.getSubMenuItems('wangpan');
    //  this.netdiskDataService.changedUploadFileList(this.files);


  }
  ngAfterViewInit() {
    // 	this.rect = this.elementRef.nativeElement.getBoundingClientRect();

  }
  public openUploadDialog(data) {

    // const target = this.ActivePage.rect;
    const rect = this.freeSpace.nativeElement.getBoundingClientRect();


    const dialogRef = this.dialog.open(UploadingdialogComponent, {

      hasBackdrop: false,
      panelClass: 'no-padding-dialog',
      data: { files: data, dockerrect: rect }
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
  // 选择好文件后，发过来的通知
  onFilesAdded() {
    // const files: { [key: string]: File } = this.file.nativeElement.files;
    // const selectfiles: Array<File> = [];

    // for (const key in files) {
    //   // 把这些文件 传入到当前的组件中
    //   if (!isNaN(parseInt(key, 10))) {
    //     this.audiotools.probefile(files[key]);
    //     //selectfiles.push();
    //   }
    // }


   // this.openUploadDialog(selectfiles);


  }
  OnUploadClick() {
  //  this.file.nativeElement.click();
  }
  onActivate($event) {
    this.ActivePage = $event;


  }

}
