import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MaterialSharedModule} from '../../shared/material.shared.module';
import {ThemeSharedModule} from '../../shared/theme.shared.module';
import {NetdiskComponent} from './netdisk.component';
import { AudioFileManagerComponent } from './audio-file-manager/audio-file-manager.component';
import { NewsComponent } from './news/news.component';
import { RecyclebinComponent } from './recyclebin/recyclebin.component';
import { SharemeComponent } from './shareme/shareme.component';
import {NetdiskdataService} from './netdiskdata.service';

export const routes = [
  { path: '', component: NetdiskComponent, children: [
    { path: 'audfilemanager', component: AudioFileManagerComponent, data: { breadcrumb: '音频文件' } , pathMatch: 'full'},
    { path: 'news', component: NewsComponent, data: { breadcrumb: '文稿' } , pathMatch: 'full'},
    { path: 'recyclebin', component: RecyclebinComponent, data: { breadcrumb: '回收站' } , pathMatch: 'full'},
    { path: 'shareme', component: SharemeComponent, data: { breadcrumb: '共享与我' } , pathMatch: 'full'},


  ] },
];

@NgModule({
  declarations: [
    NetdiskComponent,
    AudioFileManagerComponent,
    NewsComponent,
    RecyclebinComponent,
	SharemeComponent
  ],
  imports: [
    CommonModule,
    MaterialSharedModule,
    ThemeSharedModule,
    FormsModule,
    RouterModule.forChild(routes)
  ],
  providers: [
	NetdiskdataService

  ],
  entryComponents: [
  ]
})
export class NetdiskModule { }
