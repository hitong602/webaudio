import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/index';
import { Observable } from 'rxjs/index';

import {NetdiskModule} from './netdisk.module';

@Injectable({
  providedIn: 'root'
})
export class NetdiskdataService {


  private uploadFileLists = new Subject<string>();

   pageNameChange$: any = this.uploadFileLists.asObservable();


  changedUploadFileList(fileslist: any) {

    for (const element of fileslist) {
      this.uploadFileLists.next(element);
    }
  }
}
