import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SharemeComponent } from './shareme.component';

describe('SharemeComponent', () => {
  let component: SharemeComponent;
  let fixture: ComponentFixture<SharemeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SharemeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SharemeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
