import { Component, OnInit , AfterViewInit, ViewChild } from '@angular/core';
import { AppSettings } from '../app.settings';
import {Settings} from '../app.settings.model';

import { MenuService } from '../theme/components/menu/menu.service';

// import { SidenavComponent } from '../theme/components/sidenav/sidenav.component';

@Component({
  selector: 'app-workshop',
  templateUrl: './workshop.component.html',
  styleUrls: ['./workshop.component.scss'],
  providers: [ MenuService ]


})
export class WorkshopComponent implements OnInit, AfterViewInit {

  @ViewChild('sidenav', {static: true}) sidenav: any;

  public settings: Settings;
  public mainmenuitems: Array<any>;

  public scrolledContent: any;
  public showBackToTop: boolean = false;
  public lastScrollTop: number = 0;
  public isStickyMenu: boolean = false;

  

  constructor(public appSettings: AppSettings, public menuService: MenuService) {
    this.settings = this.appSettings.settings;

  }

  ngOnInit() {
    this.mainmenuitems = this.menuService.getMainMenuItems();
    // if (window.innerWidth <= 768) {
    //   this.settings.menu = 'vertical';
    //   this.settings.sidenavIsOpened = false;
    //   this.settings.sidenavIsPinned = false;
    // }
  }
  public onPsScrollY(event){
    this.scrolledContent = event.target;
    (this.scrolledContent.scrollTop > 300) ? this.showBackToTop = true : this.showBackToTop = false; 
    if(this.settings.menu === 'horizontal'){
      if(this.settings.fixedHeader){
        var currentScrollTop = (this.scrolledContent.scrollTop > 56) ? this.scrolledContent.scrollTop : 0;   
        (currentScrollTop > this.lastScrollTop) ? this.isStickyMenu = true : this.isStickyMenu = false;
        this.lastScrollTop = currentScrollTop; 
      } 
      else{
        (this.scrolledContent.scrollTop > 56) ? this.isStickyMenu = true : this.isStickyMenu = false;  
      }
    } 
  }

  public scrollToTop(){
    var scrollDuration = 200;
    var scrollStep = -this.scrolledContent.scrollTop / (scrollDuration / 20);
    var scrollInterval = setInterval(()=>{
      if(this.scrolledContent.scrollTop != 0){
         this.scrolledContent.scrollBy(0, scrollStep);
      }
      else{
        clearInterval(scrollInterval); 
      }
    },10);
    if(window.innerWidth <= 768){
      this.scrolledContent.scrollTop = 0;
    }
  }
  public toggleSidenav() {
    this.sidenav.toggle();
  }

  ngAfterViewInit() {
		setTimeout(() => { this.appSettings.settings.loadingSpinner = false; }, 300);
  }
}
