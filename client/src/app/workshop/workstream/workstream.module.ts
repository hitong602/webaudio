import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouterModule } from '@angular/router';
import { MaterialSharedModule} from '../../shared/material.shared.module';

import {WorkstreamComponent} from './workstream.component';

export const routes = [
  { path: '', component: WorkstreamComponent, pathMatch: 'full' }
];
@NgModule({
  declarations: [
    WorkstreamComponent
  ],
  imports: [
    CommonModule,
    MaterialSharedModule,
    RouterModule.forChild(routes)
  ]
})
export class WorkstreamModule { }
