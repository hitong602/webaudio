var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var expressJWT = require('express-jwt');
var jwtauth = require('./app/service/token.js');


var indexRouter = require('./routes/index');

var app = express();

var authRouter=require('./app/authentication/router');
var usersRouter = require('./app/users/router');


//实现 跨域 访问
//http://localhost:4200
var allowCrossDomain = function(req, res, next) {
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Methods', 'GET,PUT,POST');
	res.header('Access-Control-Allow-Headers', 'Content-Type,Authorization');
	res.header('Access-Control-Allow-Credentials','true');
	next();
  };


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');


  //跨域访问
app.use(allowCrossDomain);


app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(expressJWT({
	secret: jwtauth.getSceretKey()
  }).unless({
	path: [
	  /^\/api\/auth/
	]
  }));


app.use('/', indexRouter);

//web api 需要用
app.use('/api/auth',authRouter);
app.use('/api/users', usersRouter);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  if (err.name === 'UnauthorizedError') {
    res.status(401).send('invalid token...');
  }
  else{
	res.locals.message = err.message;
	res.locals.error = req.app.get('env') === 'development' ? err : {};
  
	// render the error page
	res.status(err.status || 500);
	res.render('error');

  }
  
});

module.exports = app;
