var db = require('../service/pgcontion');

/* 查询这个电话号，并返回相关的公司id 和名称
*/
exports.authenticate = (phonenumber,logintype) => {

	var queryString = "SELECT \"public\".person.nickname, \
	\"public\".person.\"id\" AS personid,\
	\"public\".company_person.company_id AS companyid,\
	\"public\".company.\"name\" as companyname \
	FROM \
	\"public\".person \
    \INNER JOIN \"public\".auths ON \"public\".person.\"id\" = \"public\".auths.person_id \
	LEFT OUTER JOIN \"public\".company_person ON \"public\".person.\"id\" = \"public\".company_person.person_id \
	LEFT OUTER JOIN \"public\".company ON \"public\".company_person.company_id = \"public\".company.\"id\" \
	WHERE \
	\"public\".auths.identity_type = $1 AND \
	\"public\".auths.identifier = $2";
    return db.any(queryString,[logintype,phonenumber] );
};


exports.verifypassword = (loginname,password,logintype)=>{
	var queryString ="SELECT person_id FROM auths WHERE identifier=$1 AND credential=$2 AND identity_type=$3";
	return db.one(queryString,[loginname,password,logintype]);

};
exports.create=(phonenumber)=>{
  return db.func('register',[phonenumber,'phone','']);
};

