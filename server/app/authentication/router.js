var express = require("express");

var resisclient = require("../service/redisclient");

var alisms = require("../service/aliyun/sms");

var authModel = require("./auth-model");
var token = require("../service/token");


var myTools = require("../MyTools");


var router = express.Router();


router.post('/reg', signup)
router.post('/', login)
router.post('/sms', sms)

function sms(req, res, next) {
	var telnumber = req.body.tel;

	var code = alisms.generateCode(6);
	alisms.sms(telnumber, code).then(data => {
			if (data.Code === "OK") {
				//save the code
				resisclient.set(telnumber, code);
				resisclient.expire(telnumber, 60); //将验证吗保存，60秒过期
				res.status(200).json({
					code: 0
				}).end();
			} else {
				res.status(404).json('获取验证码过于频繁，请稍后再试');
			}
		})
		.catch(error => {
			res.status(404).json('获取验证吗错误');
		});
}

function login(req, res, next) {
	if (req.body.loginname && req.body.password && req.body.logintype) {
		var name = req.body.loginname;
		var password = req.body.password;
		var logintype = req.body.logintype;
	} else {
		return res.status(404).json("parameter error");
	}

	const makeverifyrequest = (type) => {
		if (type === 'sms') {
// 测试代码，将验证吗保存，60秒过期
			resisclient.set(req.body.loginname, '123456');
			resisclient.expire(req.body.loginname, 60); 
// 测试代码
			return resisclient.get(req.body.loginname);

		} else { //其它方式
			return authModel.verifypassword(name, password, logintype);

		}

	}
	makeverifyrequest(logintype).then(r => {
		if(r!=req.body.password && logintype === 'sms'){
			  return Promise.reject({
				MyPromiseException: true,
				message: '验证码错误',
			   });

		} else {
			return authModel.authenticate(name, logintype);

			
		}

	}).then(userinfo => {
		if (userinfo.length == 0) {
			return Promise.reject({
				MyPromiseException: true,
				message: '登录的用户没有找到',
			  });
		} else {
			var tokenvalue = token.generateToken(userinfo[0].personid);
			res.status(200).json({
				code: 0,
				token: tokenvalue,
				PersonInfo: userinfo
			}).end();

		}


	}).catch(err => {
		res.status(404).json(err.message);
	});
}

function signup(req, res, next) {

	var phonenumber = req.body.phonenumber;
	var smsverifycode = req.body.smscode;
	if (myTools.isEmpty(phonenumber) || myTools.isEmpty(smsverifycode)) {
		res.status(404 ).json({
			MyPromiseException: true,
			message: "用户参数不正确"
		});
		return;
	}
//	resisclient.set(phonenumber, '123456');
//	resisclient.expire(phonenumber, 60); //将验证吗保存，60秒过期

	//目前注册是通过手机完成的，必须通过验证码的
	resisclient.get(phonenumber).then(reply => {
		if(smsverifycode !=reply)  //
		{
			return Promise.reject({
				MyPromiseException: true,
				message: '验证码不正确',
			  });
			//throw new Error('验证吗不正确');
		}else{
			return authModel.authenticate(phonenumber);
		}
	}).then(r => {
		if (r.length == 0) { //这个手机号没有注册，要注册上，并返回token
			return authModel.create(phonenumber);
		} else {
			var tokenvalue = token.generateToken(r[0].personid);
			res.status(200).json({
				code: 0,
				token: tokenvalue,
				PersonInfo: r
			});
			return Promise.reject({notRealPromiseException: true});
		}
	}).then(r => {
		var tmpvalue = r[0].register;
		if (tmpvalue == null) {
			return Promise.reject({
				MyPromiseException: true,
				message: '创建用户失败',
			  });
		}
		var thePerson = new Object();
		thePerson.id = tmpvalue;
		thePerson.phonenumber = phonenumber;

		var tokenvalue = token.generateToken(tmpvalue);

		res.status(200).json({
			code: 0,
			token: tokenvalue,
			Person: thePerson
		});
	//	return Promise.reject({notRealPromiseException: true});

	}).catch(err => {
		if (err.notRealPromiseException) {
			return ;
		  }
		return res.status(404 ).json(err.message);
	})

}
module.exports = router;
