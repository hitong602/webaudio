var express = require("express");
var router = express.Router();
// var alioss=require("../service/aliyun/oss");
// var aliUploader=require('../service/aliyun/uploader');
 var userModel = require("./user-model");

// var Person=require('./person');
var myTools = require("../MyTools");


router.put('/', updateUser)

router.get('/:id', getUserById)


function getUserById(req, res, next) {
  // const id=req.params.id;
  // if(!id )
  // {
  //   res.status(401).json({
  //     result: 'error',
  //     message: '找不到用户'
  //   })
  //   return;
  // }

  // userModel
  //   .findbyid(id)
  //   .then(data => {
  //     if (!data)
  //       res.status(401).json({
  //         result: 'error',
  //         message: '找不到用户'
  //       }).end();
  //     else {

  //       if(data.avatar!="")
  //       {
  //         data.avatar=alioss.getOSSObjectSignURL(data.avatar);

  //       }

  //         res.status(200).json({
  //           result: 'ok',
  //           user: data
  //         }).end();

  //     }
  //   }).catch(err=>{
  //     res.status(401).json({result: 'error',message: err.message})}
  //   )
}

function updateUser(req, res, next) {

  if (!myTools.isValidString(req.body.personname) ||
    !myTools.isValidString(req.body.companyname) ||
    !myTools.isValidString(req.body.personid) ||
    !myTools.isValidString(req.body.password)) {
    return res.status(401).json({
        result:1000,
        message: "用户参数不正确"
    });
  }
  // var person = new Person();
  // if(req.body.nickname!="")
  //    person.nickname = users.nickname;
  // if(req.body.email!="")
  //    person.email = users.email;
  // if(req.body.avstar!="")
  //    person.avstar = users.avstar; //Base64
  // person.id = req.params.userid;

  // userModel.update(person);
  userModel.UpdateProfile(req.body.personid,req.body.personname,req.body.companyname,req.body.password).then(result=>{
    res.status(200).json({code: 0,companyid:result});

  }).catch(err=>{
    if(err.table==='company')
    {
      res.status(401).json({code: 1001,message: '单位名称已经存在'});


    }
    else{
      res.status(401).json({code: 1002,message: '访问数据错误'});


    }


  })
};

// router.post('/avatar', aliUploader.upload.single('avatar'), (req, res) => {

//   //写数据库
//   userModel.putAvatar(req.file.originalname,req.file.name).then(data=>{
//     req.file.url = alioss.getOSSObjectSignURL(req.file.name);
//     res.status(200).json({
//       url: req.file.url
//     });

//   }).catch(err=>{
//     res.status(401).json({result: 'error',message: err.message})});


// });

module.exports = router;
